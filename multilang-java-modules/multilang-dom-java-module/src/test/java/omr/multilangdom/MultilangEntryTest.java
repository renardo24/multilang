/*
 * MultilangEntryTest.java
 * JUnit based test
 *
 * Created on 23 February 2006, 10:55
 */

package omr.multilangdom;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Olivier Renard
 */
public class MultilangEntryTest {

    // public static Test suite() {
    // TestSuite suite = new TestSuite(MultilangEntryTest.class);
    // return suite;
    // }

    /**
     * Test of hashCode method, of class
     * org.renardo.multilangdocs.MultilangEntry.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        MultilangEntry instance = new MultilangEntry(1);
        int result = instance.hashCode();
        assertNotEquals(0, result);
    }

    /**
     * Test of equals method, of class org.renardo.multilangdocs.MultilangEntry.
     */
    @Test
    public void testEqualsOnlyIdNoLines() {
        System.out.println("testEqualsOnlyIdNoLines");
        MultilangEntry instance1 = new MultilangEntry(1);
        MultilangEntry instance2 = new MultilangEntry(1);
        boolean expResult = true;
        boolean result = instance1.equals(instance2);
        assertEquals(expResult, result);
        result = instance2.equals(instance1);
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class org.renardo.multilangdocs.MultilangEntry.
     */
    @Test
    public void testEqualsWithIdAndSameEnglishLines() {
        System.out.println("testEqualsWithIdAndSameEnglishLines");

        MultilangLine line1 = new MultilangLine(1);
        line1.setText("Hello");

        MultilangLine line2 = new MultilangLine(1);
        line2.setText("Hello");

        MultilangEntry instance1 = new MultilangEntry(1);
        instance1.setEnglishLine(line1);
        MultilangEntry instance2 = new MultilangEntry(1);
        instance2.setEnglishLine(line2);

        boolean expResult = true;
        boolean result = instance1.equals(instance2);
        assertEquals(expResult, result);

        result = instance2.equals(instance1);
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class org.renardo.multilangdocs.MultilangEntry.
     */
    @Test
    public void testEqualsWithIdAndDifferentEnglishLines() {
        System.out.println("testEqualsWithIdAndDifferentEnglishLines");

        MultilangLine line1 = new MultilangLine(1);
        line1.setText("Hello");

        MultilangLine line2 = new MultilangLine(1);
        line2.setText("Goodbye");

        MultilangEntry instance1 = new MultilangEntry(1);
        instance1.setEnglishLine(line1);
        MultilangEntry instance2 = new MultilangEntry(1);
        instance2.setEnglishLine(line2);

        boolean expResult = false;
        boolean result = instance1.equals(instance2);
        assertEquals(expResult, result);

        result = instance2.equals(instance1);
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class org.renardo.multilangdocs.MultilangEntry.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        String sep = System.getProperty("line.separator");

        MultilangEntry instance = new MultilangEntry(1);
        MultilangLine line = new MultilangLine(1);
        line.setText("Olivier");
        instance.setEnglishLine(line);

        StringBuffer buf = new StringBuffer();
        buf.append("entry")
                .append(sep);
        buf.append("\ten")
                .append(sep);
        buf.append("\t\tTEXT: Olivier")
                .append(sep);
        String expResult = buf.toString();
        String result = instance.toString();
        assertNotNull(result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getEntryId and setEntryId methods, of class
     * org.renardo.multilangdocs.MultilangEntry.
     */
    @Test
    public void testGetAndSetEntryId() {
        System.out.println("getEntryId and setEntryId");
        MultilangEntry instance = new MultilangEntry(1);
        long expResult = 1L;
        long result = instance.getEntryId();
        assertEquals(expResult, result);

        instance.setEntryId(20);
        expResult = 20L;
        result = instance.getEntryId();
        assertEquals(expResult, result);
    }

    /**
     * Test of getEnglishLine and setEnglishLine methods, of class
     * org.renardo.multilangdocs.MultilangEntry.
     */
    @Test
    public void testGetAndSetEnglishLine() {
        System.out.println("getEnglishLine and setEnglishLine");
        MultilangEntry instance = new MultilangEntry(1);

        MultilangLine expResult = new MultilangLine(1);
        expResult.setText("Hello");
        instance.setEnglishLine(expResult);

        MultilangLine result = instance.getEnglishLine();
        assertEquals(expResult, result);
    }

    /**
     * Test of getFrenchLine and setFrenchLine methods, of class
     * org.renardo.multilangdocs.MultilangEntry.
     */
    @Test
    public void testGetAndSetFrenchLine() {
        System.out.println("getFrenchLine and setFrenchLine");
        MultilangEntry instance = new MultilangEntry(1);

        MultilangLine expResult = new MultilangLine(1);
        expResult.setText("Bonjour");
        instance.setFrenchLine(expResult);

        MultilangLine result = instance.getFrenchLine();
        assertEquals(expResult, result);
    }

    /**
     * Test of getGermanLine and setGermanLine methods, of class
     * org.renardo.multilangdocs.MultilangEntry.
     */
    @Test
    public void testGetAndSetGermanLine() {
        System.out.println("getGermanLine and setGermanLine");
        MultilangEntry instance = new MultilangEntry(1);

        MultilangLine expResult = new MultilangLine(1);
        expResult.setText("Guten Tag");
        instance.setGermanLine(expResult);

        MultilangLine result = instance.getGermanLine();
        assertEquals(expResult, result);
    }
}
