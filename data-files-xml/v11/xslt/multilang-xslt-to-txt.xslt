<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="text" encoding="iso-8859-1" indent="yes"/>

<xsl:template match="multilingual-document"><xsl:apply-templates /></xsl:template>

<xsl:template match="title">
title
<xsl:text>  </xsl:text><xsl:value-of select="."/>
</xsl:template>

<xsl:template match="english">
line<xsl:text>&#xA;</xsl:text>
<xsl:text>  </xsl:text>en<xsl:text>&#xA;    </xsl:text><xsl:value-of select="."/>
</xsl:template>

<xsl:template match="german">
<xsl:text>&#xA;</xsl:text>
<xsl:text>  </xsl:text>de<xsl:text>&#xA;    </xsl:text><xsl:value-of select="."/>
</xsl:template>

<xsl:template match="french">
<xsl:text>&#xA;</xsl:text>
<xsl:text>  </xsl:text>fr<xsl:text>&#xA;    </xsl:text><xsl:value-of select="."/>
</xsl:template>

</xsl:stylesheet>
