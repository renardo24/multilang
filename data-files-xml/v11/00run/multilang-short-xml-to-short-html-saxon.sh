## 2016-03-08

cd ..
mkdir ./tmp/out/short-html
java -jar ${HOME}/tools/saxon/saxon9he.jar -s:./xml/multilang-xslt-to-txt-bulb.xml -xsl:./xslt/multilang-short-xml-to-short-html.xsl -o:./tmp/out/short-html/bulb.html

echo -n "Press any key to exit ..."
read
