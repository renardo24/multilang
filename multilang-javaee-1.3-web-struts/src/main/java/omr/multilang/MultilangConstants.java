package omr.multilang;

public interface MultilangConstants {

	/**
	 * Forwards
	 */
	public static final String GLOBAL_FORWARD_home = "home";

	public static final String FORWARD_SUCCESS = "success";

	public static final String FORWARD_FAILURE = "failure";
}
