## leave the IN parameter out (-in ../../xml/short-xml/.xml) since we want
## to process all files in a specific directory
## 2003-02-05
## Updated 2007-03-27
cd ..
mkdir ../tmp/out/large-html
java org.apache.xalan.xslt.Process -PARAM input-dir ../xml -xsl ../xslt/multilang-short-xml-to-large-xml.xsl -out ../tmp/out/large-xml/-merged.xml -EDUMP dump.txt

echo -n "Press any key to exit ..."
read

