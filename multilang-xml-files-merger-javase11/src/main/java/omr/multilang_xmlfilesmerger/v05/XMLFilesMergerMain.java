package omr.multilang_xmlfilesmerger.v05;

/**
 * Runs the application.
 * <p>
 * Creation date: (05/02/03 11:42:27)
 *
 * @author: Olivier Renard
 */
public class XMLFilesMergerMain {
    private XMLFilesMergerModel theModel = null;

    /**
     * XmlFilesMergers constructor comment.
     */
    public XMLFilesMergerMain() {
        super();
    }

    /**
     * Starts the application.
     *
     * @param args an array of command-line arguments
     */
    public static void main(final String[] args) {
        XMLFilesMergerMain theApp = new XMLFilesMergerMain();
        theApp.init();
        try {
            XMLFilesMergerModel model = theApp.getModel();
            model.loadProperties();
            /*
             * // Useful code to test the loading of properties System.out.println(
             * "XML Version: " + model.getXmlVersion() ) ; System.out.println( "Encoding: "
             * + model.getEncoding() ) ; System.out.println( "Output filename: " +
             * model.getOutputFileName() ) ; System.out.println( "Public ID: " +
             * model.getPublicId() ) ; System.out.println( "Root output el.: " +
             * model.getRootOutputElement() ) ; System.out.println( "Start input el.: " +
             * model.getStartInputElement() ) ; System.out.println() ;
             *
             * System.out.println( "DOCTYPE PUBLIC: " + model.isDoctypePublic() ) ;
             * System.out.println( "DOCTYPE SYSTEM: " + model.isDoctypeSystem() ) ;
             * System.out.println( "Incl. DOCTYPE: " + model.isIncludeDoctype() ) ;
             * System.out.println( "Standalone: " + model.isStandalone() ) ;
             * System.out.println( "Validate input: " + model.isValidateInput() ) ;
             * System.out.println() ;
             *
             * System.out.println( "Input Dir: " + model.getInputDirectory() ) ;
             * System.out.println( "Output Dir: " + model.getOutputDirectory() ) ;
             * System.out.println( "System id: " + model.getSystemId() ) ;
             * System.out.println() ;
             *
             * System.out.println( "XML APIs: " + model.getXmlApis().length ) ; if (
             * model.getXmlApis() != null ) { int size = model.getXmlApis().length ; for (
             * int i = 0; i < size; i++ ) { System.out.println( model.getXmlApis()[ i ] ) ;
             * } } else { System.out.print( model.getXmlApis() ) ; }
             */

            model.generateOutputFile();
        } catch (Exception e) {
            System.err.println("Exception: " + e);
            e.printStackTrace(System.out);
        }
    }

    /**
     * Insert the method's description here. Creation date: (05/02/03 12:04:36)
     *
     * @return XmlFilesMergerModel
     */
    public XMLFilesMergerModel getModel() {
        return theModel;
    }

    /**
     * Insert the method's description here. Creation date: (05/02/03 12:04:08)
     */
    private void init() {
        // Create the model
        theModel = new XMLFilesMergerJDOMModel();
    }
}
