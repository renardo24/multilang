/*
 * MultilangDocumentDriver.java
 *
 * Created on 16 February 2006, 13:44
 */

package omr.multilangdom.driver;

import omr.multilangdom.MultilangDocument;
import omr.multilangdom.MultilangEntry;
import omr.multilangdom.MultilangLine;

/**
 * Driver for a multilang document
 *
 * @author Olivier Renard
 */
public class MultilangDocumentDriver {
    public static void main(final String[] args) {
        MultilangLine lineEN = new MultilangLine(System.currentTimeMillis());
        lineEN.setText("Hello");
        MultilangLine lineFR = new MultilangLine(System.currentTimeMillis());
        lineFR.setText("Bonjour");
        MultilangLine lineDE = new MultilangLine(System.currentTimeMillis());
        lineDE.setText("Guten Tag!");
        lineDE.setImage(new byte[]{1, 2, 3});
        MultilangEntry entry = new MultilangEntry(System.currentTimeMillis(), lineEN, lineFR, lineDE);
        MultilangDocument doc = new MultilangDocument(System.currentTimeMillis(), "Hello in various languages");
        doc.setFrenchTitle("Bonjour en plusieures langues");
        doc.addMultilangEntry(entry);
        doc.setGermanTitle("Guten Tag: verschiedene Sprachen");
        System.out.println(doc);
    }
}
