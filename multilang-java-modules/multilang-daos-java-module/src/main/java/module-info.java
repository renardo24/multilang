module multilang.daos.java9.module {
    requires jdom2;
    requires java.xml;
    requires multilang.domain.vos.java9.module;
}