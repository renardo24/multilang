README
Olivier Renard
2007-04-03


DESCRIPTION
    To store multilingual documents in (X)HTML format ("``*.html``",
    "``*.htm``" or "``*.xhtml``").


HISTORY
    -   2007-04-03: Created project (originally created 2003-07-22).


FILES
    -   000_template_lib_oli_lang.html (template)
    -   000_template_quadrilingual.html (template) (2002-11-25)
    -   css/000_template_lib_oli_lang.css (template)
    -   css/000_template_quadrilingual.css (template) (2001-09-12)
    -   bn_chocolates.html (2001-09-21)
    -   cinema_and_films.html
    -   colours.html (2001-09-21)
    -   cote_dor.html (2001-09-21)
    -   creme_a_recurer.html (2001-09-21)
    -   education.html (2001-09-21)
    -   eurostar_no_smoking_please.html (2001-09-21)
    -   eurostar_ticket_info.html (2001-09-21)
    -   leonidas.html (2001-06-30)
    -   liege.html (2001-09-22)
    -   marriage.html (2001-09-01)
    -   mr_piano.html
    -   quadrilingual.html and holyday_inn_japanese.gif were both created on
        2000-12-27. This document deals with 4 languages: English, French,
        German and Japanese.
    -   shorthand_notebook.html
    -   sole.html (2001-09-21)
    -   thomas_sit_and_ride.html (2001-10-12)
    -   trilingual_v01.html (2000-06-13)
    -   trilingual_v02.html (2000-07-06)
    -   trilingual_v03.html (2000-11-28)
    -   trilingual_v04.html (2001-06-28)
    -   walldorf.html (2001-09-21)
    -   winnie_musical_mobile.html
    -   wrapping_paper.html (2001-09-21)


------------------------------------ END -------------------------------------
