title
en
Oral-B Stage 2 Toothbrush

line
de
2-4 Jahre
en
2-4 years
fr
2-4 ans

line
de
F�r Kinder entwickelt, die lernen, ihre Z�hne selbst zu putzen.
en
Designed for a child learning to brush.
fr
Con�ue pour les enfants qui apprennent � se brosser les dents ...

line
de
Sie k�nnen bereits ein vollst�ndiges Milchzahngebiss haben.
en
May have full set of baby teeth.
fr
... et dont toutes les dents de lait peuvent �tre apparues.

line
de
Einzigartiger, speziell entwickelter GEPOLSTERTER B�RSTENKOPF - hilft
das empfindliche Zahnfleisch zu sch�tzen.
en
Unique CUSHIONED HEAD is designed to help protect tender gums.
fr
LA T�TE DE BROSSE � COUSSINET PROTECTEUR est con�ue pour prot�ger les
gencives d�licates.

line
de
POWER TIP Borsten, speziell entwickelt, um die schwer zu erreichenden
Backenz�hne zu reinigen.
en
POWER TIP bristles are designed to help clean hard to reach back teeth.
fr
LA POINTE POWER TIP anti-plaque dentaire est con�ue pour aider les
enfants � atteindre et � nettoyer efficacement les dents du fond.

line
de
SCHMALLER B�RSTENKOPF sorgt f�r einfachen Zugang zum kleinen Kindermund.
en
NARROW HEAD provides easy access for small mouths.
fr
LA T�TE �TROITE est con�ue pour les petites bouches.

line
de
EINFACH ZU HALTENDER GRIFF hilft Kindern das Z�hneputzen zu lernen.
en
EASY_TO_HOLD HANDLE to help a child learn to brush.
fr
SON MANCHE PERMET UNE PRISE EN MAIN facile qui aide les enfants �
apprendre � se brosser les dents.

line
de
Nur zum Z�hneputzen, nicht zum Kauen geeignet.
en
For toothbrushing only. Do not chew.
fr
� utiliser uniquement pour le brossage des dents. Ne pas m�cher.

line
en
Distributed by
fr
Distribut� par

line
de
Line #3 in German (if an image, then path to image between <>)
en
Made in Ireland
fr
Fabriqu� en Irlande

line
de
F�r weitere Mundhygiene Informationen, besuchen Sie uns unter
www.oral-bkids.com.
en
For more oral care information visit us at www.oral-bkids.com.
fr
Pour plus d'informations sur l'hygi�ne dentaire www.oral-bkids.com.

line
de
Die Zahnb�rsten-Marke, die Zahn�rzte am h�ufigsten benutzen.
en
The Brand More Dentists Use Themselves.

