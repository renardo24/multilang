import java.nio.file.*

String userHome = System.getProperty("user.home");
//String mainInputPath = "${userHome}/omr/04-archives/01-multilingual-texts";
String mainInputPath = "../test-data";
File inputDir = new File(mainInputPath, "for-clean-up");

String mainOutputPath = "./";
File outputDir = new File(mainOutputPath, "cleaned-up-txt");
if (outputDir.exists()) { outputDir.deleteDir(); }
if (!outputDir.exists()) { outputDir.mkdirs(); }
//return;
files = inputDir.listFiles();
assert files.size() > 0;

files.each() { file ->
  if (file.isFile() && file.name.endsWith("txt")) {
    newLines = [];
    println(file);
    lines = file.readLines("ISO-8859-1");
    lines.each() { line ->
     //println(line);
     String newLine = line.trim();
     if (newLine =~ /^$/) { return; }
     if (newLine =~ /^line$/) { newLine = "\nline"; }
     newLine += "\n";
     newLines << newLine;
     //println(newLine);
    }
    //println(newLines);
    String finalOutput = "";
    newLines.each() {
      finalOutput += it;
    }
    finalOutput += "\n";
    //println(finalOutput);
    new File(outputDir, "${file.name}").write(finalOutput, "ISO-8859-1");
  }
}
