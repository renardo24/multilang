README


% mode line for jEdit; line starting with a percent
% sign is a comment for txt2tags
% :mode=text:indentSize=2:folding=indent:
% :maxLineLen=72:noTabs=true:tabSize=2:wrap=hard:


Created
=======
2007-05-29


Description
===========
Multilingual documents stored in plain text format. The aim is to store
multilingual documents in plain text format ("``*.txt``"), as well as
the specification for such files.


History
=======
- 2007-07-17: Added ``multilang_samsung_clp300.txt``, ``multilang_samsung_printer_info_windows_vista.txt``, ``multilang_spider_man_chocolate.txt``, ``multilang_tomy_pop_up_pirate.txt``, ``multilang_p_and_o_ferries_departure_tag.txt``, ``multilang_p_and_o_ferries_ticket.txt``.
- 2007-03-23: Added ``multilang_bouchees.txt``, ``multilang_choco_leibniz.txt``, ``multilang_cote_dor_double_lait.txt``, ``multilang_cote_dor_mignonette.txt``, ``multilang_happy_meal_plastic_bag.txt``, ``multilang_herlitz_pen.txt``, ``multilang_ikea_lack_shelf.txt``, ``multilang_kettle_crisps.txt``, ``multilang_kodak_cdr.txt``, ``multilang_leukotape_adhesive_tape.txt``, ``multilang_maynards_wine_gums.txt``, ``multilang_plastic_bag.txt``, ``multilang_samsung_cd_recordable.txt``, ``multilang_tv_video_corner_unit.txt``, ``multilang_verbatim_dvdr.txt``, ``multilang_volumax_laser_printer_paper.txt``, ``multilang_winnie_balloons.txt``, ``multilang_xerox_laser_printer_paper.txt``, ``multilang_cdr_box.txt``, ``multilang_kinder_surprise_egg.txt``, ``multilang_huggies_pull_ups.txt``, ``multilang_iron.txt``, ``multilang_quality.txt``, ``multilang_ikea_bulb.txt``, ``multilang_gillette.txt``, ``multilang_bed_linen.txt``, ``multilang_baby_wipes.txt``. Updated ``multilang_colours.txt``, ``multilang_diskettes_box.txt``.
- 2007-03-22: Added ``multilang_bouncing_buggy.txt``, ``multilang_tuneyville_choo_choo.txt``, ``multilang_stereo_headphones.txt``, ``multilang_nike_polo.txt``, ``multilang_techno_fleece.txt``, ``multilang_gillette_blue2.txt``, ``multilang_stapler_box.txt``, ``multilang_landing_card.txt``, ``multilang_mignonette_cote_dor.txt``, ``multilang_plaster_box.txt``, ``multilang_photocopier.txt``, ``multilang_diskettes_box.txt``, ``multilang_plastic_bag.txt``, ``multilang_cinema_and_films.txt``, ``multilang_education.txt``.
- 2007-03-21: Added ``holyday_inn_japanese.gif``, ``multilang_holyday_inn.txt``.
- 2007-03-20: Added ``multilang-niceday-scissors-deluxe.txt``, ``multilang_eurostar_ticket_info.txt``, ``multilang_ratchet_secateur.txt``, ``multilang_eurostar_no_smoking_please.txt``, ``multilang_ensor.txt``, ``multilang_dancing_caterpillar.txt``, ``multilang_diary.txt``, ``multilang_duracell_batteries.txt``, ``multilang_cote_dor_chocolate.txt``, ``multilang_bn_chocolate.txt``, ``multilang_galler_chocolate.txt``, ``multilang_colours.txt``, ``multilang_thomas_sit_and_ride.txt``, ``multilang_marriage.txt``, ``multilang_walldorf.txt``, ``multilang_leonidas.txt``, ``multilang_liege.txt``.
- 2007-01-09: Added ``multilang-flower-food.txt`` and ``multilang-niceday-memo-pads.txt``.
- 2007-01-02: Added ``multilang-toy-plastic-bag.txt``, ``multilang-bouchees-cote-dor.txt``, ``multilang-disney-store-elephant-fluffy-toy.txt``, ``multilang-huggies-super-flex.txt``, ``multilang-speculoos-lotus.txt``, ``multilang-mcdonalds-toy-plastic-bag.txt``, ``multilang-pampers-newbaby-newborn.txt`` and ``multilang-lego-city-police-car-7236.txt``.
- 2006-12-21: Added ``multilang-orange-bic-box.txt`` and ``multilang-graphix-super-value-pack.txt``.
- 2006-12-18: Added ``multilang-mr-piano.txt`` and ``mr-piano.png`` image, and ``multilang-winnie-musical-mobile.txt`` and ``winnie-music-mobile.png``.
- 2006-12-15: Added ``multilang-oralb-stage2-toothbrush.txt``.
- 2006-12-04: Added ``multilang-burger-king-toy-bag.txt``, ``multilang-soft-trainer-seat.txt``, ``multilang-babyliss-paris.txt``, ``multilang-archaeology.txt``, ``multilang-bear-castle.txt``, ``multilang-creme-a-recurer.txt``, ``multilang-grangala-biscuits.txt``, ``multilang-indoor-antenna.txt``.
- 2006-11-29: Added ``multilang-niceday-notes.txt``.
- 2006-11-20: Added ``multilang-verbatim-5-pack-dvd-rw-box.txt``.
- 2006-11-15: Added ``multilang-case-logic-cd-wallet.txt``.
- 2006-11-13: Added ``multilang-tdk-cd-dvd-pen.txt`` and ``multilang-staedtler-graphite-779.txt``.
- 2006-11-08: Added ``multilang-imation-cdr.txt``.
- 2006-11-02: Created project.


Documents
=========
+ [Adidas hat ./src/multilang-adidas-hat.txt]
+ [Archaeology ./src/multilang-archaeology.txt]
+ [BaByliss Paris Hairdryer ./src/multilang-babyliss-paris.txt]
+ [Bear Castle ./src/multilang-bear-castle.txt]
+ [BIC Select (Rondo) ./src/multilang-bic-select-rondo.txt]
+ [Braun multiquick blender ./src/multilang-braun-multiquick-blender.txt]
+ [Brio activity bath toys ./src/multilang-brio-activity-bath-toys.txt]
+ [Burger King toy bag ./src/multilang-burger-king-toy-bag.txt]
+ [Case Logic CD wallet ./src/multilang-case-logic-cd-wallet.txt]
+ [Chicco highchair ./src/multilang-chicco-highchair.txt]
+ [Clear document folder ./src/multilang-clear-document-folder.txt]
+ [Cr�me � R�curer ./src/multilang-creme-a-recurer.txt]
+ [Days of the week ./src/multilang-weekdays.txt]
+ [Filofax Kent personal organiser ./src/multilang-filofax-kent.txt]
+ [Gift wrap ./src/multilang-gift-wrap.txt]
+ [Grangala biscuits ./src/multilang-grangala-biscuits.txt]
+ [Graphix super value pack ./src/multilang-graphix-super-value-pack.txt]
+ [Hitachi video cassette recorder ./src/multilang-hitachi-video-cassette-recorder.txt]
+ [Hot Wheels gorilla attack ./src/multilang-hot-wheels-gorilla-attack.txt]
+ [Household wipes ./src/multilang-household-wipes.txt]
+ [Imation CD-R ./src/multilang-imation-cdr.txt]
+ [Indoor antenna ./src/multilang-indoor-antenna.txt]
+ [Keyboard box ./src/multilang-keyboard-box.txt]
+ [Kinder chocolate box ./src/multilang-kinder-chocolate-box.txt]
+ [Ladder info ./src/multilang-ladder-info.txt]
+ [Lee Cooper trousers ./src/multilang-lee-cooper-trousers.txt]
+ [Levis trousers ./src/multilang-levis-trousers.txt]
+ [Massimo Dutti trousers tags ./src/multilang-massimo-dutti-trousers-tags.txt]
+ [Months of the year ./src/multilang-months.txt]
+ [Mr. Piano ./src/multilang-mr-piano.txt]
+ [Narnia Peter's sword and shield ./src/multilang-narnia-peters-sword-and-shield.txt]
+ [Niceday notes ./src/multilang-niceday-notes.txt]
+ [Niceday screen wipes ./src/multilang-niceday-screen-wipes.txt]
+ [Oral-B Stage 2 toothbrush ./src/multilang-oralb-stage2-toothbrush.txt]
+ [Orange Bic box ./src/multilang-orange-bic-box.txt]
+ [Pentel automatic pencil ./src/multilang-pentel-automatic-pencil.txt]
+ [Shoe sole ./src/multilang-shoe-sole.txt]
+ [Shorthand notebook ./src/multilang-shorthand-notebook.txt]
+ [Soft trainer seat ./src/multilang-soft-trainer-seat.txt]
+ [Sony DVD-R Box ./src/multilang-sony-dvdr-box.txt]
+ [Sony Ericsson Battery ./src/multilang-sony-ericsson-battery.txt]
+ [Staedtler Graphite 779 (0.7 mm; HB) ./src/multilang-staedtler-graphite-779.txt]
+ [Staple extractor ./src/multilang-staple-extractor.txt]
+ [Tag on Tigger toy ./src/multilang-tag-on-tigger-toy.txt]
+ [TDK CD/DVD Pen ./src/multilang-tdk-cd-dvd-pen.txt]
+ [Verbatim 5-Pack DVD-RW box ./src/multilang-verbatim-5-pack-dvd-rw-box.txt]
+ [Verbatim 50-CD SpindLe ./src/multilang-verbatim-50-cd-spindle.txt]
+ [Winnie Musical Mobile ./src/multilang-winnie-musical-mobile.txt]


Template
========
The ``src`` directory contains a [template ./src/000_template.txt] for
plain-text multilang documents (written on 2006-02-21).


Miscellaneous
=============
The following files used to be stored in Microsoft Word files, but have
been migrated to plain text files. Those files were part of a project
originally created on 2003-07-22:
- archaeology.doc (2001-04-28)
- babyliss-paris.doc (2001-05-20)
- bear-castle.doc (2001-04-28)
- grangala-biscuits.doc (2001-05-08)
- indoor-antenna.doc (2001-04-28)
- levis-jeans.doc  (2001-05-08)
- mr-piano.doc (2001-06-05)
- soft-trainer-seat.doc (2001-06-05)
- winnie-musical-mobile.doc (2001-04-27)

------------------------------------------------------------------------
