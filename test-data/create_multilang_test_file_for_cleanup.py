#!/bin/python3

import os;
import os.path;
import shutil;
import sys;

def main():
    sampleText = """

TitLE
    EN
	The title
Line
	De
		Deutsche Linie
	En
		English line
	Fr
		Ligne francaise
LINE
	DE
		Deutsche Linie #2
	EN
		English line #2
	FR
		Ligne francaise #2

LINE
	DE
            Deutsche Linie #3
	EN
		English line #3
	FR
		Ligne francaise #3
""";
    scriptDir = (os.path.dirname(os.path.realpath(__file__)));
    dirname = "./test-data";
    filename = "test-file.txt";
    
    path = os.path.normpath(os.path.join(scriptDir, dirname));
    #print(path);
    
    try:
        if (os.path.exists(path)):
            print("Removing dir");
            shutil.rmtree(path);
        if (not os.path.exists(path)):
            print("Creating dir");
            os.makedirs(path);
        path = os.path.normpath(os.path.join(path, filename));
        print("Path => " + path);
        theFile = open(path, "w");
        theFile.write(sampleText);
        theFile.close();
    except IOError as e:
        print("I/O error({0}): {1}".format(e.errno, e.strerror));
    except:
        print("Unexpected error:", sys.exc_info()[0]);
        raise;

if (__name__ == "__main__"):
    main();
