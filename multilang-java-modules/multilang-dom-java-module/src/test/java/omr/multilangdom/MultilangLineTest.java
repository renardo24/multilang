/*
 * MultilangLineTest.java
 * JUnit based test
 *
 * Created on 22 February 2006, 13:41
 */

package omr.multilangdom;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Olivier Renard
 */
public class MultilangLineTest {

    // public static Test suite() {
    // TestSuite suite = new TestSuite(MultilangLineTest.class);
    // return suite;
    // }

    /**
     * Test of equals method, of class org.renardo.multilangdocs.MultilangLine.
     */
    @Test
    public void testEqualsOnlyWithIdNoTextNoImage() {
        System.out.println("testEqualsOnlyWithIdNoTextNoImage");
        MultilangLine instance1 = new MultilangLine(1);
        MultilangLine instance2 = new MultilangLine(1);
        boolean expResult = true;
        boolean result = instance1.equals(instance2);
        assertEquals(expResult, result);
        result = instance2.equals(instance1);
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class org.renardo.multilangdocs.MultilangLine.
     */
    @Test
    public void testEqualsOnlyWithIdAndTextNoImage() {
        System.out.println("testEqualsOnlyWithIdAndTextNoImage");
        MultilangLine instance1 = new MultilangLine(1);
        MultilangLine instance2 = new MultilangLine(1);
        boolean expResult = true;

        instance1.setText("Olivier");
        instance2.setText("Olivier");
        boolean result = instance1.equals(instance2);
        assertEquals(expResult, result);
        result = instance2.equals(instance1);
        assertEquals(expResult, result);

        instance1.setText("Bla");
        instance2.setText("Bla ");
        expResult = false;
        result = instance1.equals(instance2);
        assertEquals(expResult, result);
        result = instance2.equals(instance1);
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class org.renardo.multilangdocs.MultilangLine.
     */
    @Test
    public void testEqualsOnlyWithIdAndTextInOneObjectAndNullTextInOtherObject() {
        System.out.println("testEqualsOnlyWithIdAndTextInOneObjectAndNullTextInOtherObject");
        MultilangLine instance1 = new MultilangLine(1);
        MultilangLine instance2 = new MultilangLine(1);
        boolean expResult = false;

        instance1.setText("Olivier");
        instance2.setText(null);
        boolean result = instance1.equals(instance2);
        assertEquals(expResult, result);
        result = instance2.equals(instance1);
        assertEquals(expResult, result);

        instance1.setText(null);
        instance2.setText("Bla");
        expResult = false;
        result = instance1.equals(instance2);
        assertEquals(expResult, result);
        result = instance2.equals(instance1);
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class org.renardo.multilangdocs.MultilangLine.
     */
    @Test
    public void testEqualsOnlyWithIdAndImageNoText() {
        System.out.println("testEqualsOnlyWithIdAndImageNoText");
        MultilangLine instance1 = new MultilangLine(1);
        MultilangLine instance2 = new MultilangLine(1);
        boolean expResult = true;
        boolean result = false;

        byte[] bytes1 = new byte[]{1, 2, 3};

        instance1.setImage(bytes1);
        instance2.setImage(bytes1);
        result = instance1.equals(instance2);
        assertEquals(expResult, result);
        result = instance2.equals(instance1);
        assertEquals(expResult, result);

        byte[] bytes2 = new byte[]{2, 3, 4};

        instance2.setImage(bytes2);
        expResult = false;
        result = instance1.equals(instance2);
        assertEquals(expResult, result);
        result = instance2.equals(instance1);
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class org.renardo.multilangdocs.MultilangLine.
     */
    @Test
    public void testEqualsOnlyWithIdAndImageInOneObjectAndNullImageInOtherObject() {
        System.out.println("testEqualsOnlyWithIdAndImageInOneObjectAndNullImageInOtherObject");
        MultilangLine instance1 = new MultilangLine(1);
        MultilangLine instance2 = new MultilangLine(1);
        boolean expResult = false;

        byte[] bytes1 = new byte[]{1, 2, 3};

        instance1.setImage(bytes1);
        instance2.setImage(null);
        boolean result = instance1.equals(instance2);
        assertEquals(expResult, result);
        result = instance2.equals(instance1);
        assertEquals(expResult, result);

        byte[] bytes2 = new byte[]{2, 3, 4};

        instance1.setImage(null);
        instance2.setImage(bytes2);
        expResult = false;
        result = instance1.equals(instance2);
        assertEquals(expResult, result);
        result = instance2.equals(instance1);
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class org.renardo.multilangdocs.MultilangLine.
     */
    @Test
    public void testEqualsOnlyWithIdAndTextAndImage() {
        System.out.println("testEqualsOnlyWithIdAndTextAndImage");
        MultilangLine instance1 = new MultilangLine(1);
        MultilangLine instance2 = new MultilangLine(1);
        boolean expResult = true;
        boolean result = false;

        byte[] bytes1 = new byte[]{1, 2, 3};

        instance1.setText("Olivier");
        instance1.setImage(bytes1);
        instance2.setText("Olivier");
        instance2.setImage(bytes1);
        result = instance1.equals(instance2);
        assertEquals(expResult, result);
        result = instance2.equals(instance1);
        assertEquals(expResult, result);

        byte[] bytes2 = new byte[]{2, 3, 4};

        instance2.setImage(bytes2);
        expResult = false;
        result = instance1.equals(instance2);
        assertEquals(expResult, result);
        result = instance2.equals(instance1);
        assertEquals(expResult, result);
    }

    /**
     * Test of hashCode method, of class org.renardo.multilangdocs.MultilangLine.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        MultilangLine instance = new MultilangLine(1);
        int expResult = 5202;
        int result = instance.hashCode();
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class org.renardo.multilangdocs.MultilangLine.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        MultilangLine instance = new MultilangLine(1);
        instance.setText("Olivier");
        StringBuffer buf = new StringBuffer();
        buf.append("\t\tTEXT: Olivier");
        buf.append(System.getProperty("line.separator"));
        String expResult = buf.toString();
        String result = instance.toString();
        assertNotNull(result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getLineId method, of class org.renardo.multilangdocs.MultilangLine.
     */
    @Test
    public void testGetLineId() {
        System.out.println("getLineId");
        MultilangLine instance = new MultilangLine(1);
        long expResult = 1L;
        long result = instance.getLineId();
        assertEquals(expResult, result);
    }

    /**
     * Test of setLineId method, of class org.renardo.multilangdocs.MultilangLine.
     */
    @Test
    public void testSetLineId() {
        System.out.println("setLineId");
        MultilangLine instance = new MultilangLine(1);
        long expResult = 2L;
        instance.setLineId(expResult);
        long result = instance.getLineId();
        assertEquals(expResult, result);
    }

    /**
     * Test of getText method, of class org.renardo.multilangdocs.MultilangLine.
     */
    @Test
    public void testGetText() {
        System.out.println("getText");
        MultilangLine instance = new MultilangLine(1);
        instance.setText("Olivier");
        String expResult = "Olivier";
        String result = instance.getText();
        assertEquals(expResult, result);

        instance.setText("Bla");
        result = instance.getText();
        assertTrue(!expResult.equals(result));
    }

    /**
     * Test of setText method, of class org.renardo.multilangdocs.MultilangLine.
     */
    @Test
    public void testSetText() {
        System.out.println("setText");
        String pText = "Bla";
        MultilangLine instance = new MultilangLine(1);
        instance.setText(pText);
        String result = instance.getText();
        assertEquals(pText, result);
    }

    /**
     * Test of getImage method, of class org.renardo.multilangdocs.MultilangLine.
     */
    @Test
    public void testGetImage() {
        System.out.println("getImage");
        MultilangLine instance = new MultilangLine(1);
        byte[] expResult = new byte[]{1, 2, 3};
        instance.setImage(expResult);
        byte[] result = instance.getImage();
        assertEquals(expResult, result);
    }

    /**
     * Test of setImage method, of class org.renardo.multilangdocs.MultilangLine.
     */
    @Test
    public void testSetImage() {
        System.out.println("setImage");
        byte[] pImage = new byte[]{1, 2, 3};
        MultilangLine instance = new MultilangLine(1);
        instance.setImage(pImage);
        byte[] result = instance.getImage();
        assertEquals(pImage, result);
    }
}