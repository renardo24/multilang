/*
 * AllTestsSuite.java
 *
 * Created on 23 February 2006, 12:29
 */

package omr.multilangdom;

import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.SelectClasses;
import org.junit.runner.RunWith;

/**
 * Runs all tests in this package
 *
 * @author Olivier Renard
 */
@RunWith(JUnitPlatform.class)
@SelectClasses({MultilangDocumentTest.class, MultilangEntryTest.class, MultilangLineTest.class})
public class AllTestsSuite {
}
