<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xalan="http://xml.apache.org/xslt">

  <xsl:output method="html"
              encoding="UTF-8"
              indent="yes"
              xalan:indent-amount="2"/>

<!--
This stylesheet converts a short XML file into a short HTML file.
-->
    <xsl:template match="/multilingual-document">
      <html>
        <head>
          <link rel="stylesheet" type="text/css" href="../../css/multilingual.css" media="screen" />
          <!-- Write the title of the document -->
          <title><xsl:value-of select="title" /></title>
        </head>
        <body>
          <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tbody>
              <tr>
                <th width="100%" align="left" valign="top">
                  <table border="1" cellpadding="5" cellspacing="0" width="100%" borderColor="#525D76">
                    <tbody>
                      <tr>
                        <!-- Write the title of the document -->
                        <th width="88%" align="left" valign="top" class="titleCell"><xsl:value-of select="title" /></th>
                        <!-- Write the date in DD/MM/YYYY format from the pub-date element -->
                        <th width="12%" align="center" valign="top" class="pubDateCell"><xsl:value-of select="pub-date/date" /><xsl:text>/</xsl:text><xsl:value-of select="pub-date/month" /><xsl:text>/</xsl:text><xsl:value-of select="pub-date/year" /></th>
                      </tr>
                    </tbody>
                  </table>
                </th>
              </tr>
              <tr>
                <td width="100%" align="left" valign="top">
                  <table border="0" cellpadding="4" cellspacing="0" width="100%" class="langTable">
                    <tbody>
                      <xsl:for-each select="multilingual-entry">
                        <xsl:apply-templates />
                        <tr>
                          <!-- &#160; is a non-breaking space and the &amp;nbsp; entity does not
                               work unless it is definied in the DTD, which is another option. -->
                          <td width="100%" colspan="2" valign="top" align="left" class="emptyCell">&#160;</td>
                        </tr>
                      </xsl:for-each>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
        </body>
      </html>
    </xsl:template>

<!--
Match the title element and suppress any output so
that it does not reappear anywhere else in the HTML
document.
"xsl:apply-templates" invokes a template for EVERY element
in the XML document and those elements for which there is no
template are simply output as is. The template prevents this
from happening for the title element.
-->
    <xsl:template match="title">
    </xsl:template>

<!--
Match the pub-date element and suppress any output so
that it does not reappear anywhere else in the HTML
document.
"xsl:apply-templates" invokes a template for EVERY element
in the XML document and those elements for which there is no
template are simply output as is. The template prevents this
from happening for the pub-date element.
-->
    <xsl:template match="pub-date">
    </xsl:template>

<!--
Process all children under the multilingual-entry element.
-->
    <xsl:template match="multilingual-entry">
      <xsl:apply-templates />
    </xsl:template>

    <xsl:template match="english">
      <tr>
        <td width="15%" valign="top" align="left" class="langCell">English</td>
        <td width="85%" valign="top" align="left" class="textCell"><xsl:value-of select="."/></td>
      </tr>
    </xsl:template>

    <xsl:template match="french">
      <tr>
        <td width="15%" valign="top" align="left" class="langCell">French</td>
        <td width="85%" valign="top" align="left" class="textCell"><xsl:value-of select="."/></td>
      </tr>
    </xsl:template>

    <xsl:template match="german">
      <tr>
        <td width="15%" valign="top" align="left" class="langCell">German</td>
        <td width="85%" valign="top" align="left" class="textCell"><xsl:value-of select="."/></td>
      </tr>
    </xsl:template>

    <xsl:template match="japanese">
      <tr>
        <td width="15%" valign="top" align="left" class="langCell">Japanese</td>
        <td width="85%" valign="top" align="left" class="textCell"><xsl:value-of select="."/></td>
      </tr>
    </xsl:template>

</xsl:stylesheet>