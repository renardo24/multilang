title
en
Samsung CLP-300 Colour Printer
fr
Imprimante couleure Samsung CLP-300

line
de
Bis zu 16 SpM S/W/4 SpM Farbe [A4]
en
Up to Fast 16ppm black 4ppm color [A4].
fr
Jusqu'� 16 ppm noir / 4ppm couleur [A4]

line
de
Die Druckgeschwindigkeit h�ngt von Betriebssystem, Rechenleistung,
Anwendung, Anschlussart, Medientyp, Medienformat und Komplexit� des
Druckauftrags ab.
en
Print speed will be affected by operating system used, computing
performance, application software, connecting method, media type, media
size and job complexity.
fr
La vitesse d'impressin varie selon le SE utilis�, rendement de l'appareil,
logiciel, m�thode de connexion, type et taille m�dia et complexit�
travail.

line
de
300-MHz-Hochleistungsprozessor
en
High performance 300 MHz processor
fr
Processeur hte. performance 300 MHz

line
de
Bis zu 2400 x 600 dpi effektive Ausgabe
en
Up to 2,400 x 600 dpi effective output
fr
Impr. effective 2 400 x 600 ppp

line
de
150 Blatt Papier Fachkapazit�t
en
150 sheets paper input capacity
fr
Capacit� alim. papier 150 feuilles

line
de
Papierformat: 3" x 5" bis 8,5" x 14" (Legal), 16~43lb (60 bis 163g/m�)
en
Paper Size: 3" x 5" bis 8,5" x 14" (Legal), 16~43lb (60 � 163g/m�)
fr
Taille papier: 3" x 5" bis 8,5" x 14" (Legal), 16~43lb (60 � 163g/m�)

line
de
Schneller USB 2.0 Anschluss
en
High-speed USB 2.0
fr
USB 2.0 gde. vitesse

line
de
Kompatibel zu Microsoft Windows 98/Me/2000/XP/2003, Mac OS 10.3~10.4 und
verschiedenen Linux
en
Compatible with Microsoft Windows 98/Me/2000/XP/2003, Mac OS 10.3~10.4 and
various Linux
fr
Compatible avec Microsoft Windows 98/Me/2000/XP/2003, SE Mac 10.3~10.4 et
plusieurs Linux

line
de
Branchenf�hrendes Tonersystem - einfach und zuverl�ssig
en
Industry Leading Toner System - Easy to Use & Reliable
fr
Syst�me encre leader secteur - Usage facile & fiable

line
de
Tonerwerte: 2.000 Seiten S/W, 1.000 Seiten jeweils (C, M, Y) bei 5 %
Deckung
en
Toner Yield: 2,000 pages black, 1,000 pages each (C, M, Y) at 5% coverage
fr
Rendement toner: 2 000 pages noir, 1 000 pages (C, M, Y) � 5% couv.

line
de
Druckleistung der Tonerkartusche aus dem Lieferumfang: 1.500 Seiten S/W,
700 Seiten jeweils (C, M, Y) bei 5 % Deckung
en
In-Box Toner Cartridge Yield: 1,500 pages black, 700 pages each (C, M, Y)
at 5% coverage
fr
Rendement de la cartouche de toner fournie: 1 500 pages noir, 700 pages
(C, M, Y) � 5% couv.
