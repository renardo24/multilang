var re = /\s*(.)\s*/ ;
var focusElement = null ;

function setFocus( element )
{
  focusElement = element ;
}

function addSpecialChar( button )
{
  if ( button != null )
  {
    var charToAdd = button.value ;

    if ( charToAdd != null )
    {
      charToAdd = charToAdd.replace( re, "$1" ) ;

      if ( focusElement != null )
      {
        // Add the special char to the text area.
        var currentText = focusElement.value ;
        currentText += charToAdd ;
        focusElement.value = currentText ;

        // Return the focus to the text area.
        focusElement.focus() ;
      }
      else
      {
        alert( "Unable to add '" + charToAdd + "' as the textarea '" + focusElement + "' is not defined." ) ;
      }
    }
    else
    {
      alert( "Unable to add a null character." ) ;
    }
  }
  else
  {
    alert( "Unable to get text from the special character button." ) ;
  }
}