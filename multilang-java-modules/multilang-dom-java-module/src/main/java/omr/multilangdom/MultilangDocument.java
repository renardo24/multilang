/*
 * MultilangDocument.java
 *
 * Created on 16 February 2006, 08:24
 */

package omr.multilangdom;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * A POJO that represents a multilang document.
 *
 * @author Olivier Renard
 */
public class MultilangDocument implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 6798824740109662719L;

    /**
     * Date format for the ISO 8601 date time.
     */
    private static final String ISO8601_DATE_TIME_FORMAT = "yyyy-MM-dd'T'hh:mm:ss";

    /**
     * The unique documentId of this document.
     */
    private long documentId = -1;

    /**
     * The English title.
     */
    private String englishTitle = null;

    /**
     * The French title.
     */
    private String frenchTitle = null;

    /**
     * The German title.
     */
    private String germanTitle = null;

    /**
     * The date of creation of the document.
     */
    private Date creationDate = null;

    /**
     * Multilang entries.
     */
    private List<MultilangEntry> multilangEntries = null;

    /**
     * Creates a new instance of <code>MultilangDocument</code> with a document
     * documentId and an English title.
     *
     * @param documentId   the document id, which is mandatory.
     * @param englishTitle the English title, which is mandatory.
     */
    public MultilangDocument(final long documentId, final String englishTitle) {
        super();
        multilangEntries = new ArrayList<MultilangEntry>();
        this.documentId = documentId;
        this.setDateOfCreation(new Date());
        this.setDocumentId(documentId);
        this.setEnglishTitle(englishTitle);
    }

    /**
     * Returns true if object is equal to this instance; false otherwise.
     *
     * @param object the object to make the comparison against.
     * @return true if object is equal to this instance; false otherwise.
     */
    @Override
    public boolean equals(final Object object) {
        if (object == null) {
            return false;
        }

        // Not strictly necessary, but often a good optimization
        if (this == object) {
            return true;
        }

        if (!(this.getClass()
                .equals(object.getClass()))) {
            return false;
        }

        MultilangDocument mdoc = (MultilangDocument) object;

        boolean result = false;
        boolean docIdEqual = documentId == mdoc.getDocumentId();
        boolean creationDateEqual = (creationDate == null) ? (mdoc.getDateOfCreation() == null)
                : (creationDate.equals(mdoc.getDateOfCreation()));
        boolean englishTitleEqual = (englishTitle == null) ? (mdoc.getEnglishTitle() == null)
                : (englishTitle.equals(mdoc.getEnglishTitle()));
        boolean frenchTitleEqual = (frenchTitle == null) ? (mdoc.getFrenchTitle() == null)
                : (frenchTitle.equals(mdoc.getFrenchTitle()));
        boolean germanTitleEqual = (germanTitle == null) ? (mdoc.getGermanTitle() == null)
                : (germanTitle.equals(mdoc.getGermanTitle()));
        boolean entriesEqual = (multilangEntries == null) ? (mdoc.getMultilangEntries() == null)
                : (multilangEntries.equals(mdoc.getMultilangEntries()));

        result = docIdEqual && creationDateEqual
                && englishTitleEqual
                && frenchTitleEqual
                && germanTitleEqual
                && entriesEqual;

        return result;
    }

    /**
     * Returns a String representation of this instance.
     *
     * @return a String representation of this instance.
     */
    @Override
    public String toString() {
        String sep = System.getProperty("line.separator");
        StringBuilder buf = new StringBuilder();
        buf.append("creation-date")
                .append(sep);
        buf.append("\t")
                .append(this.formatCreationDate())
                .append(sep);
        buf.append("title")
                .append(sep);
        buf.append("\t")
                .append("en")
                .append(sep);
        buf.append("\t\t")
                .append(englishTitle)
                .append(sep);
        if (frenchTitle != null) {
            buf.append("\t")
                    .append("fr")
                    .append(sep);
            buf.append("\t\t")
                    .append(frenchTitle)
                    .append(sep);
        }
        if (germanTitle != null) {
            buf.append("\t")
                    .append("de")
                    .append(sep);
            buf.append("\t\t")
                    .append(germanTitle)
                    .append(sep);
        }
        if ((multilangEntries != null) && (!multilangEntries.isEmpty())) {
            int numberOfEntries = multilangEntries.size();
            for (int i = 0; i < numberOfEntries; i++) {
                MultilangEntry entry = multilangEntries.get(i);
                if (entry != null) {
                    buf.append(entry.toString());
                }
            }
        }
        return buf.toString();
    }

    /**
     * Returns the hashcode of this instance.
     *
     * @return the hashcode of this instance.
     */
    @Override
    public int hashCode() {
        int hash = 1;
        int primeNumber = 17;

        hash = hash * primeNumber + ((int) documentId);
        hash = hash * primeNumber + ((englishTitle != null) ? englishTitle.hashCode() : 0);
        hash = hash * primeNumber + ((frenchTitle != null) ? frenchTitle.hashCode() : 0);
        hash = hash * primeNumber + ((germanTitle != null) ? germanTitle.hashCode() : 0);
        hash = hash * primeNumber + ((multilangEntries != null) ? multilangEntries.hashCode() : 0);

        return hash;
    }

    /**
     * Returns the document id.
     *
     * @return the document id.
     */
    public long getDocumentId() {
        return documentId;
    }

    /**
     * Sets the document id.
     *
     * @param documentId the document id.
     */
    public void setDocumentId(final long documentId) {
        this.documentId = documentId;
    }

    /**
     * Returns the English title.
     *
     * @return the English title.
     */
    public String getEnglishTitle() {
        return englishTitle;
    }

    /**
     * Sets the English title.
     *
     * @param englishTitle the English title.
     */
    public void setEnglishTitle(final String englishTitle) {
        this.englishTitle = englishTitle;
    }

    /**
     * Returns the French title.
     *
     * @return the French title.
     */
    public String getFrenchTitle() {
        return frenchTitle;
    }

    /**
     * Sets the French title.
     *
     * @param frenchTitle the French title.
     */
    public void setFrenchTitle(final String frenchTitle) {
        this.frenchTitle = frenchTitle;
    }

    /**
     * Returns the German title.
     *
     * @return the German title.
     */
    public String getGermanTitle() {
        return germanTitle;
    }

    /**
     * Sets the German title.
     *
     * @param germanTitle the German title.
     */
    public void setGermanTitle(final String germanTitle) {
        this.germanTitle = germanTitle;
    }

    /**
     * Returns the multilang entries.
     *
     * @return the multilang entries.
     */
    public List<MultilangEntry> getMultilangEntries() {
        return multilangEntries;
    }

    /**
     * Returns the document's date of creation.
     *
     * @return the document's date of creation.
     */
    public Date getDateOfCreation() {
        return creationDate;
    }

    /**
     * Sets the document's date of creation.
     *
     * @param creationDate the document's date of creation..
     */
    public void setDateOfCreation(final Date creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * Add a multilang entry to this multilang document. If the entry is null, it is
     * not added to this document.
     *
     * @param pMultilangEntry The multilang entry to add.
     */
    public void addMultilangEntry(final MultilangEntry pMultilangEntry) {
        if (pMultilangEntry != null) {
            multilangEntries.add(pMultilangEntry);
        }
    }

    /**
     * Formats the creation date in the date-time ISO 8601 format.
     *
     * @return the creation date formatted in the date-time ISO 8601 format.
     */
    private String formatCreationDate() {
        String output = null;
        if (creationDate == null) {
            creationDate = new Date();
        }
        SimpleDateFormat sdf = new SimpleDateFormat(ISO8601_DATE_TIME_FORMAT);
        output = sdf.format(creationDate);
        return output;
    }
}
