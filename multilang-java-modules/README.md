# Java SE Multilang POJOs (DAOs and VOs)

Originally created 2007-04-02

A library of DAOs (Data Access Objects) and VOs (Value Objects) for
multilingual texts using Java SE 1.4. The data is stored in XML
files. There is a dependency on JDOM to read and write the XML files.


## History

-   2019-06-04:
    -   uses Java 9 modules
    -   moved from JUnit 4.12 to JUnit 5.4.2
-   2019-06-03: refactored with Maven submodules
-	2018-05-18: refactored, moved to Java 1.8
-   2017-04-06: Moving to JDOM2 (2.0.6)
-   2016-03-10: Refactored
-   2007-04-02: Created project.
-   2003-08-27:
    -   Updated JavaDoc comments of MultilingualDocumentVO class.
    -   Removed getEntryAt(int index) from MultilingualDocumentVO class.
    -   Removed removeEntry(int index) from MultilingualDocumentVO class.
    -   Replaced removeEntry(long id) by removeEntryById(String entryId)
        in MultilingualDocumentVO class.
-   2003-08-26:
    -   Added setEntryById(String entryId, MultilingualEntryVO entryVO)
        method to MultilingualDocumentVO class.
    -   Added getEntryById(String entryId) method to
        MultilingualDocumentVO class.
-   2003-08-14:
    -   Tidied up equals method of MultilingualDocumentVO class.
    -   Tidied up equals method of MultilingualEntryVO class.
    -   Ensures that a MultilingualDocumentVO always has a List object
        available when it is first created (avoids NPEs).
    -   Implements hashCode method in MultilingualEntryVO class.
    -   Implements hashCode method in MultilingualDocumentVO class.
-   2003-08-07: Uses JDOM 1.0.
-   2003-04-03: Uses DOM.

