package omr.multilang.dao;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.Namespace;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

import omr.multilang.vo.MultilingualDocumentVO;
import omr.multilang.vo.MultilingualEntryVO;

/**
 * Implementation of the <code>MultilingualDocumentDAOIF</code> interface for
 * the Data Access Object to be used for MultilingualDocumentVO objects.
 * 
 * @author Olivier Renard
 * @version 1.0 - 06/08/2003
 */
public class MultilingualDocumentDAOImpl implements MultilingualDocumentDAOIF {
	/**
	 * Reads data from an XML file and creates a MultilingualDocumentVO object
	 * from the information contained in the XML file.
	 * 
	 * @param path
	 *            the path to the XML file.
	 * @return a MultilingualDocumentVO with the data contained in the XML file.
	 * @throws DataAccessException
	 *             if something goes wrong during the load.
	 */
	public MultilingualDocumentVO load(String path) throws DataAccessException {
		MultilingualDocumentVO docVO = null;
		long id = 0;

		if (path != null) {
			SAXBuilder builder = new SAXBuilder();
			try {
				docVO = new MultilingualDocumentVO();
				Document doc = builder.build(new File(path));
				Element root = doc.getRootElement();

				// Set the id
				id = (Long.valueOf(root.getAttributeValue("id"))).longValue();
				docVO.setId(id);

				// Set the title
				List titles = root.getChildren("title");
				docVO.setTitle(((Element) titles.get(0)).getText());

				// Set the entries
				List entries = root.getChildren("entry");
				int nbrEntries = entries.size();
				for (int i = 0; i < nbrEntries; i++) {
					Element entry = (Element) entries.get(i);

					MultilingualEntryVO entryVO = new MultilingualEntryVO();

					id = (Long.valueOf(entry.getAttributeValue("id")))
							.longValue();
					entryVO.setId(id);

					List lines = entry.getChildren("line");
					entryVO.setEnglishText(((Element) lines.get(0)).getText());
					entryVO.setFrenchText(((Element) lines.get(1)).getText());
					entryVO.setGermanText(((Element) lines.get(2)).getText());
					entryVO.setJapaneseText(((Element) lines.get(3)).getText());
					docVO.addEntry(entryVO);
				}
			} catch (JDOMException jdome) {
				throw new DataAccessException(jdome.getMessage());
			} catch (IOException ioe) {
				throw new DataAccessException(ioe.getMessage());
			}
		} else {
			throw new DataAccessException("The path cannot be null.");
		}

		return docVO;
	}

	/**
	 * Save a multilingual document value object to a particular location
	 * defined by the path argument.
	 * 
	 * @param text
	 *            the multilingual document value object to save.
	 * @param path
	 *            the location where the multilingual document value object is
	 *            to be saved.
	 * @return true if the save was successful.
	 * @throws DataAccessException
	 *             if something goes wrong during the save.
	 */
	public boolean save(MultilingualDocumentVO text, String path)
			throws DataAccessException {
		if (text == null || path == null || path.trim().length() == 0) {
			throw new DataAccessException(
					"Invalid path or multilingual document value object.");
		}

		Element root = new Element("text");
		Document doc = new Document(root);
		root.setAttribute("id", "" + text.getId());

		Element title = new Element("title");
		title.setAttribute("lang", "en", Namespace.XML_NAMESPACE);
		title.setText(text.getTitle());
		root.addContent(title);

		List entries = text.getEntries();
		int nbrEntries = entries.size();
		for (int i = 0; i < nbrEntries; i++) {
			MultilingualEntryVO entryVO = (MultilingualEntryVO) entries.get(i);
			Element entry = new Element("entry");
			entry.setAttribute("id", "" + entryVO.getId());

			String enText = entryVO.getEnglishText();
			Element enLine = new Element("line");
			enLine.setText(enText);
			enLine.setAttribute("lang", "en", Namespace.XML_NAMESPACE);
			entry.addContent(enLine);

			String frText = entryVO.getFrenchText();
			Element frLine = new Element("line");
			frLine.setText(frText);
			frLine.setAttribute("lang", "fr", Namespace.XML_NAMESPACE);
			entry.addContent(frLine);

			String deText = entryVO.getGermanText();
			Element deLine = new Element("line");
			deLine.setText(deText);
			deLine.setAttribute("lang", "de", Namespace.XML_NAMESPACE);
			entry.addContent(deLine);

			String jaText = entryVO.getJapaneseText();
			Element jaLine = new Element("line");
			jaLine.setText(jaText);
			jaLine.setAttribute("lang", "ja", Namespace.XML_NAMESPACE);
			entry.addContent(jaLine);

			root.addContent(entry);
		}

		try {
			FileOutputStream out = new FileOutputStream(path);
			XMLOutputter outputter = new XMLOutputter();
			Format format = Format.getPrettyFormat();
			format.setEncoding("UTF16");
			format.setIndent("  ");
			outputter.output(doc, out);
			out.flush();
		} catch (IOException ioe) {
			throw new DataAccessException(ioe.getMessage());
		}

		return true;
	}
}