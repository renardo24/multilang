<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- 2001-11-28 -->
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xalan="http://xml.apache.org/xslt">

	<xsl:output method="html" 
							encoding="UTF-8"
							indent="yes" 
							xalan:indent-amount="2"/>

	<xsl:template match="/">
		<html>
			<head>
				<title>
				  Multilingual Texts and Sentences
				</title>
				<link rel="stylesheet" type="text/css" href="..\..\style\html\screen.css" media="screen"/>
				<link rel="stylesheet" type="text/css" href="..\..\style\html\print.css" media="print"/>
			</head>
			<body>
				<a name="top"><h1>Multilingual Texts and Sentences</h1></a>
				
				<xsl:choose>
					<!-- i.e. this is a large document made up of smaller ones -->
					<xsl:when test="multilingual_entries"> 
						<xsl:call-template name="create_links"/>
						<xsl:call-template name="handle_large_document"/>
					</xsl:when>
					<!-- i.e. this is a small document -->
					<xsl:otherwise>
						<xsl:call-template name="handle_small_document"/>
					</xsl:otherwise>
				</xsl:choose>
			</body>
		</html>
	</xsl:template>

	<!--
		CREATE THE LINKS INSIDE THE DOCUMENT
		TO MOVE FROM ONE SMALL DOCUMENT TO
		ANOTHER
	-->
	<xsl:template name="create_links">
		<div style="background-color: #eeeeee; border: 1px solid #999999; padding: 1%;">
			<ol>
				<xsl:for-each select="multilingual_entries/multilingual_entry">
					<li>
						<a href="{concat('#', @url)}">
							<xsl:for-each select="titles/title[. != '']">
								<xsl:value-of select="."/>
								<xsl:if test="position()!=last()"> - </xsl:if>
							</xsl:for-each>
						</a>
					</li>
				</xsl:for-each>
			</ol>
		</div>
		<hr/>
	</xsl:template>
	
	<xsl:template name="handle_large_document">
		<xsl:for-each select="multilingual_entries/multilingual_entry">
			<xsl:apply-templates select="."/>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="handle_small_document">
		<xsl:for-each select="multilingual_entry">
			<xsl:apply-templates select="."/>
		</xsl:for-each>
	</xsl:template>
	
	<xsl:template match="multilingual_entry">
	<!--
		CREATE AN ANCHOR WITH A NAME FOR EACH OF THE
		TITLES IN THE MULTILINGUAL-ENTRY THAT IS NOT
		AN EMPTY STRING (I.E. THAT CONTAINS TEXT).
	-->
		<a name="{@url}">
			<h2>
				<xsl:for-each select="titles/title[. != '']">
					<xsl:value-of select="."/>
					<xsl:if test="position()!=last()"> - </xsl:if>
				</xsl:for-each>
			</h2>
		</a>
		<table border="0" width="100%" cellspacing="0" cellpadding="0" class="bordermother">
			<tr>
				<th>No.</th>
				<xsl:for-each select="titles/title">
					<th><xsl:value-of select="@lang"/></th>
				</xsl:for-each>
			</tr>
			<xsl:for-each select="entry">
				<tr>
					<td width= "4%" valign="top"><b><xsl:value-of select="position()"/></b></td>
					<td width="24%" valign="top">
						<xsl:choose>
							<xsl:when test="line[@lang='English'] != ''">
								<xsl:value-of select="line[@lang='English']"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>---</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td width="24%" valign="top">
						<xsl:choose>
							<xsl:when test="line[@lang='French'] != ''">
								<xsl:value-of select="line[@lang='French']"/>											
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>---</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td width="24%" valign="top">
						<xsl:choose>
							<xsl:when test="line[@lang='German'] != ''">
								<xsl:value-of select="line[@lang='German']"/>											
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>---</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</tr>
			</xsl:for-each>
		</table>
		<!-- PRINT THE LINK TO THE TOP OF THE DOCUMENT -->
		<br/>
		<div align="right" style="background-color: #dddddd; border-top: 1px solid #545454; border-bottom: 1px solid #545454; padding: 0.6%;">
			<a href="#top">TOP</a>
		</div>
		<hr style="margin-bottom: 2%;"/>
	</xsl:template>

</xsl:stylesheet>
