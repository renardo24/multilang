# Multilang docs XML2any (v04)

## Created
2007-06-21 (originally created 2003-08-26).

## Description
Version 04 is very similar to version 03. The difference is that every
<entry> tag has a "id" attribute with a unique value.

Used service at http://www.hitsw.com/xml_utilites/ to generate DTD.

