
MULTILINGUAL TEXTS
------------------


What is it?
-----------

Collection of HTML and XML documents containing texts in English,
French, German and Japanese.

This project was originally started on 22/07/2003.


Running
-------

The 'index.html' file contains a complete listing of all the examples
included in this project. The 'index.html' file is in the root directory
of this project.


Building
--------

To build the whole project, ANT 1.6 or above is required. Simply run the
'build.xml' file in the root directory of this project.
