title
de
Sony Ericsson Akku - Standard-Akku
en
Sony Ericsson Battery - Standard Battery
fr
Batterie Sony Ericsson - Batterie Standard

line
de
Achtung! Vorsicht beim �ffnen!
en
Use caution when opening!
fr
Attention! Prenez quelques pr�cautions lors de l'ouverture de cet emballage.

line
de
Ein Sony Ericsson Akku hat bei sachgerechter Behandlung eine lange Lebensdauer.
fr
Une batterie rechargeable Sony Ericsson peut durer tr�s longtemps si elle est utilis�e de mani�re ad�quate.

line
de
In der Telefon-Bedienungsanleitung finden Sie Informationen zum Akku und zum Laden.
fr
Consultez le guide de l'utilisateur du t�l�phone pour de plus amples informations sur la batterie et les op�rations de charge.

line
de
Laden Sie den Akku nach dem Erwerb vollst�ndig auf.
fr
Effectuez une charge compl�te de la batterie apr�s son achat.

line
de
Das Laden sollte bei Temperaturen zwischen +5� C et +45� C erfolgen.
fr
Rechargez-la � des temp�ratures comprises entre +5�C et +45�C.

line
de
Gespr�chs- und Standby-Dauer sind von den jeweiligen Einsatzbedingungen und von der Netzkonfiguration abh�ngig.
fr
L'autonomie en veille et en communication d�pend des conditions d'utilisation et des caract�ristiques du r�seau.

line
de
Warnung! Explosionsgefahr in offenen Flammen.
fr
Attention! La batterie peut exploser si elle est jet�e au feu.

line
de
verwenden Sie ausschlie�lich Ericsson oder Sony Ericsson Akkus und Ladeger�te, dir f�r dieses Mobiltelefon entwickelt wurden.
fr
N'utilisez que des batteries et des chargeurs d'origine Ericsson ou Sony Ericsson, con�us pour �tre utilis�s avec notre portable.

line
de
Der Einsatz anderer Ladeger�te kann zu unzureichender Spannung oder �berm��iger Hitzeentwicklung f�hren.
fr
Les autres chargeurs pourraient ne pas charger correctement la batterie ou d�gager une chaleur excessive.

line
de
Die Verwendung anderer Akkus und Ladeger�te ist gef�hrlich.
fr
L'utilisation d'autres batteries ou d'autres chargeurs pourraient s'av�rer dangereuse.

line
de
Verwenden Sie den Akku ausschlie�lich f�r den vorgesehenden Einsatzzweck.
fr
Utilisez la batterie pour l'usage auquel elle est destin�e, � l'exclusion de tout autre.

line
de
Schalten Sie das Telefon aus, bevor Sie den Akku entnehmen.
fr
Eteignez le portable avant de retirer la batterie.

line
de
Setzen Sie den Akku keinen extremen Temperaturen aus (niemals �ber +60� C).
fr
N'exposez pas la batterie � des temp�ratures extr�mes, jamais au-dessus de +60�C).

line
de
Setzen Sie den Akku keinen Fl�ssigkeiten aus.
fr
Ne mettez jamais la batteries au contact d'un liquide.

line
de
Die Metallkontakte am Akku d�rfen keine anderen Metallobjekte ber�hren.
fr
Ne mettez pas les p�les de la batterie en contact avec un objet m�tallique.

line
de
Bauen Sie den Akku nicht auseinander und nehmen Sie keine Modifikationen vor.
fr
N'essayez pas d'ouvrir la batterie ou de lui apporter des modifications.

line
de
Bewahren Sie den Akku au�erhalb des Reichweite von Kindern auf.
fr
Conservez la batterie hors de port�e des enfants.

line
de
Nehmen Sie den Akku nicht in den Mund.
fr
Ne portez jamais la batterie � la bouche.

line
de
Entsorgen Sie den Akku nicht mit dem Hausm�ll.
fr
Ne jetez pas la batterie dans les lieux de collecte d'ordures m�nag�res.

line
de
Beachten Sie die lokalen Vorschriften zur Entsorgung von Akkus.
fr
Renseignez-vous sur la r�glementation en vigeur dans votre localit� concernant le traitement des batteries.



