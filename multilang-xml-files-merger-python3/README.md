# xml files merge

Merges various XML files together.
Use various implementations to make this work:

* `xml.dom.minidom`
* `xml.etree.ElementTree`
* `xmltodict` (requires virtual environment; see `requirements.txt`)

To make sure this works, define this first.

```
export XML_DATA_FILES_PATH=./test-data-xml-files-merger
```