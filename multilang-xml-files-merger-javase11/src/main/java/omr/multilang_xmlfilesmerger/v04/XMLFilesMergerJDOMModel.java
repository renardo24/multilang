package omr.multilang_xmlfilesmerger.v04;

import org.jdom2.*;
import org.jdom2.input.SAXBuilder;
import org.jdom2.input.sax.XMLReaders;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * Create a large XML file based on the input from several smaller XML files
 * using JDOM.
 * <p>
 * Creation date: (06/02/03 10:18:05)
 *
 * @author: Olivier Renard
 */
public class XMLFilesMergerJDOMModel extends XMLFilesMergerModel {
    /**
     * XMLFilesMergerModelJDOM constructor comment.
     */
    public XMLFilesMergerJDOMModel() {
        super();
    }

    /**
     * Checks where the data (parameters) to be used for generating the output XML
     * file is valid.
     * <p>
     * Creation date: (06/02/03 10:43:19)
     *
     * @return java.util.List
     */
    @Override
    public List<String> checkParameters() {
        return super.checkParameters();
    }

    /**
     * Insert the method's description here. Creation date: (06/02/03 16:40:55)
     *
     * @param elementToFind     java.lang.String
     * @param startInputElement org.jdom.Element
     * @return org.jdom.Element
     */
    private Element findMatchingElement(final String elementToFind, final Element startInputElement) {
        Element elementFound = null;

        if (elementToFind != null && elementToFind.trim()
                                                  .length() > 0) {
            String startInputElementName = startInputElement.getName();

            if (!elementToFind.equals(startInputElementName)) {
                List<Element> childElements = startInputElement.getChildren();
                int size = childElements.size();

                for (int i = 0; i < size; i++) {
                    elementFound = findMatchingElement(elementToFind, childElements.get(i));
                }
            } else {
                // Ensure that the found element is cloned, otherwise
                // an exception will be thrown, namely, the following
                // message will be output:
                // Exception: org.jdom.IllegalAddException:
                // The element already has an existing parent "name_of_parent"
                // where the name_of_parent refers to the name of the
                // parent element of the element we want to find.
                elementFound = startInputElement.clone();
            }
        }

        return elementFound;
    }

    /**
     * Generate output file.
     * <p>
     * Method to be implemented by by all implementing classes.
     * <p>
     * Creation date: (06/02/03 10:18:05)
     *
     * @throws IOException The exception description.
     */
    @Override
    public void generateOutputFile() throws IOException {
        List<String> errorMsgs = checkParameters();

        if (errorMsgs == null || errorMsgs.size() == 0) {
            // Build the complete path to the output file
            String outputDir = getOutputDirectory().toString();
            String outputFileName = getOutputFileName();
            File outputFilePath = null;

            if (outputDir.endsWith(File.separator)) {
                outputFilePath = new File(outputDir + outputFileName);
            } else {
                outputFilePath = new File(outputDir + File.separator + outputFileName);
            }

            // Create the root element
            String rootOutputElement = getRootOutputElement();
            Element rootElement = new Element(rootOutputElement);

            // Create a new JDOM document with a root element
            Document doc = new Document();
            doc.setRootElement(rootElement);

            // Create the DOCTYPE delaration if necessary
            if (isIncludeDoctype()) {
                DocType doctype = new DocType(rootOutputElement);
                if (isDoctypePublic()) {
                    doctype.setPublicID(getPublicId());
                } else {
                    doctype.setSystemID(getSystemId().toString());
                }
                doc.setDocType(doctype);
            }

            // Get the input files and copy their contents into the JDOM
            // document object. As the getInputFiles returns a List that
            // it has already trimmed to size, we do not need to repeat
            // that step here.
            List<File> inputFilesList = getInputFiles(getInputDirectory());
            if (inputFilesList != null) {
                int size = inputFilesList.size();
                if (inputFilesList.size() > 0) {
                    for (int i = 0; i < size; i++) {
                        File f = inputFilesList.get(i);
                        processInputFile(f, doc);
                    }
                }
            }

            // Now save the data to file with two spaces and extra line breaks.
            // Set the encoding.
            XMLOutputter outputter = new XMLOutputter();
            outputter.setFormat(Format.getPrettyFormat());
            // outputter.output( doc, System.out ) ;
            outputter.output(doc, new PrintWriter(new FileWriter(outputFilePath)));
        }
    }

    /**
     * Insert the method's description here. Creation date: (06/02/03 10:25:56)
     *
     * @param inputFile java.io.File
     * @param outputDoc org.jdom.Document
     * @throws IOException The exception description.
     */
    private void processInputFile(final File inputFile, final Document outputDoc) throws IOException {
        // Create a SAX builder for reading the input file
        // Check whether the user wants the input files to be
        // validated or not.
        SAXBuilder builder = null;
        if (isValidateInput()) {
            builder = new SAXBuilder(XMLReaders.NONVALIDATING);
        } else {
            builder = new SAXBuilder();
        }

        try {
            // Build the input XML document
            Document inputDoc = builder.build(inputFile);

            // Root element of the input document
            Element inputRootElement = inputDoc.getRootElement();

            // Get a reference to the output file's root element
            Element outputRootElement = outputDoc.getRootElement();

            // Now find the element that we are looking for in the input document
            Element elementFound = findMatchingElement(getStartInputElement(), inputRootElement);

            // Check that we have found the element. If we have, then add a comment
            // with the name of the input file and then add the found element as a
            // child of the root element of the output docuemnt.
            if (elementFound != null) {
                // Insert the name of the input file as a comment into the output file
                outputRootElement.addContent(new Comment(inputFile.getName()));

                // Now add the contents of the input file into the output file
                outputDoc.getRootElement()
                         .addContent(elementFound);
            }
        } catch (JDOMException je) {
            throw new IOException(je.getCause()
                                    .toString()
                                          + ": "
                                          + je.getMessage());
        }
    }
}
