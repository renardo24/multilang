<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xalan="http://xml.apache.org/xslt">

  <xsl:output method="html"
              encoding="ISO-8859-1"
              indent="yes"
              xalan:indent-amount="2"/>

<!--
This stylesheet converts a large XML file into a large HTML file.
-->
    <xsl:template match="/">
    </xsl:template>
</xsl:stylesheet>