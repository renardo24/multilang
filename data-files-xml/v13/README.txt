README


% mode line for jEdit; line starting with a percent
% sign is a comment for txt2tags
% :mode=text:indentSize=2:folding=indent:
% :maxLineLen=72:noTabs=true:tabSize=2:wrap=hard:


Created
=======
2007-06-21 (originally created 2001-11-21).


Description
===========
Create multilingual documents using XML. The format is slightly
different from other versions and the texts are generic vocabulary
texts.

Used service at http://www.hitsw.com/xml_utilites/ to generate DTD.
