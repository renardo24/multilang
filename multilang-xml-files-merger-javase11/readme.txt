readme.txt - XML Files Merger
=============================
Olivier Renard - 22/11/2001

Description:
------------
Small application that merges several XML files into one larger XML file

version_002 is the recommended version to use as later version have not
been tested exhaustively enough.

version_005 (14/02/2003)
version_004 (07/02/2003)
version_003 (05/02/2003)
version_002 (22/11/2001)
version_001 (22/11/2001)