readme.txt - Multilingual XML Texts
===================================
Olivier Renard - 22/07/2003

Description:
------------
Multilingual texts containind sentences in English, French, German
and English stored in XML format.

version_001 (22/07/2003)
------------------------
- Used service at http://www.hitsw.com/xml_utilites/ to generate DTD.
- Used service at http://www.hitsw.com/xml_utilites/ to generate Schema.