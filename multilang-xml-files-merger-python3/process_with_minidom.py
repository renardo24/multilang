#!/usr/bin/env python3
# -*- coding: <encoding name> -*-

import os
import pathlib
import pprint
import tempfile
import xml.dom.minidom

OUTPUT_FILE = 'merged_with_minidom.xml'


def build_output_file_path():
    """ Build the output file path. """
    output_file_path = pathlib.Path(tempfile.gettempdir(), OUTPUT_FILE)
    # print(output_file_path)
    return output_file_path


def process_xml_files_with_minidom(xml_files):
    """ Process XML files using minidom. """

    impl = xml.dom.minidom.getDOMImplementation()
    merged_doc = impl.createDocument(None, "merged_multilang", None)
    print(merged_doc.toprettyxml())
    top_element = merged_doc.documentElement
    for xml_file in xml_files:
        with open(xml_file, mode='r', encoding='ISO-8859-1') as fd:
            print(xml_file)
            #lines = fd.readlines()
            # print(lines)
            doc = xml.dom.minidom.parse(fd)
            top_element.appendChild(doc.firstChild)
            # print(merged_doc.toprettyxml())
    output_file_path = build_output_file_path()
    with open(output_file_path, 'w') as of:
        merged_doc.writexml(of)


def process_files():
    xml_files_path_envvar = os.getenv("XML_DATA_FILES_PATH")
    if xml_files_path_envvar is not None:
        print(f"Processing files in {xml_files_path_envvar}")
        xml_files_path = pathlib.Path(xml_files_path_envvar)
        xml_files = list(xml_files_path.glob("*.xml"))
        pprint.pprint(xml_files, indent="2")
        process_xml_files_with_minidom(xml_files)
    else:
        print(f"XML_DATA_FILES_PATH environment variable not defined")


if __name__ == '__main__':
    process_files()
