:: 2001-11-28
:: Updated 2007-03-27
@cd ..
@mkdir .\tmp\out
@java org.apache.xalan.xslt.Process -in ./src/combined_multilang.xml -xsl ./src/xslt/multilang_xslt_to_html.xslt -out ./tmp/out/combined.html -EDUMP dump.txt
@pause
