ReadMe.txt
----------

File format
-----------
* All multilingual information or text should be contained in its own file.
  For example, all information about James Ensor, i.e. the 5 lines of text
  on the artist, should be contained in their own file.
* Each file should be in its own XML file.


Using XmlFilesMerger
--------------------
* To include all the XML files within a single large XML file, just run 
  the small utility provided by the XmlFilesMerger Java application.
* The output file (whose name is set in the properties file) will be saved in 
  the same directory as the one where the Java application resides unless you
  specifiy a different path in the properties file.


Using XSL
---------
* To apply the XSL stylesheet, i.e. to transform the XML document into an
  HTML document, use the style sheets provided in the xslt directory.
* To create an HTML document for either one single, small XML file, or one 
  single, large XML file which was created as a result of using the 
  XmlFilesMerger utility, use the multilingual.xsl style sheet and the 
  multilingual.bat file.