# Multilang docs XML2any (v03)

## Created
2007-06-21 (originally created 2003-02-04).

## Description
Create multilingual documents using XML. Provide templates. Provide XSLT
stylesheets to convert XML documents into alternate formats. Version 02
is also an old version, but not as crude as version 01. However, it is
no longer maintained either.
- DTD/Schema
  - multilingual-xml-texts.dtd (2003-07-22)
  - multilingual-xml-texts.xsd (2003-07-22)
  - Used service at http://www.hitsw.com/xml_utilites/ to generate DTD.
  - Used service at http://www.hitsw.com/xml_utilites/ to generate
    Schema.
- CSS
  - multilingual.css (2003-02-04)
