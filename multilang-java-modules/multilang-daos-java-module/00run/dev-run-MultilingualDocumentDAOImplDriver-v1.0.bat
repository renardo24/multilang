:: 2007-04-02
@set CWD=%cd%
@cd %CWD%/..
@if not exist .\tmp mkdir .\tmp
@"%JDK_HOME%"\bin\java -cp "%M2_REPO%/org/jdom/jdom2/jdom-2.0.6.jar;./target/classes" omr.multilang.driver.MultilingualDocumentDAOImplDriver 1>.\tmp\MultilingualDocumentDAOImplDriver.OUTPUT 2>.\tmp\run_errors
@cd %CWD%
@pause
