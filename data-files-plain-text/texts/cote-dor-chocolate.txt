title
de
C�te d'Or Schokolade
en
C�te d'Or Chocolate
fr
Chocolat C�te d'Or

line
de
Mindestens haltbar bis -
en
Best before -
fr
� consommer de pr�f�rence avant le -

line
de
Vollmilchschokolade mit Haselnuss-, Mandel- und Cashewnussf�llung.
en
Milk chocolate filled with hazelnut, almond and cashew nut praline.
fr
Chocolat au fait fourr� aux noisettes, amandes et noix de cajou.

line
de
Vollmilchschokolade.
en
Milk chocolate.
fr
Chocolat au lait extra fin.

line
de
ZUTATEN:
en
INGREDIENTS:
fr
INGR�DIENTS:

line
de
Zucker
en
Sugar
fr
Sucre

line
de
Vollmilchpulver
en
Whole milk powder
fr
Poudre de lait entier / Lait entier en poudre

line
de
Kakaomasse
en
Cocoa mass
fr
P�te de cacao

line
de
Kakaobutter
en
Cocoa butter
fr
Beurre de cacao

line
de
Pfanzliche Fette
en
Hydrogenated vegetable fats
fr
Graisses v�g�tales hydrog�n�es

line
de
Haseln�sse (6%)
en
Hazelnuts (6%)
fr
Noisettes (6%)

line
de
Mandeln (4%)
en
Almonds (4%)
fr
Amandes (4%)

line
de
Cashewn�sse (3%)
en
Cashew nuts (3%)
fr
Noix de cajou (3%)

line
de
Milchzucker
en
Lactose
fr
Lactose

line
de
Milcheiweiss
en
Dried milk proteins
fr
Lactoprot�ines

line
de
S��molkenpulver
en
Whey powder
fr
Lactos�rum en poudre

line
de
Emulgator: Soja-Lecithin
en
Emulsifier: soya lecithin
fr
�mulsifiant: l�cithine de soja

line
de
Aromen
en
Flavourings
fr
Ar�mes

line
en
Milk chocolate contains:

line
de
Kakao: 35% mindestens
en
Cocoa solids: 35% minimum
fr
Cacao: 35% minimum

line
en
Milk solids: 14% minimum

line
de
Kann Spuren von N�ssen und Ei enthalten.
en
May contain traces of nuts and egg protein.
fr
Peut contenir des traces de fruits secs et de prot�ines d'oeuf.

line
de
Vor W�rme sch�tzen.
en
Keep cool and dry.
fr
� conserver au fraid et au sec.
