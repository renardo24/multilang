package omr.multilang.domain;

import java.io.Serializable;
import java.util.UUID;

/**
 * Class representing a multilingual entry in a multilingual text.
 *
 * @author Olivier Renard
 * @version 1.0 - 25/07/2003
 */
public class MultilingualEntryVO implements Serializable {

    private static final long serialVersionUID = 1865603266286131574L;
    private UUID id;
    private String englishText = "";
    private String frenchText = "";
    private String germanText = "";

    public MultilingualEntryVO() {
        super();
        id = UUID.randomUUID();
    }

    /**
     * Helper method that returns true if the String passed as parameter is not null
     * and has a length greater than 1, false otherwise.
     *
     * @param s The string to check
     * @return boolean true if the string is not null and has a length greater than
     * 1, false otherwise.
     */
    private static boolean containsText(final String s) {
        boolean result = false;

        if (s != null) {
            String newS = s.trim();
            if (newS.length() > 0) {
                result = true;
            }
        }

        return result;
    }

    /**
     * Overrides the equals() method. Checks for equality by comparing each language
     * String, <b>NOT</b> taking into account case sensitivity.
     *
     * @param o the object to check for equality
     * @return boolean true if 2 objects are equal; false otherwise
     */
    @Override
    public boolean equals(final Object o) {
        // Step 1: Perform an == test
        if (this == o) {
            return true;
        }

        // Step 2: Instance of check
        if (!(o instanceof MultilingualEntryVO)) {
            return false;
        }

        // Step 3: Cast argument
        MultilingualEntryVO mevo = (MultilingualEntryVO) o;

        // Step 4: For each important field, check to see if they are equal
        // For primitives use ==
        // For objects use equals() but be sure to also
        // handle the null case first
        // ----------------------------------------
        // Compare the ids.
        // ----------------------------------------
        if (id != mevo.id) {
            return false;
        }

        // ----------------------------------------
        // Compare the English texts.
        // ----------------------------------------
        if (englishText != null) {
            if (englishText.equalsIgnoreCase(mevo.getEnglishText())) {
                // ----------------------------------------
                // Compare the French texts.
                // ----------------------------------------
                if (frenchText != null) {
                    if (frenchText.equalsIgnoreCase(mevo.getFrenchText())) {
                        // ----------------------------------------
                        // Compare the German texts.
                        // ----------------------------------------
                        if (germanText != null) {
                            if (germanText.equalsIgnoreCase(mevo.getGermanText())) {
                                return true;
                            }
                        }
                    }
                }
            }
        }

        return false;
    }

    /**
     * Returns the hashcode for this object.
     *
     * @return int the hashcode for this object.
     */
    @Override
    public int hashCode() {
        StringBuilder buf = new StringBuilder();
        buf.append(id);
        buf.append(englishText);
        buf.append(frenchText);
        buf.append(germanText);

        return buf.toString()
                  .hashCode();
    }

    public String getEnglishText() {
        return englishText;
    }

    public void setEnglishText(final String englishText) {
        if (containsText(englishText)) {
            this.englishText = englishText;
        }
    }

    public String getFrenchText() {
        return frenchText;
    }

    public void setFrenchText(final String frenchText) {
        if (containsText(frenchText)) {
            this.frenchText = frenchText;
        }
    }

    public String getGermanText() {
        return germanText;
    }

    public void setGermanText(final String germanText) {
        if (containsText(germanText)) {
            this.germanText = germanText;
        }
    }

    public UUID getId() {
        return id;
    }

    public void setId(final UUID id) {
        this.id = id;
    }

    @Override
    public String toString() {
        String LS = System.getProperty("line.separator");
        StringBuilder buf = new StringBuilder();
        buf.append("\t--------------------------------------------------");
        buf.append(LS);
        buf.append("\tEntry id:\t");
        buf.append(id);
        buf.append(LS);
        buf.append("\tEnglish Text:\t");
        buf.append(englishText);
        buf.append(LS);
        buf.append("\tFrench Text:\t");
        buf.append(frenchText);
        buf.append(LS);
        buf.append("\tGerman Text:\t");
        buf.append(germanText);
        buf.append(LS);

        return buf.toString();
    }
}
