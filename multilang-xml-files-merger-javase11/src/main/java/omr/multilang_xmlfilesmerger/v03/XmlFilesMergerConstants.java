package omr.multilang_xmlfilesmerger.v03;

/**
 * Constants
 *
 * @author Olivier Renard
 */

public interface XmlFilesMergerConstants {
    String INPUT_DIR = "input_directory";

    String OUTPUT_DIR = "output_directory";

    String OUTPUT_FILE_NAME = "output_file_name";

    String XML_DECL = "xml_declaration";

    String DOCTYPE_DECL = "doctype_declaration";

    String INPUT_START_ELEMENT = "input_start_element";

    String OUTPUT_ROOT_ELEMENT = "output_root_element";

    String PROPS_FILE_NAME = "xml_files_merger_v03";
}