package omr.multilang_xmlfilesmerger.v04;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Interface to be implemented by all models.
 * <p>
 * Creation date: (06/02/03 08:57:39)
 *
 * @author: Olivier Renard
 */
public interface XMLFilesMergerModelInterface {
    /**
     * Checks where the data (parameters) to be used for generating the output XML
     * file is valid.
     * <p>
     * Method to be implemented by all implementing classes.
     * <p>
     * Creation date: (06/02/03 10:43:19)
     *
     * @return java.util.List
     */
    List<String> checkParameters();

    /**
     * Generate output file.
     * <p>
     * Method to be implemented by all implementing classes.
     * <p>
     * Creation date: (06/02/03 09:29:12)
     *
     * @throws IOException The exception description.
     */
    void generateOutputFile() throws IOException;

    /**
     * Get all the input files and put them in a List.
     * <p>
     * Method to be implemented by all implementing classes.
     * <p>
     * Creation date: (06/02/03 09:29:12)
     *
     * @throws IOException The exception description.
     */
    List<File> getInputFiles(File inputDirectory);
}
