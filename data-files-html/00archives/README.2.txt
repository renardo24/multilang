multilang-docs-html
Olivier Renard
2006-11-21


: HISTORY
  - 2006-11-21: Created project (Originally created 2003-07-22).

: PURPOSE
  To store multilingual documents in (X)HTML format ("``*.html``",
  "``*.htm``" or "``*.xhtml``").
  
: FILES
  - trilingual-v04.htm was originally written on 2001-06-28.
  - trilingual-v03.htm was originally written on 2000-11-28.
  - trilingual-v02.htm was originally written on 2000-07-06.
  - trilingual-v01.htm was originally written on 2000-06-13.
  - quadrilingual.htm and holyday_inn_japanese.gif were both created on
    2000-12-27. This document deals with 4 languages: English, French, German
    and Japanese.
  - leonidas.htm was originally written on 2001-06-30.
  - liege.html (2001-09-22)
  - marriage.htm (2001-09-01)
  - education.htm (2001-09-21)
  - sole.htm (2001-09-21)
  - walldorf.htm (2001-09-21)
  - eurostar-ticket-info.htm (2001-09-21)
  - eurostar_no_smoking_please.htm (2001-09-21)
  - wrapping-paper (2001-09-21)
  - thomas-sit-and-ride (2001-10-12)
  - creme-a-recurer (2001-09-21)
  - cote-dor (2001-09-21)
  - colours (2001-09-21)
  - bn-chocolates (2001-09-21)
  - (template) quadrilingual-template.htm (2002-11-25)
  - (template) quadrilingual.css (2001-09-12)

