package omr.multilang.daos;

/**
 * Data access exception to insulate business objects from the knowledge of the
 * unerlying exception.
 *
 * @author Olivier Renard
 * @version 1.0 - 24/07/2003
 * @see Exception
 */
public class DataAccessException extends Exception {

    private static final long serialVersionUID = -5066628698842074282L;

    public DataAccessException() {
        super();
    }

    public DataAccessException(final String message) {
        super(message);
    }

    public DataAccessException(final Throwable throwable) {
        super(throwable);
    }

    public DataAccessException(final String message, final Throwable throwable) {
        super(message, throwable);
    }
}