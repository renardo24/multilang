<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="text" encoding="iso-8859-1" indent="yes"/>
	<xsl:template match="titles">
title<xsl:apply-templates /></xsl:template>
	<xsl:template match="entry">
line<xsl:apply-templates /></xsl:template>
	<xsl:template match="title[@lang='German']">
  de
    <xsl:value-of select="."/></xsl:template>
    <xsl:template match="line[@lang='German']">
  de
    <xsl:value-of select="."/></xsl:template>
	<xsl:template match="title[@lang='English']">
  en
    <xsl:value-of select="."/></xsl:template>
    <xsl:template match="line[@lang='English']">
  en
    <xsl:value-of select="."/></xsl:template>
    <xsl:template match="line[@lang='French']">
  fr
    <xsl:value-of select="."/></xsl:template>
	<xsl:template match="title[@lang='French']">
  fr
    <xsl:value-of select="."/></xsl:template>
	<xsl:template match="title[@lang='Japanese']">
  ja
    <xsl:value-of select="."/></xsl:template>
    <xsl:template match="line[@lang='Japanese']">
  ja
    <xsl:value-of select="."/></xsl:template>
</xsl:stylesheet>
