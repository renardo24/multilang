<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="text" encoding="iso-8859-1" indent="yes"/>
	<xsl:template match="title">
title<xsl:apply-templates /></xsl:template>
	<xsl:template match="entry">
line<xsl:apply-templates /></xsl:template>
	<xsl:template match="german">
  de
    <xsl:value-of select="."/></xsl:template>
	<xsl:template match="english">
  en
    <xsl:value-of select="."/></xsl:template>
    <xsl:template match="french">
  fr
    <xsl:value-of select="."/></xsl:template>
</xsl:stylesheet>
