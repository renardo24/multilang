/*
 * AllTestsSuite.java
 *
 * Created on 23 February 2006, 12:29
 */

package omr.multilangdom;

import org.junit.platform.engine.discovery.DiscoverySelectors;
import org.junit.platform.launcher.Launcher;
import org.junit.platform.launcher.LauncherDiscoveryRequest;
import org.junit.platform.launcher.core.LauncherDiscoveryRequestBuilder;
import org.junit.platform.launcher.core.LauncherFactory;
import org.junit.platform.launcher.listeners.SummaryGeneratingListener;
import org.junit.platform.launcher.listeners.TestExecutionSummary;

/**
 * Runs all tests in this package. Can be run from the command line.
 *
 * @author Olivier Renard
 */
public class AllTestsSuiteRunner {

    public static void main(final String[] args) {
        LauncherDiscoveryRequest request = LauncherDiscoveryRequestBuilder.request().selectors(DiscoverySelectors.selectClass(AllTestsSuite.class)).build();
        Launcher launcher = LauncherFactory.create();

        SummaryGeneratingListener summaryListener = new SummaryGeneratingListener();
        launcher.registerTestExecutionListeners(summaryListener);

        launcher.execute(request);

        TestExecutionSummary summary = summaryListener.getSummary();

        for (TestExecutionSummary.Failure failure : summary.getFailures()) {
            System.out.println(failure.toString());
        }
        if (summary.getTotalFailureCount() == 0) {
            System.out.println("All tests finished successfully...");
        }
    }
}
