#!/usr/bin/env python3
#20170407
# Processes HTML files and converts them to XML files

import codecs
import datetime
from lxml import etree, html
import os
import sys
from io import StringIO
import itertools

if not os.path.exists("../out"):
    os.mkdir("../out")

onlyfiles = [f for f in os.listdir("../html-input-files") if os.path.isfile(os.path.join("../html-input-files", f))]
parser = html.HTMLParser()
for infile in onlyfiles:
    print("--> Processing '{}'".format(infile));
    try:
        with codecs.open(os.path.join("../html-input-files", infile), 'r', encoding='ISO-8859-1') as f:
            filecontent = f.read()
            tree = html.parse(StringIO(filecontent), parser)
            url = infile.replace(".htm", "")
            englishLines = []
            germanLines= []
            frenchLines = []
            try:
                for tableTitleElement in tree.xpath('//td[@class="clsTableTitle"]'):
                    if tableTitleElement is not None:
                        title = tableTitleElement.text_content()
                        break
                #for englishTitleElement in tree.xpath('//td[@class="clsFirstColTitle"]'):
                #    if englishTitleElement is not None:
                #        englishTitle = englishTitleElement.text_content()
                #for frenchTitleElement in tree.xpath('//td[@class="clsCenterColTitle"]'):
                #    if frenchTitleElement is not None:
                #        frenchTitle = frenchTitleElement.text_content()
                #for germanTitleElement in tree.xpath('//td[@class="clsLastColTitle"]'):
                #    if germanTitleElement is not None:
                #        germanTitle = germanTitleElement.text_content()

                for englishLineElement in tree.xpath('//td[@class="clsEnglish"]'):
                    if englishLineElement is not None:
                        englishLines.append(englishLineElement.text_content())
                for frenchLineElement in tree.xpath('//td[@class="clsFrench"]'):
                    if frenchLineElement is not None:
                        frenchLines.append(frenchLineElement.text_content())
                for germanLineElement in tree.xpath('//td[@class="clsGerman"]'):
                    if germanLineElement is not None:
                        germanLines.append(germanLineElement.text_content())
            except etree.XPathEvalError as etreeErr:
                print("etree error: {}".format(etreeErr));
                
            root = etree.Element('multilingual_entry')
            root.attrib['url']=url
            titles = etree.SubElement(root, 'titles')
            titleEn = etree.SubElement(titles, 'title')
            titleEn.attrib['lang']="English"
            titleEn.text=title
            titleFr = etree.SubElement(titles, 'title')
            titleFr.attrib['lang']="French"
            titleFr.text=title
            titleDe = etree.SubElement(titles, 'title')
            titleDe.attrib['lang']="German"
            titleDe.text=title
            
            for englishLine, frenchLine, germanLine in itertools.zip_longest(englishLines, frenchLines, germanLines, fillvalue=''):
                entry = etree.SubElement(root, 'entry')
                lineEn = etree.SubElement(entry, 'line')
                lineEn.attrib['lang']="English"
                lineEn.text = englishLine
                lineFr = etree.SubElement(entry, 'line')
                lineFr.attrib['lang']="French"
                lineFr.text = frenchLine
                lineDe = etree.SubElement(entry, 'line')
                lineDe.attrib['lang']="German"
                lineDe.text = germanLine

            currentDateTime = datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d at %H:%M:%S')
            # add the current date and time as a tome level comment
            root.addprevious(etree.Comment('Generated on ' + currentDateTime))
                            
            # print/write the result
            print (etree.tostring(root, pretty_print=True, xml_declaration=True, encoding='UTF-8', method='xml'))
            tree = etree.ElementTree(root)
            tree.write(open(os.path.join('../out', url + '.xml'), 'wb'), encoding='UTF-8', pretty_print=True, xml_declaration=True)
            
    except IOError as ioe:
        #print("Error: {} - {}", format(ioe.errno, ioe.strerror))
        print("Error: {}".format(ioe))

