package omr.multilang.driver;

import omr.multilang.daos.DataAccessException;
import omr.multilang.daos.MultilingualDocumentDAO;
import omr.multilang.daos.MultilingualDocumentDAOJDOMImpl;
import omr.multilang.domain.MultilingualDocumentVO;
import omr.multilang.domain.MultilingualEntryVO;

/**
 * Implementation to test the <code>MultilingualDocumentDAOImpl</code> class
 * to save and load MultilingualDocumentVO objects.
 * <p>
 * Note that for this driver to work, the "out" directory MUST be on the
 * classpath.
 *
 * @author Olivier Renard
 * @version 1.0 - 06/08/2003
 */
public class MultilingualDocumentDAOImplDriver {
    public static final String OUTPUT_FILE_PATH_HELLO_TEST = "./tmp/hello_test.xml";
    public static final String OUTPUT_FILE_PATH_HAPPY_MEAL = "./tmp/happy_meal_toy_platic_bag.xml";

    public static void main(final String args[]) {
        try {
            // FIRST DOCUMENT
            MultilingualDocumentVO saveDVO1 = new MultilingualDocumentVO();
            saveDVO1.setTitle("Hello and goodbye");

            Thread.currentThread()
                  .sleep(2000);

            MultilingualEntryVO evo11 = new MultilingualEntryVO();
            evo11.setEnglishText("Hello");
            evo11.setFrenchText("Bonjour");
            evo11.setGermanText("Guten Tag");
            saveDVO1.addEntry(evo11);

            Thread.currentThread()
                  .sleep(5000);

            MultilingualEntryVO evo12 = new MultilingualEntryVO();
            evo12.setEnglishText("Goodbye");
            evo12.setFrenchText("Au revoir");
            evo12.setGermanText("Auf wiedersehen");
            saveDVO1.addEntry(evo12);

            System.out.println("DVO1 before saving:");
            System.out.println(saveDVO1);

            MultilingualDocumentDAO impl1 = new MultilingualDocumentDAOJDOMImpl();
            impl1.save(saveDVO1, OUTPUT_FILE_PATH_HELLO_TEST);
            impl1 = null;

            impl1 = new MultilingualDocumentDAOJDOMImpl();
            MultilingualDocumentVO readDVO1 = impl1
                    .load(OUTPUT_FILE_PATH_HELLO_TEST);

            System.out.println("DVO1 after loading:");
            System.out.println(readDVO1);

            // SECOND DOCUMENT
            MultilingualDocumentVO saveDVO2 = new MultilingualDocumentVO();
            saveDVO2.setTitle("On a Happy Meal toy's platic bag");

            Thread.currentThread()
                  .sleep(5000);

            MultilingualEntryVO evo21 = new MultilingualEntryVO();
            evo21.setEnglishText("Risk of choking - small parts.");
            evo21.setFrenchText("Risque d'asphyxie - petits <E9>l<E9>ments.");
            evo21.setGermanText("Erstickungsgefahr - enth<E4>lt kleine Teile.");
            saveDVO2.addEntry(evo21);

            Thread.currentThread()
                  .sleep(2000);

            MultilingualEntryVO evo22 = new MultilingualEntryVO();
            evo22
                    .setEnglishText("Please retain this information for future reference.");
            evo22
                    .setFrenchText("Merci de conserver cette notice d'information.");
            evo22
                    .setGermanText("Bitte diese Information f<FC>r sp<E4>teren Gebrauch aufbewahren.");
            saveDVO2.addEntry(evo22);

            System.out.println("DVO2 before saving:");
            System.out.println(saveDVO2);

            MultilingualDocumentDAO impl2 = new MultilingualDocumentDAOJDOMImpl();
            impl2.save(saveDVO2, OUTPUT_FILE_PATH_HAPPY_MEAL);
            impl2 = null;

            impl2 = new MultilingualDocumentDAOJDOMImpl();
            MultilingualDocumentVO readDVO2 = impl2.load(OUTPUT_FILE_PATH_HAPPY_MEAL);

            System.out.println("DVO2 after loading:");
            System.out.println(readDVO2);
        } catch (DataAccessException dae) {
            dae.printStackTrace(System.out);
        } catch (InterruptedException ie) {
            ie.printStackTrace(System.out);
        }
    }
}
