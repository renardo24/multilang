package omr.multilang.domain;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class MultilingualVOsTest {

    @Test
    public void testMultilingualEntryVO() {
        MultilingualEntryVO vo = new MultilingualEntryVO();
        vo.setEnglishText("Hello");
        vo.setFrenchText("Bonjour");
        vo.setGermanText("Guten Tag");
        assertThat(vo).isNotNull();
        assertThat(vo.getEnglishText()).isEqualTo("Hello");
        assertThat(vo.getGermanText()).isEqualTo("Guten Tag");
        assertThat(vo.getFrenchText()).isEqualTo("Bonjour");
    }

    @Test
    public void testMultilingualDocumentVO() {

        List<MultilingualEntryVO> vosList = new ArrayList<MultilingualEntryVO>();
        MultilingualEntryVO evo1 = this.buildMultilingualEntryVO("Hello", "Bonjour", "Guten Tag");
        MultilingualEntryVO evo2 = this.buildMultilingualEntryVO("Goodbye", "Au revoir", "Auf wiedersehen");
        vosList.add(evo1);
        vosList.add(evo2);

        MultilingualDocumentVO dvo = this.buildMultilingualDocumentVO(vosList);

        assertThat(dvo.getId()).isNotNull();
        assertThat(dvo.getEntries()).hasSize(2);
        assertThat(dvo.getTitle()).isNotBlank();
        assertThat(dvo.getEntries()).contains(evo1, evo2);

        System.out.println(dvo);

        dvo.removeEntryById(evo1.getId());

        assertThat(dvo.getEntries()).hasSize(1);
        assertThat(dvo.getEntries()).contains(evo2);
        assertThat(dvo.getEntries()).doesNotContain(evo1);

        System.out.println(evo1.getId());
        System.out.println(evo2.getId());

        // uuid
        MultilingualEntryVO evoRetrieved1 = dvo.getEntryById(evo2.getId());
        assertThat(evoRetrieved1).isNotNull();
        assertThat(evoRetrieved1).isEqualTo(evo2);

        // string id
        MultilingualEntryVO evoRetrieved2 = dvo.getEntryById("" + evo2.getId());
        assertThat(evoRetrieved2).isNotNull();
        assertThat(evoRetrieved2).isEqualTo(evo2);
    }

    @Test
    public void testMultilingualDocumentVOWithNonExistingEntry() {
        MultilingualDocumentVO dvo = new MultilingualDocumentVO();
        dvo.setTitle("Document title");

        assertThat(dvo.getEntries()).hasSize(0);

        // string id
        MultilingualEntryVO evoRetrieved2 = dvo.getEntryById("someId");
        assertThat(evoRetrieved2).isNull();
    }

    private MultilingualDocumentVO buildMultilingualDocumentVO(final List<MultilingualEntryVO> vosList) {
        MultilingualDocumentVO dvo = new MultilingualDocumentVO();
        dvo.setTitle("Document title");
        vosList.stream()
               .forEach(e -> dvo.addEntry(e));

        return dvo;
    }

    private MultilingualEntryVO buildMultilingualEntryVO(final String englishText, final String frenchText,
                                                         final String germanText) {

        MultilingualEntryVO evo = new MultilingualEntryVO();
        evo.setEnglishText(englishText);
        evo.setFrenchText(frenchText);
        evo.setGermanText(germanText);

        return evo;
    }

}
