# README

This project contains HTML documents in multiple languages, predominently
in French, English and German. 

## Histry
Created on 2007-09-26 (originally created 2003-07-22).

### Files
- 000-lib-oli-lang.html.template (template)
- 000-quadrilingual.html.template (template) (2002-11-25)
- css/000-template-lib-oli-lang.css (template)
- css/000-template-quadrilingual.css (template) (2001-09-12)
- bn-chocolates.html (2001-09-21)
- cinema-and-films.html
- colours.html (2001-09-21)
- cote-dor.html (2001-09-21)
- creme-a-recurer.html (2001-09-21)
- education.html (2001-09-21)
- eurostar-no-smoking-please.html (2001-09-21)
- eurostar-ticket-info.html (2001-09-21)
- leonidas.html (2001-06-30)
- liege.html (2001-09-22)
- marriage.html (2001-09-01)
- mr-piano.html
- quadrilingual.html and holyday-inn-japanese.gif were both created on
  2000-12-27. This document deals with 4 languages: English, French,
  German and Japanese.
- shorthand-notebook.html
- sole.html (2001-09-21)
- thomas-sit-and-ride.html (2001-10-12)
- trilingual-v01.html (2000-06-13)
- trilingual-v02.html (2000-07-06)
- trilingual-v03.html (2000-11-28)
- trilingual-v04.html (2001-06-28)
- walldorf.html (2001-09-21)
- winnie-musical-mobile.html
- wrapping-paper.html (2001-09-21)

