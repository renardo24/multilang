package omr.multilang.action.form;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

public class CreateDocActionForm extends ActionForm {
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1798754784156789L;

	protected String docTitle = null;

	protected String docPath = null;

	public CreateDocActionForm() {
		super();
		init();
	}

	/**
	 * Initialisation method called by both the constructor and the
	 * <code>reset</code> method.
	 */
	protected void init() {
		Locale locale = Locale.getDefault();
		MessageResources messages = MessageResources
				.getMessageResources("ApplicationResources");

		docTitle = "";
		docPath = messages.getMessage(locale, "root.dir");
	}

	public void reset(ActionMapping actionMapping, HttpServletRequest request) {
		init();
	}

	public ActionErrors validate(ActionMapping actionMapping,
			HttpServletRequest request) {
		ActionErrors errors = new ActionErrors();

		if ((docTitle == null) || (docTitle.trim().length() == 0)) {
			errors.add("docTitle", new ActionError("error.doc.title"));
		}

		if ((docPath == null) || (docPath.trim().length() == 0)) {
			errors.add("docPath", new ActionError("error.doc.path"));
		}

		return errors;
	}

	/**
	 * Returns the docTitle.
	 * 
	 * @return String
	 */
	public String getDocTitle() {
		return docTitle;
	}

	/**
	 * Returns the docPath.
	 * 
	 * @return String
	 */
	public String getDocPath() {
		return docPath;
	}

	/**
	 * Reset the doc path to what it was before and reset the doc title by
	 * setting it to null.
	 */
	public void resetDocPathAndDocTitle(String originalPath) {
		this.docTitle = null;
		this.docPath = originalPath;
	}

	/**
	 * Sets the docTitle.
	 * 
	 * @param docTitle
	 *            The docTitle to set
	 */
	public void setDocTitle(String docTitle) {
		this.docTitle = docTitle;
	}

	/**
	 * Sets the docPath.
	 * 
	 * @param docPath
	 *            The docPath to set
	 */
	public void setDocPath(String docPath) {
		this.docPath = docPath;
	}
}
