#!/usr/bin/perl

use File::Spec;
use File::Copy qw(move);
use strict;

my $fileName;
my $oldFullPath;
my $newFullPath;
my $mainDir;
my $newFileName;
my $line;
my @fileList;

# $#ARGV represents the number of args passed to script; initially it is -1
die "$0 requires an argument: a directory." if $#ARGV != 0;
$mainDir = $ARGV[0];
die "$mainDir is not a directory" if !( -d ${mainDir} && -r ${mainDir});
opendir(MAINDIR, ${mainDir}) || die "Can't open directory $mainDir: $!\n";
@fileList = readdir ( MAINDIR ); # list all files
closedir(MAINDIR);

foreach $fileName ( @{fileList} ) {
    # print "FILE: ${file}\n";
    if (${fileName} ne "." && ${fileName} ne "..") {
        $oldFullPath = File::Spec->catfile(${mainDir}, ${fileName});
        print "${oldFullPath}\n";
		if ( -f ${oldFullPath} ) {
            $newFileName = ${fileName};
            $newFileName =~ s/_/-/g;
            $newFileName =~ s/^multilang-//g; # remove leading text 'multilang
            print "${newFileName}\n";
            $newFullPath = File::Spec->catfile(${mainDir}, ${newFileName});
            print "${newFullPath}\n";
            move ${oldFullPath}, ${newFullPath};
		}
	}
}
