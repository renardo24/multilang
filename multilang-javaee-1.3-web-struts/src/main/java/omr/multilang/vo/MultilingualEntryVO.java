package omr.multilang.vo;

import java.io.Serializable;

/**
 * Class representing a multilingual entry in a multilingual text.
 * 
 * @author Olivier Renard
 * @version 1.0 - 25/07/2003
 */
public class MultilingualEntryVO implements Serializable {
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1145465465464L;

	private long id = 0;

	// ----------------------------------------------------------------
	// Instance variables. There is one String for each language
	// in the multilingual application.
	// ----------------------------------------------------------------
	private String englishText = "";

	private String frenchText = "";

	private String germanText = "";

	private String japaneseText = "";

	/**
	 * Constructor for MultilingualEntryVO.
	 */
	public MultilingualEntryVO() {
		super();
		id = System.currentTimeMillis();
	}

	/**
	 * Helper method that returns true if the String passed as parameter is not
	 * null and has a length greater than 1, false otherwise.
	 * 
	 * @param s
	 *            The string to check
	 * @return boolean true if the string is not null and has a length greater
	 *         than 1, false otherwise.
	 */
	private static boolean containsText(String s) {
		boolean result = false;

		if (s != null) {
			s = s.trim();
			if (s.length() > 0) {
				result = true;
			}
		}

		return result;
	}

	/**
	 * Overrides the equals() method. Checks for equality by comparing each
	 * language String, <b>NOT</b> taking into account case sensitivity.
	 * 
	 * @param o
	 *            the object to check for equality
	 * @return boolean true if 2 objects are equal; false otherwise
	 */
	public boolean equals(Object o) {
		// Step 1: Perform an == test
		if (this == o) {
			return true;
		}

		// Step 2: Instance of check
		if (!(o instanceof MultilingualEntryVO)) {
			return false;
		}

		// Step 3: Cast argument
		MultilingualEntryVO mevo = (MultilingualEntryVO) o;

		// Step 4: For each important field, check to see if they are equal
		// For primitives use ==
		// For objects use equals() but be sure to also
		// handle the null case first
		// ----------------------------------------
		// Compare the ids.
		// ----------------------------------------
		if (this.id != mevo.id) {
			return false;
		}

		// ----------------------------------------
		// Compare the English texts.
		// ----------------------------------------
		if (this.englishText != null) {
			if (this.englishText.equalsIgnoreCase(mevo.getEnglishText())) {
				// ----------------------------------------
				// Compare the French texts.
				// ----------------------------------------
				if (this.frenchText != null) {
					if (this.frenchText.equalsIgnoreCase(mevo.getFrenchText())) {
						// ----------------------------------------
						// Compare the German texts.
						// ----------------------------------------
						if (this.germanText != null) {
							if (this.germanText.equalsIgnoreCase(mevo
									.getGermanText())) {
								// ----------------------------------------
								// Compare the Japanese texts.
								// ----------------------------------------
								if (this.japaneseText != null) {
									if (this.japaneseText.equalsIgnoreCase(mevo
											.getJapaneseText())) {
										return true;
									}
								}
							}
						}
					}
				}
			}
		}

		return false;
	}

	/**
	 * Returns the englishText.
	 * 
	 * @return String
	 */
	public String getEnglishText() {
		return englishText;
	}

	/**
	 * Returns the frenchText.
	 * 
	 * @return String
	 */
	public String getFrenchText() {
		return frenchText;
	}

	/**
	 * Returns the germanText.
	 * 
	 * @return String
	 */
	public String getGermanText() {
		return germanText;
	}

	/**
	 * Returns the japaneseText.
	 * 
	 * @return String
	 */
	public String getJapaneseText() {
		return japaneseText;
	}

	/**
	 * Returns the id.
	 * 
	 * @return long
	 */
	public long getId() {
		return id;
	}

	/**
	 * Returns the hascode for this object.
	 * 
	 * @return int the hascode for this object.
	 */
	public int hashCode() {
		StringBuffer buf = new StringBuffer();
		buf.append(id);
		buf.append(englishText);
		buf.append(frenchText);
		buf.append(germanText);
		buf.append(japaneseText);

		return buf.toString().hashCode();
	}

	/**
	 * Sets the englishText.
	 * 
	 * @param englishText
	 *            The englishText to set
	 */
	public void setEnglishText(String englishText) {
		if (containsText(englishText)) {
			this.englishText = englishText;
		}
	}

	/**
	 * Sets the frenchText.
	 * 
	 * @param frenchText
	 *            The frenchText to set
	 */
	public void setFrenchText(String frenchText) {
		if (containsText(frenchText)) {
			this.frenchText = frenchText;
		}
	}

	/**
	 * Sets the germanText.
	 * 
	 * @param germanText
	 *            The germanText to set
	 */
	public void setGermanText(String germanText) {
		if (containsText(germanText)) {
			this.germanText = germanText;
		}
	}

	/**
	 * Sets the japaneseText.
	 * 
	 * @param japaneseText
	 *            The japaneseText to set
	 */
	public void setJapaneseText(String japaneseText) {
		if (containsText(japaneseText)) {
			this.japaneseText = japaneseText;
		}
	}

	/**
	 * Sets the id.
	 * 
	 * @param id
	 *            The id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * Return the string representation of this object
	 * 
	 * @return the string representation of this object
	 */
	public String toString() {
		String LS = System.getProperty("line.separator");
		StringBuffer buf = new StringBuffer();
		buf.append("\t--------------------------------------------------");
		buf.append(LS);
		buf.append("\tEntry id:\t");
		buf.append(id);
		buf.append(LS);
		buf.append("\tEnglish Text:\t");
		buf.append(englishText);
		buf.append(LS);
		buf.append("\tFrench Text:\t");
		buf.append(frenchText);
		buf.append(LS);
		buf.append("\tGerman Text:\t");
		buf.append(germanText);
		buf.append(LS);
		buf.append("\tJapanese Text:\t");
		buf.append(japaneseText);
		buf.append(LS);

		return buf.toString();
	}
}
