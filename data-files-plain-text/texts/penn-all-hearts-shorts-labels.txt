title
en
Penn All Hearts (Navy Heather) Shorts Labels

line
en
Penn is a registered trademeark of HEAD technology GmbH and is used under license by High Life, LLC.
fr
Penn est une marque déposée de HEAD Technology GmbH et est usagé sous licence par High Lige, LLC.

line
en
Slim Fir
fr
Coupe étroite

line
en
A summer athleticcut that delivers better mobility by eliminitating the bulk of extra fabric. More modern fit.
fr
Une coupe athlétique, étroite et moderne offrant une liberté de mouvement accrue par l'élimination de l'excédent de tissu.
