README


% mode line for jEdit; line starting with a percent
% sign is a comment for txt2tags
% :mode=text:indentSize=2:folding=indent:
% :maxLineLen=72:noTabs=true:tabSize=2:wrap=hard:


Created
=======
2006-06-29 (originally created 2003-07-24)


Description
===========
Enable the creation and handling of multilingual texts and data via a
web front end.
