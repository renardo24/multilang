<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xalan="http://xml.apache.org/xslt">

<!--
Tell the parser that the output XML document is not
standalone, i.e. it needs a DTD.
Tell the parser what the doctype system is.
Tell XALAN to have an indent of 2.
-->
  <xsl:output method="xml"
              encoding="ISO-8859-1"
              doctype-system="../schema/multilingual-dtd.dtd"
              indent="yes"
              standalone="no"
              xalan:indent-amount="2"/>

<!--
Specify a variable (effectively a constant) for the input
directory where to get the input XML files from.
-->
<xsl:variable name="input_dir">..\xml\short_xml</xsl:variable>

<!--
This stylesheet converts a short XML file into a large XML file.
-->
    <xsl:template match="/">
      <!-- The processing instruction target matching "[xX][mM][lL]"
           is not allowed in the content. The only way to get round this
           is to use xsl:text, but this would insert a second processing
           instruction to the output document
      <?xml version="1.0" encoding="ISO-8859-1" standalone="no"?>
       -->

      <!-- A doctype delcaration is not allowed in the content, so it
           cannot be used as shown below:
           <!DOCTYPE multilingual-documents SYSTEM "../../dtd/multilingual-docs.dtd">
           The work around is to use xsl:text as follows:
      <xsl:text disable-output-escaping="yes"><![CDATA[<!DOCTYPE multilingual-document SYSTEM "../../dtd/multilingual-docs.dtd">]]></xsl:text>
      -->
        <multilingual-documents>
          <xsl:value-of select="$input_dir"/>
        </multilingual-documents>
    </xsl:template>
</xsl:stylesheet>
