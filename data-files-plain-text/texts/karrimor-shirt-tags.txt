title
en
Karrimor shirt tags

line
de
Leistungsfähige, feuchtigkeitsabsorbierende Technologie
en
Performance wicking technology
fr
Technologie d'évacuation de l'humidité

line
de
Atmungsaktiv
en
Breathable
fr
Respirant

line
de
Multifunktional
en
Multi-purpose
fr
Multi-usages

line
de
Schnelltrocknend
en
Quick drying
fr
Séchage rapide

line
de
Gebaut zum Laufen
en
Built to run
fr
Faits pour courir

line
de
Laufprodukte vom Karrimor sind anders, sind zum Laufen und für Sie gemacht.
en
Karrimor Runninf products are built different, built to run and built for you.
fr
Les produits de course Karrimor sont faits autrements, faits pour courir et faits pour vous.

line
de
Karrimor-Produkte werden unter Verwendung modernster Designs und Materialien hergestellt und warten mit einer Fülle innovativer Eigenschaften auf; inspiriert von Erfahrung und geschaffen für Leistung.
en
Utilising modern design and fabrics, Karrimor products are packed with innovative features; inspired by experience and designed to perform.
fr
En utilisant un design et des tissues modernes, les produits Karrimor sont préparés avec des fonctions innovantes; ils sont inspirés par l'expérience et conçus pour la performance.

