:: leave the IN parameter out (-in ../../xml/short_xml/.xml) since we want
:: to process all files in a specific directory
:: 2003-02-05
:: Updated 2007-03-27
@cd ..
@mkdir .\tmp\out\large_html
java org.apache.xalan.xslt.Process -PARAM input_dir ./src -xsl ./src/xslt/multilang_04_short_xml_to_large_xml.xsl -out ./tmp/out/large_xml/_merged.xml -EDUMP dump.txt
@pause