#!/bin/python3

import re;

def main():
    sampleText = """
TitLE
	The title
Line
	De
		Deutsche Linie
	En
		English line
	Fr
		Ligne francaise
LINE
	DE
		Deutsche Linie #2
	EN
		English line #2
	FR
		Ligne francaise #2

LINE
	DE
            Deutsche Linie #3
	EN
		English line #3
	FR
		Ligne francaise #3
""";
    lines = sampleText.split("\n");
    newText = "";
    for line in lines:
        pattern = re.compile(r"^\s+", re.IGNORECASE);
        line = pattern.sub('', line);
        if ('' == line):
            continue;
        pattern = re.compile(r"^LINE\s*$", re.IGNORECASE);
        line = pattern.sub('line', line);
        pattern = re.compile(r"^TITLE\s*$", re.IGNORECASE);
        line = pattern.sub('title', line);
        pattern = re.compile(r"^DE\s*$", re.IGNORECASE);
        line = pattern.sub('de', line);
        pattern = re.compile(r"^EN\s*$", re.IGNORECASE);
        line = pattern.sub('en', line);
        pattern = re.compile(r"^FR\s*$", re.IGNORECASE);
        line = pattern.sub('fr', line);
        line += "\n";
        newText += line;

    print(newText);

if (__name__ == "__main__"):
    main();
