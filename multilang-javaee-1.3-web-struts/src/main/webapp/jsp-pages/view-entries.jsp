<%@ page errorPage="error-page.jsp" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c" %>
<html:html locale="true">
<head>
  <title><bean:message key="title" /> - <bean:message key="label.view.entries" /></title>
  <link href="<html:rewrite page='/theme/global.css'/>" rel="stylesheet" type="text/css" />
  <script language="JavaScript">
  <!--
    function confirmDeletion( row )
    {
      var url = row.toString() ;
      //alert( url ) ;

      var idIndex = url.lastIndexOf( "=" ) + 1 ;
      //alert( idIndex ) ;

      var id = url.substring( idIndex ) ;
      //alert( id ) ;

      var agree = confirm( "Are you sure that you want to delete this entry (" + id + ")?" )
      if ( agree )
      {
        return true ;
      }
      else
      {
        return false ;
      }
    }
  //-->
  </script>
</head>
<body>
<%@ include file="../jspf/navbar.jspf"%>
<br />

<div align="center" class="error">
  <html:errors />
</div>

<logic:present name="error.message">
  <bean:write filter="false" name="error.message" />
</logic:present>

<logic:present name="success.message">
  <bean:write filter="false" name="success.message" />
</logic:present>

<h1><bean:message key="label.view.entries" />: <span style="color:#336699"><c:out value="${sessionScope.doc.title}" /></span></h1>

<c:choose>
  <c:when test="${empty sessionScope.doc.entries}">
    <p style="text-align:center"><bean:message key="message.empty.document" /></p>
  </c:when>
  <c:otherwise>
    <c:forEach items="${sessionScope.doc.entries}" var="entries" varStatus="vs">
      <table border="1" bordercolor="#CECF9C" cellpadding="11" cellspacing="0" width="100%">
        <tr>
          <th valign="top" width="10%"><b><bean:message key="label.id" /></b></th>
          <th valign="top" width="30%"><b><c:out value="${entries.id}" /></b></th>
          <th valign="top" width="30%">
            <c:set var="id" value="${entries.id}" />
            <html:link action="/action/update/entry/1"
                       paramId="entryId"
                       paramName="id">
              <bean:message key="label.update" />
            </html:link>
          </th>
          <th valign="top" width="30%">
            <html:link action="/action/delete/entry"
                       paramId="entryId"
                       paramName="id"
                       onclick="return confirmDeletion( this )">
              <bean:message key="label.delete" />
            </html:link>
          </th>
        </tr>
        <tr>
          <td colspan="1" valign="top" width="10%"><b><bean:message key="label.english" /></b></td>
          <td colspan="3" valign="top" width="90%"><c:out value="${entries.englishText}" /></td>
        </tr>
        <tr>
          <td colspan="1" valign="top" width="10%"><b><bean:message key="label.french" /></b></td>
          <td colspan="3" valign="top" width="90%"><c:out value="${entries.frenchText}" /></td>
        </tr>
        <tr>
          <td colspan="1" valign="top" width="10%"><b><bean:message key="label.german" /></b></td>
          <td colspan="3" valign="top" width="90%"><c:out value="${entries.germanText}" /></td>
        </tr>
        <tr>
          <td colspan="1" valign="top" width="10%"><b><bean:message key="label.japanese" /></b></td>
          <td colspan="3" valign="top" width="90%"><c:out value="${entries.japaneseText}" /></td>
        </tr>
      </table>
      <p>&nbsp;</p>
    </c:forEach>
  </c:otherwise>
</c:choose>

<hr />

<%@ include file="../jspf/footer.jspf"%>

</body>
</html:html>