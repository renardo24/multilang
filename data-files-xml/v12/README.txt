README


% mode line for jEdit; line starting with a percent
% sign is a comment for txt2tags
% :mode=text:indentSize=2:folding=indent:
% :maxLineLen=72:noTabs=true:tabSize=2:wrap=hard:


Created
=======
2007-06-21 (originally created 2003-07-22).


Description
===========
Create multilingual documents using XML. Provide templates. Provide XSLT
stylesheets to convert XML documents into alternate formats. This
version is an old version (and cruder version) than any other version.
This version is no longer maintained.

Used service at http://www.hitsw.com/xml_utilites/ to generate DTD.
