<%@ page errorPage="error-page.jsp" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c" %>
<html:html locale="true">
<head>
  <title><bean:message key="title" /> - <bean:message key="label.create.new" /></title>
  <link href="<html:rewrite page='/theme/global.css'/>" rel="stylesheet" type="text/css" />
  <script language="JavaScript" src="<html:rewrite page='/js/update-path.js' />"></script>
</head>
<body onload="setRoot();">
<%@ include file="../jspf/navbar.jspf"%>
<br />

<div align="center" class="error">
  <html:errors />
</div>

<logic:present name="error.message">
  <bean:write filter="false" name="error.message" />
</logic:present>

<h1><bean:message key="label.create.new" /></h1>

<div align="center">

  <center>

    <html:form action="/action/create/doc/2" focus="docTitle">

      <table border="1" bordercolor="#CECF9C" cellpadding="11" cellspacing="0" width="100%">
        <tr>
          <td class="projectLabel" width="20%"><bean:message key="label.document.title"/></td>
          <td class="project" width="80%"><html:text property="docTitle" onkeyup="completePath()" size="60"></html:text></td>
        </tr>
        <tr>
          <td class="projectLabel" width="20%"><bean:message key="label.document.path"/></td>
          <td class="project" width="80%"><html:text property="docPath" readonly="true" size="120" styleClass="readonly"></html:text></td>
        </tr>
        <tr>
          <th class="project" colspan="2" width="100%"><center><html:reset><bean:message key="button.reset"/></html:reset>&nbsp;&nbsp;&nbsp;<html:submit><bean:message key="button.create"/></html:submit></center></th>
        </tr>
      </table>

      <hr />

    </html:form>

  </center>

</div>

<%@ include file="../jspf/footer.jspf"%>

</body>
</html:html>