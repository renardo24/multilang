#!/usr/bin/env bash

echo "Compiling..."
groovyc clean-multilang-txt-file.groovy
echo "Running..."
groovy clean-multilang-txt-file
echo "Done."

# clean up
rm -f *.class
