/*
 * MultilangDocumentTest.java
 * JUnit based test
 *
 * Created on 23 February 2006, 10:54
 */

package omr.multilangdom;

import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * @author Olivier Renard
 */
public class MultilangDocumentTest {

    // public static Test suite() {
    // TestSuite suite = new TestSuite(MultilangDocumentTest.class);
    // return suite;
    // }

    /**
     * Test of equals method, of class org.renardo.multilangdocs.MultilangDocument.
     */
    @Test
    public void testEqualsOnlyIdAndTitleNoEntries() {
        System.out.println("testEqualsOnlyIdNoEntries");
        MultilangDocument instance1 = new MultilangDocument(1,
                "Title");
        MultilangDocument instance2 = new MultilangDocument(1,
                "Title");
        boolean expResult = true;
        boolean result = instance1.equals(instance2);
        assertEquals(expResult, result);
        result = instance2.equals(instance1);
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class org.renardo.multilangdocs.MultilangDocument.
     */
    @Test
    public void testEqualsOnlyIdAndTitleNoEntriesDifferentTitles() {
        System.out.println("testEqualsOnlyIdAndTitleNoEntriesDifferentTitles");
        MultilangDocument instance1 = new MultilangDocument(1,
                "Title");
        MultilangDocument instance2 = new MultilangDocument(1,
                "Bla bla");
        boolean expResult = false;
        boolean result = instance1.equals(instance2);
        assertEquals(expResult, result);
        result = instance2.equals(instance1);
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class org.renardo.multilangdocs.MultilangEntry.
     */
    @Test
    public void testEqualsWithIdAndTitleAndSameEnglishLines() {
        System.out.println("testEqualsWithIdAndTitleAndSameEnglishLines");

        MultilangLine line1 = new MultilangLine(1);
        line1.setText("Hello");

        MultilangLine line2 = new MultilangLine(1);
        line2.setText("Hello");

        MultilangEntry entry1 = new MultilangEntry(1);
        entry1.setEnglishLine(line1);
        MultilangEntry entry2 = new MultilangEntry(1);
        entry2.setEnglishLine(line2);

        MultilangDocument instance1 = new MultilangDocument(1,
                "Title");
        instance1.addMultilangEntry(entry1);
        MultilangDocument instance2 = new MultilangDocument(1,
                "Title");
        instance2.addMultilangEntry(entry2);

        boolean expResult = true;
        boolean result = instance1.equals(instance2);
        assertEquals(expResult, result);

        result = instance2.equals(instance1);
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class org.renardo.multilangdocs.MultilangEntry.
     */
    @Test
    public void testEqualsWithIdAndTitleAndDifferentEnglishLines() {
        System.out.println("testEqualsWithIdAndTitleAndDifferentEnglishLines");

        MultilangLine line1 = new MultilangLine(1);
        line1.setText("Hello");

        MultilangLine line2 = new MultilangLine(1);
        line2.setText("Goodbye");

        MultilangEntry entry1 = new MultilangEntry(1);
        entry1.setEnglishLine(line1);
        MultilangEntry entry2 = new MultilangEntry(1);
        entry2.setEnglishLine(line2);

        MultilangDocument instance1 = new MultilangDocument(1,
                "Title");
        instance1.addMultilangEntry(entry1);
        MultilangDocument instance2 = new MultilangDocument(1,
                "Title");
        instance2.addMultilangEntry(entry2);

        boolean expResult = false;
        boolean result = instance1.equals(instance2);
        assertEquals(expResult, result);

        result = instance2.equals(instance1);
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class
     * org.renardo.multilangdocs.MultilangDocument.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        // String sep = System.getProperty("line.separator");
        MultilangDocument instance = new MultilangDocument(1,
                "Title");
        MultilangEntry entry = new MultilangEntry(1);
        MultilangLine line = new MultilangLine(1);
        line.setText("Olivier");
        entry.setEnglishLine(line);
        instance.addMultilangEntry(entry);
        String result = instance.toString();
        assertNotNull(result);
    }

    /**
     * Test of getDocumentId and setDocumentId methods, of class
     * org.renardo.multilangdocs.MultilangDocument.
     */
    @Test
    public void testGetAndSetDocumentId() {
        System.out.println("getDocumentId and setDocumentId ");

        MultilangDocument instance = new MultilangDocument(1,
                "Title");
        long expResult = 1L;
        long result = instance.getDocumentId();
        assertEquals(expResult, result);

        instance.setDocumentId(20);
        expResult = 20L;
        result = instance.getDocumentId();
        assertEquals(expResult, result);
    }

    /**
     * Test of getEnglishTitle and setEnglishTitle methods, of class
     * org.renardo.multilangdocs.MultilangDocument.
     */
    @Test
    public void testGetAndSetEnglishTitle() {
        System.out.println("getEnglishTitle and setEnglishTitle");

        MultilangDocument instance = new MultilangDocument(1,
                "Title");
        instance.setEnglishTitle("Hello");

        String expResult = "Hello";
        String result = instance.getEnglishTitle();
        assertEquals(expResult, result);
    }

    /**
     * Test of getFrenchTitle and setFrenchTitle methods, of class
     * org.renardo.multilangdocs.MultilangDocument.
     */
    @Test
    public void testGetAndSetFrenchTitle() {
        System.out.println("getFrenchTitle and setFrenchTitle");

        MultilangDocument instance = new MultilangDocument(1,
                "Title");
        instance.setFrenchTitle("Bonjour");

        String expResult = "Bonjour";
        String result = instance.getFrenchTitle();
        assertEquals(expResult, result);
    }

    /**
     * Test of getGermanTitle and setGermanTitle methods, of class
     * org.renardo.multilangdocs.MultilangDocument.
     */
    @Test
    public void testGetAndSetGermanTitle() {
        System.out.println("getGermanTitle and setGermanTitle");

        MultilangDocument instance = new MultilangDocument(1,
                "Title");
        instance.setGermanTitle("Guten Tag");

        String expResult = "Guten Tag";
        String result = instance.getGermanTitle();
        assertEquals(expResult, result);
    }

    /**
     * Test of getDateOfCreation and setDateOfCreation methods, of class
     * org.renardo.multilangdocs.MultilangDocument.
     */
    @Test
    public void testGetAndSetDateOfCreation() {
        System.out.println("getDateOfCreation and setDateOfCreation");

        MultilangDocument instance = new MultilangDocument(1,
                "Title");
        assertNotNull(instance.getDateOfCreation());

        Date newDate = new Date(123456789);
        System.out.println(newDate);
        // String iso8601DateTime = "1970-01-02T11:17:36";
        instance.setDateOfCreation(newDate);
        Date result = instance.getDateOfCreation();
        assertNotNull(result);
        assertEquals(newDate, result);
    }

    /**
     * Test of addMultilangEntry and getMultilangEntries methods, of class
     * org.renardo.multilangdocs.MultilangDocument.
     */
    @Test
    public void testAddAndGetMultilingualEntry() {
        System.out.println("addMultilingualEntry and getMultilingualEntries");

        MultilangLine line = new MultilangLine(1);
        line.setText("Olivier");

        MultilangEntry entry = new MultilangEntry(1);
        entry.setEnglishLine(line);

        MultilangDocument instance = new MultilangDocument(1,
                "Title");
        instance.addMultilangEntry(entry);

        assertNotNull(instance.getMultilangEntries());
        assertEquals(1,
                instance.getMultilangEntries()
                        .size());
        assertEquals(entry,
                instance.getMultilangEntries()
                        .get(0));
    }

    @Test
    public void testSeveralEntriesInVariousLanguages() {
        MultilangLine lineEN = new MultilangLine(System.currentTimeMillis());
        lineEN.setText("Hello");
        MultilangLine lineFR = new MultilangLine(System.currentTimeMillis());
        lineFR.setText("Bonjour");
        MultilangLine lineDE = new MultilangLine(System.currentTimeMillis());
        lineDE.setText("Guten Tag!");
        lineDE.setImage(new byte[]{1, 2, 3});
        MultilangEntry entry = new MultilangEntry(System.currentTimeMillis(),
                lineEN,
                lineFR,
                lineDE);
        MultilangDocument doc = new MultilangDocument(System.currentTimeMillis(),
                "Hello in various languages");
        doc.setFrenchTitle("Bonjour en plusieures langues");
        doc.addMultilangEntry(entry);
        doc.setGermanTitle("Guten Tag: verschiedene Sprachen");
        assertEquals(1,
                doc.getMultilangEntries()
                        .size());
    }
}
