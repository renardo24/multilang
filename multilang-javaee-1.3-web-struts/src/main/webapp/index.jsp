<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<html:html locale="true">
<head>
  <title><bean:message key="title" /></title>
  <link href="theme/global.css" rel="stylesheet" type="text/css" />
</head>
<body>
<%@ include file="jspf/navbar.jspf"%>
<br />

<div align="center" class="error">
  <html:errors />
</div>

<logic:present name="error.message">
  <bean:write filter="false" name="error.message" />
</logic:present>

<h1><bean:message key="title" /></h1>

<br /><br />
<div align="center">
  <html:img src="img/multilingual_400.png" />
</div>

<hr />

<%@ include file="jspf/footer.jspf"%>

</body>
</html:html>