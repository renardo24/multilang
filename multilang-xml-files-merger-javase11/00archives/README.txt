======
README
======

Created
  2001-11-22

Aim
  Small application that merges several XML files into one larger XML file.

There are several version of this application:

- v04 (2003-02-14)
- v03 (2003-02-05)
- v02 (2001-11-22)
- v01 (2001-11-22)

v02 is the recommended version to use as later versions have not been tested
exhaustively enough.

