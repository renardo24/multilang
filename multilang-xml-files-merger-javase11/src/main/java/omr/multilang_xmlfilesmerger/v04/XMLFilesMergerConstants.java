package omr.multilang_xmlfilesmerger.v04;

/**
 * Insert the type's description here.
 * <p>
 * Creation date: (05/02/03 11:45:18)
 *
 * @author: Olivier Renard
 */
public interface XMLFilesMergerConstants {
    // Main properties file
    String XML_PROPERTIES_FILE = "xml_files_merger_v04.properties";

    // XML file extension
    String XML_FILE_EXTENSION = ".xml";

    // XML processing options
    int JDOM = 0;
    int DOM = 1;
    int SAX = 2;

    /////////////// START - PROPERTIES FILES KEYS ///////////////
    // XML APIs
    String XML_APIS = "xmlApis";

    // XML information
    String XML_VERSION = "xmlVersion";
    String ENCODING = "encoding";
    String STANDALONE = "standalone";
    String VALIDATE_INPUT = "validateInput";

    // Input and output information
    String INPUT_DIRECTORY = "inputDirectory";
    String OUTPUT_DIRECTORY = "outputDirectory";
    String OUTPUT_FILE_NAME = "outputFileName";

    // Doctype and DTD information
    String INCLUDE_DOCTYPE = "includeDoctype";
    String DOCTYPE_PUBLIC = "doctypePublic";
    String DOCTYPE_SYSTEM = "doctypeSystem";
    String PUBLIC_ID = "publicId";
    String SYSTEM_ID = "systemId";

    // Root and start elements information.
    // For the output, we need to know the name
    // of the root element of the document.
    // For input, we need to know the name of
    // the element whose contents (and children)
    // will be included in the large XML file.
    String ROOT_OUTPUT_ELEMENT = "rootOutputElement";
    String START_INPUT_ELEMENT = "startInputElement";
    /////////////// END - PROPERTIES FILES KEYS ///////////////
}
