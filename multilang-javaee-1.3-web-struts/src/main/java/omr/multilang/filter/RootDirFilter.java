package omr.multilang.filter;

import java.io.File;
import java.io.IOException;
import java.util.ResourceBundle;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public final class RootDirFilter implements Filter {
	public static final String FILE_SEPARATOR = System
			.getProperty("file.separator");

	public static final String USER_HOME = System.getProperty("user.home");

	private String errorMessage = null;

	private File rootDir = null;

	private FilterConfig filterConfig = null;

	private ResourceBundle bundle = null;

	private static String rootDirNotDefined = null;

	private static String rootDirNotExist = null;

	private static String rootDirNotDirectory = null;

	public void init(FilterConfig filterConfig) {
		this.filterConfig = filterConfig;
		System.out.println(this.filterConfig);
		String messageResources = filterConfig
				.getInitParameter("message-resources");
		String rootDirStr = null;

		try {
			bundle = ResourceBundle.getBundle(messageResources);

			rootDirStr = bundle.getString("root.dir");
			rootDir = new File(rootDirStr);

			if ((rootDir == null) || (!rootDir.exists())
					|| (!rootDir.isDirectory())) {
				rootDir = new File(USER_HOME);
			}

			rootDirNotDefined = bundle.getString("error.root.dir.not.defined");
			rootDirNotExist = bundle.getString("error.root.dir.not.exist");
			rootDirNotDirectory = bundle
					.getString("error.root.dir.not.directory");

			rootDirStr = rootDir.toString();

			if (!rootDirStr.endsWith(FILE_SEPARATOR)) {
				StringBuffer buf = new StringBuffer(rootDirStr);
				buf.append(FILE_SEPARATOR);
				rootDirStr = buf.toString();
			}

			filterConfig.getServletContext().setAttribute("original-path",
					rootDirStr);
		} catch (Exception e) {
			rootDir = new File(USER_HOME);
			filterConfig.getServletContext().log(
					"Unable to load the message resources from "
							+ messageResources);
			filterConfig.getServletContext().log(e.getMessage());
			rootDirNotDefined = "The root directory is undefined.";
			rootDirNotExist = "The root directory does not exist.";
			rootDirNotDirectory = "The value of the root directory does not point to a directory.";
		}

		filterConfig.getServletContext().log(
				"The root directory has been set to: " + rootDirStr);
	}

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		boolean errorFound = false;

		if (rootDir == null) {
			errorFound = true;
			errorMessage = rootDirNotDefined;

		} else if (!rootDir.exists()) {
			errorFound = true;
			errorMessage = rootDirNotExist;
		} else if (!rootDir.isDirectory()) {
			errorFound = true;
			errorMessage = rootDirNotDirectory;
		}

		if (errorFound) {
			// RequestDispatcher rd =
			// this.filterConfig.getServletContext().getRequestDispatcher(
			// "/index.jsp" ) ;
			// rd.forward( request, response ) ;

			HttpServletRequest httpRequest = (HttpServletRequest) request;
			httpRequest.getSession()
					.setAttribute("error.message", errorMessage);
			HttpServletResponse httpResponse = (HttpServletResponse) response;
			System.out.println(httpRequest.getContextPath());
			System.out.println(httpRequest.getServletPath());
			String redirect = httpResponse.encodeRedirectURL(httpRequest
					.getContextPath()
					+ "/index.jsp");
			httpResponse.sendRedirect(redirect);
			return;
		} else {
			HttpServletRequest httpRequest = (HttpServletRequest) request;
			httpRequest.getSession().removeAttribute("error.message");
			chain.doFilter(request, response);
			return;
		}
	}

	public void destroy() {
		this.errorMessage = null;
		this.filterConfig = null;
		this.rootDir = null;
	}
}