package omr.multilang.action;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import omr.multilang.MultilangConstants;
import omr.multilang.action.form.EntryActionForm;
import omr.multilang.dao.DataAccessException;
import omr.multilang.dao.MultilingualDocumentDAOIF;
import omr.multilang.dao.MultilingualDocumentDAOImpl;
import omr.multilang.vo.MultilingualDocumentVO;
import omr.multilang.vo.MultilingualEntryVO;

public class AddEntryAction extends org.apache.struts.action.Action {
	public static final String FILE_SEPARATOR = System
			.getProperty("file.separator");

	public AddEntryAction() {
		super();
	}

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		EntryActionForm entryForm = (EntryActionForm) form;

		if (this.isCancelled(request)) {
			return (mapping.findForward("home"));
		}

		String action = request.getParameter("action");
		if (action == null) {
			action = "addEntry";
		}
		servlet.log("AddEntryAction: Processing " + action + " action");

		Locale locale = getLocale(request);
		MessageResources mr = getResources(request);

		HttpSession session = request.getSession();
		Object oDoc = session.getAttribute("doc");
		Object oPath = session.getAttribute("docPath");
		if (oDoc == null) {
			// HANDLE ERROR - NO DOCUMENT IN THE SESSION
			servlet.log("AddEntryAction: The document does not exists!");
			request.setAttribute("error.message", mr.getMessage(locale,
					"error.no.document.available"));
			return (mapping.findForward("failure"));
		} else if (oPath == null) {
			// HANDLE ERROR - NO PATH IN THE SESSION
			servlet.log("AddEntryAction: The document path does not exists!");
			request.setAttribute("error.message", mr.getMessage(locale,
					"error.no.document.path.available"));
			return (mapping.findForward("failure"));
		} else {
			MultilingualDocumentVO docVO = (MultilingualDocumentVO) oDoc;
			String docPath = oPath.toString();
			String docName = docPath.substring(docPath
					.lastIndexOf(FILE_SEPARATOR) + 1);

			String englishText = entryForm.getEnglishText();
			String frenchText = entryForm.getFrenchText();
			String germanText = entryForm.getGermanText();
			String japaneseText = entryForm.getJapaneseText();

			MultilingualEntryVO entryVO = new MultilingualEntryVO();
			entryVO.setEnglishText(englishText);
			entryVO.setFrenchText(frenchText);
			entryVO.setGermanText(germanText);
			entryVO.setJapaneseText(japaneseText);

			docVO.addEntry(entryVO);

			MultilingualDocumentDAOIF docDAO = new MultilingualDocumentDAOImpl();

			try {
				docDAO.save(docVO, docPath);
				session.setAttribute("doc", docVO);
				request.setAttribute("success.message", mr.getMessage(locale,
						"success.add.entry", "" + entryVO.getId(), docName));

				// Can we really do this?
				entryForm.reset(mapping, request);

				return (mapping.findForward(MultilangConstants.FORWARD_SUCCESS));
			} catch (DataAccessException dae) {
				servlet.log(dae.getMessage());
				request.setAttribute("error.message", mr.getMessage(locale,
						"error.document.save", docName));
				return (mapping.findForward(MultilangConstants.FORWARD_FAILURE));
			}
		}
	}
}
