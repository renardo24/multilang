title
en
Volumax laser printer paper

line
de
wei�
en
white
fr
blanc

line
de
Papier f�r den t�glichen Gebrauch, speziell interne Kommunikation
en
Paper for day to day use, especially internal communication
fr
Papier pour usage quotidien. Communication interne

line
de
Papier passend f�r interne und externe Kommunikation, wenig Farbausdruck
en
Paper suitable for internal and external communication using a splash of colour
fr
Papier pr�conis� pour votre communication interne et externe. Possibilit� d'imprimer un peu de couleur

line
de
Papier passend f�r jeden Einsatz, externe Kommunikation,, grossfl�chiger Farbausdruck
en
Paper suited for all types of use, external communication using a lot of colour
fr
Papier garanti tous usages, sp�cialement la communication externe n�cessitant beaucoup de couleur

line
de
Prestige Papier passend zu Ihrem Firmenbriefkopf, erh�ht Ihr Image
en
Prestige paper suited to your company letterheads and to improve your image
fr
Papier de prestige parfait pour vos t�tes de lettre ou pour am�liorer votre image

line
de
Papier speziell hergestellt f�r den Einsatz auf Ihrem Farblaser und Farbkopierer
en
Paper designed to make the most of your colour printer
fr
Papier vous permettant d'utiliser au mieux votre imprimante couleur
