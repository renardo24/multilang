:: leave the IN parameter out (-in ../../xml/short_xml/.xml) since we want
:: to process all files in a specific directory
:: 2003-02-05
@cd ..\..
java org.apache.xalan.xslt.Process -PARAM input_dir ./src/main/v05/xml -xsl short-xml-to-large-xml.xsl -out ./out/v05/large-xml/_merged.xml -EDUMP dump.txt
@pause