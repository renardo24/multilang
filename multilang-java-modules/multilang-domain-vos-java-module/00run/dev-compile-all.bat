::
::  Olivier Renard
::  2007-04-02
::
::  Compiling Multilang VOs JavaSE
::

@Title Compiling Multilang VOs JavaSE

@echo off

@set CWD=%cd%
@cd %CWD%/..

@if not exist .\target\classes mkdir .\target\classes

@if not exist .\tmp mkdir .\tmp

@"%JDK_HOME%"\bin\javac -cp "%M2_REPO%/org/jdom/jdom2/jdom-2.0.6.jar;./src/main/java" -d ./target/classes ./src/main/java/omr/multilang/domain/*.java 2>>.\tmp\compile_errors

@cd %CWD%

@pause
