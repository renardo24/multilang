package omr.multilang.daos;

import omr.multilang.domain.MultilingualDocumentVO;

/**
 * Interface for the Data Access Object to be used for MultilingualDocumentVO
 * objects.
 * <p>
 * Objects implementing this interface needs to implement a form of data access
 * mechanism in order to support a form of persistence for these VO objects.
 * <p>
 * Enables decoupling between VO objects and data access implementation.
 *
 * @author Olivier Renard
 * @version 1.1 - 06/08/2003
 */

public interface MultilingualDocumentDAO {
    MultilingualDocumentVO load(String path) throws DataAccessException;

    boolean save(final MultilingualDocumentVO text, final String path) throws DataAccessException;
}
