# Multilang docs XML2any (v02)

## Created
2007-06-21 (originally created 2001-11-21).

## Description
Create multilingual documents using XML. The format is slightly
different from other versions and the texts are generic vocabulary
texts.

Used service at http://www.hitsw.com/xml_utilites/ to generate DTD.

