# multilang data files

Multilingual texts, documents and images.

This repository only contains data. It does not
contain any programs or logic to transform or
interpret the data.

Created 2017-04-03 (originally created: 2007-02-01)

+ html -> multilang documents in (X)HTML format
+ txt  -> multilang documents in text format 
+ odt  -> multilang files in ODT file format


## Content

- html: HTML documents
- images: images referenced by other texts
- odt: documents in OpenDocument Text format
- plain-text: plain text multilingual documents


## XML files

XML files (and XSLT files) are stored in their own
repository.


## HTML files

HTML documents are a collection of documents
containing texts in English, French and German.
Multilingual documents in the form of (X)HTML
documents. The aim to store multilingual documents
in (X)HTML format ("``*.html``", "``*.htm``" or
"``*.xhtml``").

This project was originally started on 20030722.

The `index.html` file contains a complete listing of all
the HTML and XML examples.

### Files
- 000-template-lib-oli-lang.html (template)
- 000-template-quadrilingual.html (template) (2002-11-25)
- css/000-template-lib-oli-lang.css (template)
- css/000-template-quadrilingual.css (template) (2001-09-12)
- bn-chocolates.html (2001-09-21)
- cinema-and-films.html
- colours.html (2001-09-21)
- cote-dor.html (2001-09-21)
- creme-a-recurer.html (2001-09-21)
- education.html (2001-09-21)
- eurostar-no-smoking-please.html (2001-09-21)
- eurostar-ticket-info.html (2001-09-21)
- leonidas.html (2001-06-30)
- liege.html (2001-09-22)
- marriage.html (2001-09-01)
- mr-piano.html
- quadrilingual.html and holyday-inn-japanese.gif were both created on
  2000-12-27. This document deals with 4 languages: English, French,
  German and Japanese.
- shorthand-notebook.html
- sole.html (2001-09-21)
- thomas-sit-and-ride.html (2001-10-12)
- trilingual-v01.html (2000-06-13)
- trilingual-v02.html (2000-07-06)
- trilingual-v03.html (2000-11-28)
- trilingual-v04.html (2001-06-28)
- walldorf.html (2001-09-21)
- winnie-musical-mobile.html
- wrapping-paper.html (2001-09-21)

## Plain text files

Multilingual documents in the form of plain text files. The aim is to
store multilingual documents in plain text format ("``*.txt``").


## Building

To build the whole project, ANT 1.6 or above is required.
Simply run the `build.xml` file in the root directory of this project.
