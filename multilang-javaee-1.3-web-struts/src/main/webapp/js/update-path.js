var rootField = null ;
var rootFieldValue = null ;

function setRoot() {
  rootField = document.forms[0].docPath ;
  rootFieldValue = rootField.value ;
  //alert( rootFieldValue ) ;
}

function completePath() {
  //alert( root ) ;
  var docPathField = document.forms[0].docPath ;
  var docPathFieldValue = docPathField.value ;
  var docTitleField = document.forms[0].docTitle.value ;

  // Replace all whitespace with a single underscore. If, for example,
  // there are 3 spaces, these 3 spaces will be replaced by a single underscore.
  docTitleField = docTitleField.replace( /\s+/g, '_' ) ;

  // Replace all punctuation characters by an underscore and ensure that
  // thoise characters that are used by Regular Expressions are
  // properly escaped.
  docTitleField = docTitleField.replace( /\?/g, '_' ) ;
  docTitleField = docTitleField.replace( /\[/g, '_' ) ;
  docTitleField = docTitleField.replace( /\]/g, '_' ) ;
  docTitleField = docTitleField.replace( /\./g, '_' ) ;
  docTitleField = docTitleField.replace( /\+/g, '_' ) ;
  docTitleField = docTitleField.replace( /\|/g, '_' ) ;
  docTitleField = docTitleField.replace( /\^/g, '_' ) ;
  docTitleField = docTitleField.replace( /\(/g, '_' ) ;
  docTitleField = docTitleField.replace( /\)/g, '_' ) ;
  docTitleField = docTitleField.replace( /\*/g, '_' ) ;
  docTitleField = docTitleField.replace( /\\/g, '_' ) ; // escape the backslash (\)
  docTitleField = docTitleField.replace( /\//g, '_' ) ; // escape the forward slash (/)

  docTitleField = docTitleField.replace( /!/g, '_' ) ;
  docTitleField = docTitleField.replace( /,/g, '_' ) ;
  docTitleField = docTitleField.replace( /:/g, '_' ) ;
  docTitleField = docTitleField.replace( /;/g, '_' ) ;
  docTitleField = docTitleField.replace( /~/g, '_' ) ;
  docTitleField = docTitleField.replace( /#/g, '_' ) ;
  docTitleField = docTitleField.replace( /-/g, '_' ) ;
  docTitleField = docTitleField.replace( /=/g, '_' ) ;
  docTitleField = docTitleField.replace( /{/g, '_' ) ;
  docTitleField = docTitleField.replace( /}/g, '_' ) ;
  docTitleField = docTitleField.replace( /%/g, '_' ) ;
  docTitleField = docTitleField.replace( /�/g, '_' ) ;
  docTitleField = docTitleField.replace( /`/g, '_' ) ;
  docTitleField = docTitleField.replace( /"/g, '_' ) ;
  docTitleField = docTitleField.replace( /'/g, '_' ) ;
  docTitleField = docTitleField.replace( /</g, '_' ) ;
  docTitleField = docTitleField.replace( />/g, '_' ) ;
  docTitleField = docTitleField.replace( /&/g, '_' ) ;
  docTitleField = docTitleField.replace( /@/g, '_' ) ;


  // If, for any reason there are several underscores following each
  // other ensure that these are replaced by one and ony one underscore.
  docTitleField = docTitleField.replace( /_+/g, '_' ) ;

  // Convert entered character to lower case
  docTitleField = docTitleField.toLowerCase() ;

  // Append character to the other field
  docPathField.value = rootFieldValue + docTitleField ;
}