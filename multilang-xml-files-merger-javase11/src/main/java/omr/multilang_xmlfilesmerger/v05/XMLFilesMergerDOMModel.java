package omr.multilang_xmlfilesmerger.v05;

import java.io.IOException;
import java.util.List;

/**
 * Create a large XML file based on the input from several smaller XML files
 * using DOM.
 * <p>
 * Creation date: (06/02/03 10:17:45)
 *
 * @author: Olivier Renard
 */
public class XMLFilesMergerDOMModel extends XMLFilesMergerModel {
    /**
     * XMLFilesMergerModelDOM constructor comment.
     */
    public XMLFilesMergerDOMModel() {
        super();
    }

    /**
     * Checks where the data (parameters) to be used for generating the output XML
     * file is valid.
     * <p>
     * Creation date: (06/02/03 10:43:19)
     *
     * @return java.util.List
     */
    @Override
    public List<String> checkParameters() {
        return super.checkParameters();
    }

    /**
     * Generate output file.
     * <p>
     * Method to be implemented by by all implementing classes.
     * <p>
     * Creation date: (06/02/03 10:17:45)
     *
     * @throws IOException The exception description.
     */
    @Override
    public void generateOutputFile() throws IOException {
    }
}
