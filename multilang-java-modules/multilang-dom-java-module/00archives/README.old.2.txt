README


% mode line for jEdit; line starting with a percent
% sign is a comment for txt2tags
% :mode=text:indentSize=2:folding=indent:
% :maxLineLen=72:noTabs=true:tabSize=2:wrap=hard:


Created
=======
2007-04-02


Description
===========
Define and clarify an object model for multilingual documents using Java
SE 6.0.
