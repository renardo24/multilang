title
de
Laufsohle
en
Shoe sole
fr
Semelle de chaussure

line
de
Futter (synthetisch)
en
Futter (synthetic)
fr
Doublure (synthétique)

line
de
Innensohle (synthetisch)
en
Inlay sole (synthetic)
fr
Semelle intérieure (synthétique)

line
de
Laufsohle (Kunststoff)
en
Outsole (rubber)
fr
Semelle (caoutchouc)

