package omr.multilang.action;

import java.io.File;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import omr.multilang.MultilangConstants;

public class ListAction extends org.apache.struts.action.Action {
	public ListAction() {
		super();
	}

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		File rootDir = new File(servlet.getServletContext().getAttribute(
				"original-path").toString());
		ArrayList al = new ArrayList();
		File[] files = rootDir.listFiles();
		for (int i = 0; i < files.length; i++) {
			if (files[i].getName().endsWith(".xml")) {
				al.add(files[i]);
			}
		}

		request.setAttribute("list", al);

		return mapping.findForward(MultilangConstants.FORWARD_SUCCESS);
	}
}