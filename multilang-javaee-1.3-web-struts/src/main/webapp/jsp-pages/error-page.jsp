<%@ page isErrorPage="true" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c" %>
<html:html locale="true">
<head>
  <title><bean:message key="title" /> - <bean:message key="label.error.page" /></title>
  <link href="<html:rewrite page='/theme/global.css'/>" rel="stylesheet" type="text/css" />
</head>
<body>
<%@ include file="../jspf/navbar.jspf"%>
<br />

<h1><bean:message key="label.error.page" /></h1>

<h3 align="center"><bean:message key="message.error.page" /></h3>
<p>
<c:out value="${pageContext.exception}" />
<c:choose>
  <c:when test="${!empty pageContext.exception.cause}">
  : <c:out value="${pageContext.exception.cause}" />
  </c:when>
  
  <c:when test="${!empty requestScope['javax.servlet.error.exception']}">
	<h4>Stacktrace</h4>
	<c:out value="${requestScope['javax.servlet.error.exception']}" />
  </c:when>
<%-- 
  <c:when test="${!empty pageContext.exception.rootCause}">
  : <c:out value="${pageContext.exception.rootCause}" />
  </c:when>
--%>
</c:choose>



<hr />

<%@ include file="../jspf/footer.jspf"%>

</html:html>