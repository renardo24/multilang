#!/usr/bin/env bash
#
#  Olivier Renard
#  2007-04-02
#
#  Compiling Multilang DAOs JavaSE
#

PROMPT_COMMAND='echo -ne "/033]0;Compiling Multilang DAOs JavaSE/007"'

CWD=`pwd`;
cd ${CWD}/..

[ ! -d ./target/classes ] && mkdir -p ./target/classes
[ ! -d ./tmp ] && mkdir ./tmp;

"${JDK_HOME}/bin/javac" -cp "${M2_REPO}/org/jdom/jdom2/jdom-2.0.6.jar;./src/main/java" -d ./target/classes ./src/main/java/omr/multilang/daos/*.java 2>>./tmp/compile_errors

cd ${CWD}
