title
en
On the tags of a pair of Levi's trousers
fr
Sur une paire de pantalon Levi's

line
en
Bar tacked at points of strain
fr
Point d'arre^t pour renforcer les coutures

line
en
America's original jeans - since 1850
fr
L'authentique jean am�ricain - depuis 1850

line
en
Preshrunk jeans
fr
Jean pr�lav�

line
en
Levi's 501 jeans, America's original jeans since 1850, now come washed and shrunk for your convenience.
fr
Ce jean Levi's 501 en denim super r�sistant a �t� pr�lav� et r�tr�ci pour un meilleur confort.

line
en
Please note that the size shown on the garment is the size after washing.
fr
La taille indiqu�e sur le jean tient compte de ce r�tr�cissement, choisissez votre taille habituelle.

line
en
To ensure a proper fit, please try on before buying and expect further shrinkage of approximately 3% in length.
fr
(L�ger retrait de 3% en longeur au premier lavage).

line
en
Original Levi's 501 button-fly jeans.
fr
L'authentique jean Levi's 501 braguette boutons.

line
en
The coloured Tab and stitched pocket designs are registered trademarks to help you identify garments made only by Levi Strauss & Co.
fr
La griffe Levi's en tissu et le dessin des surpiqures des poches sont des marques d�pos�es qui vous permettent de reconna�tre ce jean de la marque Levi's.

line
de
Aufgrund der tiefen F�rbung k�nnen Kleidungsst�cke oder Polster, die mit der Jeans in Verbindung kommen, anfangs etwas Farbe annehmen. Dieser Effekt legt sich nach den ersten W�schen.
en
Given the rich dye characteristic of this product, colour may transfer when new onto other garments or upholstery.
fr
La forte densit� de la teinture de ce produit peut provoquer quand il est neuf un ph�nom�ne de d�gorgement sur d'autres v�tements et articles en tissue ou cuir.

