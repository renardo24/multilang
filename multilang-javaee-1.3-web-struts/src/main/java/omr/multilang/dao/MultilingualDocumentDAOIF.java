package omr.multilang.dao;

import omr.multilang.vo.MultilingualDocumentVO;

/**
 * Interface for the Data Access Object to be used for MultilingualDocumentVO
 * objects.
 * 
 * Objects implementing this interface needs to implement a form of data access
 * mechanism in order to support a form of persistence for these VO objects.
 * 
 * Enables decoupling between VO objects and data access implementation.
 * 
 * @author Olivier Renard
 * @version 1.1 - 06/08/2003
 */

public interface MultilingualDocumentDAOIF {
	public MultilingualDocumentVO load(String path) throws DataAccessException;

	public boolean save(MultilingualDocumentVO text, String path)
			throws DataAccessException;
}
