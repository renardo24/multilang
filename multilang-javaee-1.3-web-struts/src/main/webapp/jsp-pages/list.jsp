<%@ page errorPage="error-page.jsp" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c" %>
<html:html locale="true">
<head>
  <title><bean:message key="title" /> - <bean:message key="label.list.all" /></title>
  <link href="<html:rewrite page='/theme/global.css'/>" rel="stylesheet" type="text/css" />
</head>
<body>
<%@ include file="../jspf/navbar.jspf"%>
<br />

<div align="center" class="error">
  <html:errors />
</div>

<h1><bean:message key="label.list.all" /></h1>

<%-- <c:out value="${requestScope.list}" /> --%>

<c:choose>
  <c:when test="${empty requestScope.list}">
    <p style="text-align: center"><bean:message key="message.empty.root.dir" /></p>
  </c:when>
  <c:otherwise>
    <table border="1" bordercolor="#CECF9C" cellpadding="11" cellspacing="0" width="100%">
    <c:forEach items="${requestScope.list}" var="text" varStatus="vs">
      <tr>
        <td width="10%"><c:out value="${vs.count}" />.</td>
        <td width="90%"><html:link action="/action/view/entries"
                                   paramId="doc"
                                   paramName="text">
                          <c:out value="${text.name}" />
                        </html:link>
        </td>
      </tr>
    </c:forEach>
    </table>
  </c:otherwise>
</c:choose>

<hr />

<%@ include file="../jspf/footer.jspf"%>

</body>
</html:html>