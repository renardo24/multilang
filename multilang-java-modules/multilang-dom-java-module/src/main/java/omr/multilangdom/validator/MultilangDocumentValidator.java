/*
 * MultilangDocumentValidator.java
 *
 * Created on 16 February 2006, 10:02
 */

package omr.multilangdom.validator;

import omr.multilangdom.InvalidMultilangDocumentException;
import omr.multilangdom.MultilangDocument;

/**
 * Validator of multilang documents.
 *
 * @author Olivier Renard
 */
public class MultilangDocumentValidator {

    /**
     * Creates a new instance of <code>MultilangDocumentValidator</code>.
     * <p>
     * Private constructor to prevent external instantiation.
     */
    private MultilangDocumentValidator() {
        super();
    }

    /**
     * Validates a <code>MultilangDocument</code> object. If the multilang document
     * is considered invalid, then an <code>InvalidMultilangDocumentException</code>
     * is thrown.
     *
     * @param multilangDocument the multilang document.
     * @throws InvalidMultilangDocumentException If the document is considered to be invalid.
     */
    public static void validate(final MultilangDocument multilangDocument) throws InvalidMultilangDocumentException {
        boolean valid = true;
        String errorMsg = null;
        if (multilangDocument != null) {
            valid = false;
            errorMsg = "Invalid multilang document: null";
        } else {
            // validate the contents of the multilang document object
            // TODO
        }

        if (!valid) {
            throw new InvalidMultilangDocumentException(errorMsg);
        }
    }
}
