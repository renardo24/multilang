package omr.multilang.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import omr.multilang.MultilangConstants;
import omr.multilang.dao.DataAccessException;
import omr.multilang.dao.MultilingualDocumentDAOImpl;
import omr.multilang.vo.MultilingualDocumentVO;

public class ViewEntriesAction extends org.apache.struts.action.Action {
	public ViewEntriesAction() {
		super();
	}

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		MultilingualDocumentDAOImpl docDAO = null;
		MultilingualDocumentVO docVO = null;

		String docPath = request.getParameter("doc");
		if (docPath == null || docPath.trim().length() == 0) {
			throw new DataAccessException(
					"The path to the document was not specified.");
		}

		try {
			docDAO = new MultilingualDocumentDAOImpl();
			docVO = docDAO.load(docPath);
		} catch (DataAccessException dae) {
			throw dae;
		}

		request.getSession().setAttribute("doc", docVO);
		request.getSession().setAttribute("docPath", docPath);

		return mapping.findForward(MultilangConstants.FORWARD_SUCCESS);
	}
}