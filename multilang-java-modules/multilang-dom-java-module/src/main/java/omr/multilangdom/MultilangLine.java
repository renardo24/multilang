/*
 * MultilangLine.java
 *
 * Created on 16 February 2006, 10:56
 */

package omr.multilangdom;

import java.io.Serializable;

/**
 * A multilang line, which can contain text or an image (or both) for a specific
 * foreign language.
 *
 * @author Olivier Renard
 */
public class MultilangLine implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -2435112432653836561L;

    /**
     * The id of the line.
     */
    private long lineId = -1;

    /**
     * The text.
     */
    private String text = null;

    /**
     * The image bytes.
     */
    private byte[] image = null;

    /**
     * Creates a new instance of <code>MultilangLine</code> with a line id.
     *
     * @param lineId the line id.
     */
    public MultilangLine(final long lineId) {
        super();
        this.setLineId(lineId);
    }

    /**
     * Returns true if object is equal to this instance; false otherwise.
     *
     * @param object the object to make the comparison against.
     * @return true if object is equal to this instance; false otherwise.
     */
    @Override
    public boolean equals(final Object object) {
        if (object == null) {
            return false;
        }

        // Not strictly necessary, but often a good optimization
        if (this == object) {
            return true;
        }

        if (!(this.getClass()
                .equals(object.getClass()))) {
            return false;
        }

        MultilangLine mline = (MultilangLine) object;

        boolean result = false;
        boolean lineIdEqual = lineId == mline.getLineId();
        boolean textEqual = (text == null) ? (mline.getText() == null) : (text.equals(mline.getText()));
        boolean imageEqual = (image == null) ? (mline.getImage() == null) : (image.equals(mline.getImage()));

        result = lineIdEqual && textEqual && imageEqual;

        return result;
    }

    /**
     * Returns the hashcode of this instance.
     *
     * @return the hashcode of this instance.
     */
    @Override
    public int hashCode() {
        int hash = 1;
        int primeNumber = 17;

        hash = hash * primeNumber + ((int) lineId);
        hash = hash * primeNumber + ((text != null) ? text.hashCode() : 0);
        hash = hash * primeNumber + ((image != null) ? image.hashCode() : 0);

        return hash;
    }

    /**
     * Returns a String representation of this instance.
     *
     * @return a String representation of this instance.
     */
    @Override
    public String toString() {
        String sep = System.getProperty("line.separator");
        StringBuilder buf = new StringBuilder();
        buf.append("\t\tTEXT: ")
                .append(text)
                .append(sep);
        if (image != null) {
            buf.append("\t\tIMAGE: ");
            for (int i = 0; i < image.length; i++) {
                buf.append(image[i]);
            }
            buf.append(sep);
        }
        return buf.toString();
    }

    /**
     * Returns the line id.
     *
     * @return the line id.
     */
    public long getLineId() {
        return lineId;
    }

    /**
     * Sets the line id.
     *
     * @param lineId the line id.
     */
    public void setLineId(final long lineId) {
        this.lineId = lineId;
    }

    /**
     * Returns the text.
     *
     * @return the text.
     */
    public String getText() {
        return text;
    }

    /**
     * Sets the text.
     *
     * @param text the text.
     */
    public void setText(final String text) {
        this.text = text;
    }

    /**
     * Returns the image bytes.
     *
     * @return the image bytes.
     */
    public byte[] getImage() {
        return image;
    }

    /**
     * Sets the image bytes.
     *
     * @param image the image bytes.
     */
    public void setImage(final byte[] image) {
        this.image = image;
    }
}
