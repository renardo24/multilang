======
README
======

Created
  2006-06-21 (Originally created 2003-07-22)

Aim
  Store all data and templates for multilingual documents in XML format in one
  place. Templates are provided as well as XSLT stylesheets to convert XML
  documents into alternate formats.

  v06 (2003-09-15)
  ----------------------------------------------------------------------------
    Create multilingual documents using XML. Provide templates. Provide XSLT
    stylesheets to convert XML documents into alternate formats. This previous
    version is the latest version and the one that is currently maintained.

  v05 (2003-02-05)
  ----------------------------------------------------------------------------
    Store multilingual documents and texts in simple XML files (.xml). The
    multilingual texts contain sentences in English, French, and German 
    in XML format.

  v04 (2003-08-26)
  ----------------------------------------------------------------------------
    Version 04 is very similar to version 03. The difference is that every
    <entry> tag has a "id" attribute with a unique value.

  v03 (2003-02-04)
  ----------------------------------------------------------------------------
    Create multilingual documents using XML. Provide templates. Provide XSLT
    stylesheets to convert XML documents into alternate formats. Version 02 is
    also an old version, but not as crude as version 01. However, it is no
    longer maintained either.
        
    DTD/Schema
    ----------
    * multilingual_xml_texts.dtd (2003-07-22)
    * multilingual_xml_texts.xsd (2003-07-22)
    * Used service at http://www.hitsw.com/xml_utilites/ to generate DTD.
    * Used service at http://www.hitsw.com/xml_utilites/ to generate Schema.
    
    TEMPLATE
    --------
    * multilingual_template.xml (2002-10-25)

    CSS
    ---
    * multilingual.css (2003-02-04)

  v02 (2001-11-21)
  ----------------------------------------------------------------------------
    Create multilingual documents using XML. The format is slightly different
    from other versions and the texts are generic vocabulary texts.

  v01 (2003-07-22)
  ----------------------------------------------------------------------------
    Create multilingual documents using XML. Provide templates. Provide XSLT
    stylesheets to convert XML documents into alternate formats.

    Version 01 is an old version (and cruder version) than any other version.
    Version 01 is no longer maintained.

    File format
    -----------
    * All multilingual information or text should be contained in its own
      file. For example, all information about James Ensor, should be
      contained in its own file.
    * Each file should be in its own XML file.

    Using XmlFilesMerger
    --------------------
    * To include all the XML files within a single large XML file, just run
      the small utility provided by the XmlFilesMerger Java application.
    * The output file (whose name is set in the properties file) will be saved
      in the same directory as the one where the Java application resides
      unless you specifiy a different path in the properties file.
    
    Using XSL
    ---------
    * To apply the XSL stylesheet, i.e. to transform the XML document into an
      HTML document, use the style sheets provided in the xslt directory.
    * To create an HTML document for either one single, small XML file, or one
      single, large XML file which was created as a result of using the
      XmlFilesMerger utility, use the multilingual.xsl style sheet and the
      multilingual.bat file.
    
    TEMPLATE
    --------
    * xml-template.xml (2001-11-28)
    
    SCHEMATA
    --------
    * multilang-single-doc.dtd (2003-04-03)
    * multilang-many-docs.dtd (2003-04-03)
    * Used service at http://www.hitsw.com/xml_utilites/ to generate DTD.
    
    CSS
    ---
    * print.css (2001-11-28)
    * screen.css (2001-11-28)
    
    BIN
    ---
    * multilingual.xsl (2001-11-28)
    * multilingual.bat (2001-12-31)

