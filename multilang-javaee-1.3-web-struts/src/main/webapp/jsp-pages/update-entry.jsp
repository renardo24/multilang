<%@ page errorPage="error-page.jsp" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c" %>
<html:html locale="true">
<head>
  <title><bean:message key="title" /> - <bean:message key="label.update.entry" /></title>
  <link href="<html:rewrite page='/theme/global.css'/>" rel="stylesheet" type="text/css" />
  <script language="JavaScript" src="<html:rewrite page='/js/add-special-char.js' />"></script>
</head>
<body>
<%@ include file="../jspf/navbar.jspf"%>
<br />

<div align="center" class="error">
  <html:errors />
</div>

<logic:present name="error.message">
  <bean:write filter="false" name="error.message" />
</logic:present>

<logic:present name="success.message">
  <bean:write filter="false" name="success.message" />
</logic:present>

<h1><bean:message key="label.update.entry" />: <c:out value="${sessionScope.entryId}" /></h1>

<div align="center">

  <center>

    <html:form action="/action/update/entry/2" focus="englishText">

      <table border="1" bordercolor="#CECF9C" cellpadding="11" cellspacing="0" width="100%">
        <tr>
          <th width="10%" valign="top" align="center"><bean:message key="label.language"/></th>
          <th width="60%" valign="top" align="center"><bean:message key="label.text"/></th>
          <th width="30%" valign="top" align="center"><bean:message key="label.special.chars"/></th>
        </tr>
        <tr>
          <td  width="10%" valign="middle" align="center"><bean:message key="label.english"/></td>
          <td width="60%" valign="top" align="left">
            <center>
              <textarea name="englishText" cols="90" rows="5" tabindex="1" onfocus="setFocus( this );"><c:out value="${requestScope.entry.englishText}" /></textarea>
            </center>
          </td>
          <td width="30%" rowspan="4" valign="top" align="left">
            <h2><bean:message key="label.french.chars"/></h2>
            <input type="button" class="specialChar" value="�" name="a_grave_upper"     onclick="addSpecialChar( this )" />
            <input type="button" class="specialChar" value="�" name="a_chapeau_upper"   onclick="addSpecialChar( this )" />
            <input type="button" class="specialChar" value="�" name="ae_ligature_upper" onclick="addSpecialChar( this )" />
            <input type="button" class="specialChar" value="�" name="c_cedille_upper"   onclick="addSpecialChar( this )" />
            <input type="button" class="specialChar" value="�" name="e_acute_upper"     onclick="addSpecialChar( this )" />
            <input type="button" class="specialChar" value="�" name="e_grave_upper"     onclick="addSpecialChar( this )" />
            <input type="button" class="specialChar" value="�" name="e_chapeau_upper"   onclick="addSpecialChar( this )" />
            <input type="button" class="specialChar" value="�" name="e_umlaut_upper"    onclick="addSpecialChar( this )" />
            <input type="button" class="specialChar" value="�" name="i_chapeau_upper"   onclick="addSpecialChar( this )" />
            <input type="button" class="specialChar" value="�" name="i_umlaut_upper"    onclick="addSpecialChar( this )" />
            <input type="button" class="specialChar" value="�" name="i_chapeau_upper"   onclick="addSpecialChar( this )" />
            <input type="button" class="specialChar" value="�" name="oe_ligature_upper" onclick="addSpecialChar( this )" />
            <input type="button" class="specialChar" value="�" name="u_grave_upper"     onclick="addSpecialChar( this )" />
            <input type="button" class="specialChar" value="�" name="u_chapeau_upper"   onclick="addSpecialChar( this )" />
            <input type="button" class="specialChar" value="�" name="a_grave_lower"     onclick="addSpecialChar( this )" />
            <input type="button" class="specialChar" value="�" name="a_chapeau_lower"   onclick="addSpecialChar( this )" />
            <input type="button" class="specialChar" value="�" name="ae_ligature_lower" onclick="addSpecialChar( this )" />
            <input type="button" class="specialChar" value="�" name="c_cedille_lower"   onclick="addSpecialChar( this )" />
            <input type="button" class="specialChar" value="�" name="e_acute_lower"     onclick="addSpecialChar( this )" />
            <input type="button" class="specialChar" value="�" name="e_grave_lower"     onclick="addSpecialChar( this )" />
            <input type="button" class="specialChar" value="�" name="e_chapeau_lower"   onclick="addSpecialChar( this )" />
            <input type="button" class="specialChar" value="�" name="e_umlaut_lower"    onclick="addSpecialChar( this )" />
            <input type="button" class="specialChar" value="�" name="i_chapeau_lower"   onclick="addSpecialChar( this )" />
            <input type="button" class="specialChar" value="�" name="i_umlaut_lower"    onclick="addSpecialChar( this )" />
            <input type="button" class="specialChar" value="�" name="i_chapeau_lower"   onclick="addSpecialChar( this )" />
            <input type="button" class="specialChar" value="�" name="oe_ligature_lower" onclick="addSpecialChar( this )" />
            <input type="button" class="specialChar" value="�" name="u_grave_lower"     onclick="addSpecialChar( this )" />
            <input type="button" class="specialChar" value="�" name="u_chapeau_lower"   onclick="addSpecialChar( this )" />
            <input type="button" class="specialChar" value="�" name="left_quot_mark"    onclick="addSpecialChar( this )" />
            <input type="button" class="specialChar" value="�" name="right_quot_mark"   onclick="addSpecialChar( this )" />
            <br />
            <br />
            <h2><bean:message key="label.german.chars"/></h2>
            <input type="button" class="specialChar" value="�" name="a_umlaut_upper" onclick="addSpecialChar( this )">
            <input type="button" class="specialChar" value="�" name="o_umlaut_upper" onclick="addSpecialChar( this )">
            <input type="button" class="specialChar" value="�" name="u_umlaut_upper" onclick="addSpecialChar( this )">
            <input type="button" class="specialChar" value="�" name="a_umlaut_lower" onclick="addSpecialChar( this )">
            <input type="button" class="specialChar" value="�" name="o_umlaut_lower" onclick="addSpecialChar( this )">
            <input type="button" class="specialChar" value="�" name="u_umlaut_lower" onclick="addSpecialChar( this )">
            <input type="button" class="specialChar" value="�" name="sz" onclick="addSpecialChar( this )">
          </td>
        </tr>
        <tr>
          <td width="10%" valign="middle" align="center"><bean:message key="label.french"/></td>

          <td width="60%" valign="top" align="left">
            <center>
              <textarea name="frenchText" cols="90" rows="5" tabindex="2" onfocus="setFocus( this );"><c:out value="${requestScope.entry.frenchText}" /></textarea>
            </center>
          </td>
        </tr>
        <tr>
          <td width="10%" valign="middle" align="center"><bean:message key="label.german"/></td>
          <td width="60%" valign="top" align="left">
            <center>
              <textarea name="germanText" cols="90" rows="5" tabindex="3" onfocus="setFocus( this );"><c:out value="${requestScope.entry.germanText}" /></textarea>
            </center>
          </td>
        </tr>
        <tr>
          <td width="10%" valign="middle" align="center"><bean:message key="label.japanese"/></td>
          <td width="60%" valign="top" align="left">
            <center>
              <textarea name="japaneseText" cols="90" rows="5" tabindex="4" onfocus="setFocus( this );"><c:out value="${requestScope.entry.japaneseText}" /></textarea>
            </center>
          </td>
        </tr>
        <tr>
          <th colspan="3" width="100%"><center><html:reset tabindex="5"><bean:message key="button.reset"/></html:reset>&nbsp;&nbsp;&nbsp;<html:submit tabindex="6"><bean:message key="button.update"/></html:submit></center></td>
          </td>
        </tr>
      </table>

      <hr />

    </html:form>

  </center>

</div>

<%@ include file="../jspf/footer.jspf"%>

</body>
</html:html>