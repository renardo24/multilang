title
en
Sansa Clip

line
de
Wiedergabe von Audiodateien im MP3-, WMA-, Secure WMA-, Ogg Vorbis und Audible-Format
en
Playes your MP3, WMA, secure WMA, Ogg Vorbis  and Audible audio file formats
fr
Lit les fichiers audio MP3, WMA, s�cure WMA, Ogg Vorbis et Audible

line
de
UKW-Radio
en
FM radio
fr
Radio FM

line
de
Sprachaufnahmen mit eingebautem Mikrofon
en
Voice recording with built-in microphone
fr
Dictaphone avec micro int�gr�

line
de
Bis zu 15 Stunden Wiedergabe mit integriertem Akku
en
Up to 15 hours of play time with internal rechargeable battery
fr
Jusqu'� 15 heures de lecture avec batterie rechargeable interne
