title
en
Tuneyville Choo Choo, Toy from TOMY
fr
Sur l'emballage d'un jouet TOMY (Tuneyville Choo Choo)

line
de
Diese Angeabe bitte aufbewahren.
en
Please retain for information.
fr
Pri�re de conserver � titre d'information.

line
de
Entfernen Sie das gesamte Verpackungsmaterial, bevor Sie Ihrem Kind das
Spielzeug geben.
en
Dispose of all packaging safely before you give the toy to your child.
fr
Retirez tout emballage avant de donner le jouet &agrave; votre enfant.

line
de
Farbabweichung zwischen Produkt und Abbildung m�glich.
en
Colour and contents may vary.
fr
Les couleurs et les d�tails peuvent varier.

line
de
Tomy empfiehlt, Duracell Powercheck-Baterrien zu verwenden.
en
Tomy recommend the use of Duracell Powercheck batteries.
fr
Tomy vous conseille d'utiliser des piles Duracell Powercheck.

line
de
Batterien nicht enthalten.
en
Batteries not included.
fr
Piles non comprises.

line
de
Tuneyville Choo Choo: Die musikalischste Lokomotive der Welt! Sie spielt
elf verschiedene Kinderlieder. Man braucht sich nur seine Lieblingsmelodie
herauszusuchen, die Platte einlegen, und schon tuckert der Zug los und
spielt das gew�hlte Lied.
en
Tuneyville Choo Choo: The most tuneful engine in the world! There are
eleven different nursery tunes to play. Choose your favourite records and
simply drop in the disc to listen to your tune as the train chugs around.
fr
Choo Choo: La locomotive la plus m�lodieuse du monde! Il te suffit de
choisir tes chansons favorites parmi onze m�lodies diff�rentes et
d'introduire le disque. Le train avance au son de la m�lodie.