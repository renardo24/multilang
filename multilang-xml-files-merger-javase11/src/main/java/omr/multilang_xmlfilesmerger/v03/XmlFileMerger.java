package omr.multilang_xmlfilesmerger.v03;

/**
 * Application class.
 *
 * @author Olivier Renard
 */

public class XmlFileMerger {
    public static void main(final String args[]) {
        XMlFilesMergerModel model = new XMlFilesMergerModel();
        model.loadData();
        System.out.println(model.getInputDirectory());
        System.out.println(model.getOutputDirectory());
        System.out.println(model.getOutputFileName());
        System.out.println(model.getXmlDeclaration());
        System.out.println(model.getDoctypeDeclaration());
        System.out.println(model.getInputStartElement());
        System.out.println(model.getOutputRootElement());
    }
}