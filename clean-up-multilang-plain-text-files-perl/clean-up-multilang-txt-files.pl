#!/usr/bin/perl

use File::Spec;
use File::Path 'mkpath';
use File::Path 'rmtree';
use strict;

my $file;
my $fullInputPath;
my $fullOutputPath;
my $mainDir;
my $newFile;
my $line;
my @fileList;

#$HOME dir
#my $homeDir = $ENV{"HOME"};
#my $mainDir = $homeDir . "/omr/04-archives/01-multilingual-texts";
#my $inputDir = $mainDir . "/txt";
#my $outputDir = $mainDir . "/cleaned-up-txt";

my $mainInputDir = "../test-data";
my $inputDir = $mainInputDir . "/for-clean-up";
my $mainOutputDir = "./";
my $outputDir = $mainOutputDir . "/cleaned-up-txt";

#print($homeDir."\n");
print($mainDir."\n");
print($inputDir."\n");

#check input dir exists
die "${mainInputDir} is not a directory" if !( -d ${mainInputDir} && -r ${mainInputDir} );
die "${inputDir} is not a directory" if !( -d ${inputDir} && -r ${inputDir} );
#check if output dir exists; if so delete
if ( -d ${outputDir} ) { rmtree(${outputDir}); }
#check if output dir exists; if not create it
if (! -d ${outputDir} ) { mkpath(${outputDir}); }

opendir(INPUTDIR, ${inputDir}) || die "Can't open directory ${inputDir}: $!\n";
@fileList = readdir ( INPUTDIR ); # list all files
closedir(INPUTDIR);

foreach $file ( @{fileList} ) {
    print "FILE: ${file}\n";
    my $newFile = "";
    if (${file} ne "." && ${file} ne "..") {
         $fullInputPath = File::Spec->catfile(${inputDir}, ${file});
         print "${fullInputPath}\n";
         if ( -f ${fullInputPath} ) {
             open ( FILE, ${fullInputPath} ) || die "Can't open file '$fullInputPath': $!\n";
             while ( ${line} = <FILE> ) {
                 $line =~ s/^\s+//;
				 $line =~ s/\s+$//;
                 $line =~ s/^\s*$//gi;
                 $line =~ s/^De$/de/i;
                 $line =~ s/^En$/en/i;
                 $line =~ s/^Fr$/fr/i;
                 $line =~ s/^Title$/title/i;
                 $line =~ s/^Line$/line/i;
                 $line =~ s/\n*^line$/\nline/m; # word "line" preceded by multiple \n replaced by single \n
                 #if ($line =~ /\n$/) { print ("MATCH: ".$line); }
                 #if ($line !=~ /\n$/) { print ("NO MATCH: ".$line."\n"); }
                 if ($line !=~ /\n$/) { $line = $line."\n"; }
                 $newFile .= $line;
             }
             close ( FILE );
             $fullOutputPath = File::Spec->catfile(${outputDir}, ${file});
             print("Output: $fullOutputPath\n");
             open ( OUTFILE, ">${fullOutputPath}" );
             print OUTFILE ${newFile};
             close ( OUTFILE );
         }
    }
}

