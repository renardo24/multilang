package omr.multilang.action;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import omr.multilang.MultilangConstants;
import omr.multilang.vo.MultilingualDocumentVO;
import omr.multilang.vo.MultilingualEntryVO;

public class GetEntryToUpdateAction extends org.apache.struts.action.Action {
	public GetEntryToUpdateAction() {
		super();
	}

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		if (this.isCancelled(request)) {
			return (mapping.findForward("home"));
		}

		Locale locale = getLocale(request);
		MessageResources mr = getResources(request);

		String action = request.getParameter("action");
		if (action == null) {
			action = "getEntryToUpdate";
		}
		servlet.log("GetEntryToUpdateAction: Processing " + action + " action");

		String entryId = request.getParameter("entryId");
		if (entryId == null) {
			// HANDLE ERROR - NO ENTRY ID SPECIFIED
			servlet
					.log("GetEntryToUpdateAction: The entryId has not ben specified");
			request.setAttribute("error.message", mr.getMessage(locale,
					"error.no.entry.id.specified"));
			return (mapping.findForward(MultilangConstants.FORWARD_FAILURE));
		}
		servlet.log("GetEntryToUpdateAction: Processing entry ID: " + entryId);

		HttpSession session = request.getSession();
		Object oDoc = session.getAttribute("doc");
		Object oPath = session.getAttribute("docPath");
		if (oDoc == null) {
			// HANDLE ERROR - NO DOCUMENT IN THE SESSION
			servlet
					.log("GetEntryToUpdateAction: The document does not exists!");
			request.setAttribute("error.message", mr.getMessage(locale,
					"error.no.document.available"));
			return (mapping.findForward(MultilangConstants.FORWARD_FAILURE));
		} else if (oPath == null) {
			// HANDLE ERROR - NO PATH IN THE SESSION
			servlet
					.log("GetEntryToUpdateAction: The document path does not exists!");
			request.setAttribute("error.message", mr.getMessage(locale,
					"error.no.document.path.available"));
			return (mapping.findForward(MultilangConstants.FORWARD_FAILURE));
		} else {
			MultilingualDocumentVO docVO = (MultilingualDocumentVO) oDoc;
			MultilingualEntryVO entryVO = docVO.getEntryById(entryId);
			if (entryVO != null) {
				session.setAttribute("entryId", entryId);
				request.setAttribute("entry", entryVO);
				return (mapping.findForward(MultilangConstants.FORWARD_SUCCESS));
			} else {
				// HANDLE ERROR - NO ENTRY FOR SPECIFIED ID
				servlet
						.log("GetEntryToUpdateAction: No entry for the specified id: "
								+ entryId);
				request.setAttribute("error.message", mr.getMessage(locale,
						"error.no.entry.for.specified.id", entryId));
				return (mapping.findForward(MultilangConstants.FORWARD_FAILURE));
			}
		}
	}
}