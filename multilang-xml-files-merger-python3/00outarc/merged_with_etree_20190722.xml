<merged_multilang><multilingual_entry>
	<titles>
		<title lang="English">Ensor</title>
		<title lang="French">Ensor</title>
		<title lang="German">Ensor</title>
	</titles>
	<entry>
		<line lang="English">Although James Ensor's bold an unusual paintings are now familiar to a fairly wide public, it is not generally realized that his engravings alone would be enough to ensure his reputation. Born in 1860 in Ostend where he died in 1949, he is one of the most important artists of the far-reaching Symbolist Movement at the end of the 19th century. Most of his work as an engraver was completed over a very short period, at a time when he had reached the height of his maturity as a painter. From 1886 to 1888, his output was almost exclusively determined by his discovery of this form, dry points and etchings following one another in frenzied succession. By 1904, at which date he abandoned engraving, he had produced 130 plates.</line>
		<line lang="French">James Ensor (Ostende 1860-1949) est l'un des artistes majeurs du vaste mouvement symboliste, &#195;&#160; la fin du XIXe si&#195;&#168;cle. Si sa peinture, singuli&#195;&#168;re et audacieuse, est bien connue du public, on ignore souvent que son oeuvre grav&#195;&#169;e aurait suffi &#195;&#160; sa gloire. Il r&#195;&#169;alisa la plus grande partie de son oeuvre grav&#195;&#169;e dans un laps de temps tr&#195;&#168;s court, &#195;&#160; une p&#195;&#169;riode o&#195;&#185; il &#195;&#169;tait parvenu &#195;&#160; sa pleine maturit&#195;&#169; en tant que peintre. De 1886 &#195;&#160; 1888, il utilisa presqu'exclusivement ce nouveau mode d'expression, produisant &#195;&#160; un rythme fr&#195;&#169;n&#195;&#169;tique pointes s&#195;&#168;ches et eaux-fortes. En 1904, date o&#195;&#185; il cessa de graver, il avait r&#195;&#169;alis&#195;&#169; 130 planches.</line>
		<line lang="German">James Ensor (Ostende 1860-1949) gilt als einer der bedeutendsten Vertreter der symbolistischen Bewegung um die Jahrhundertwende. Gut bekannt in der &#195;&#150;ffentlichkeit sind eine eigent&#195;&#188;mlichen, gewagten &#195;&#150;lbilder, doch allein sein grafisches Werk h&#195;&#164;tte gen&#195;&#188;gt, um seinen Ruhm zu begr&#195;&#188;nden. Den gr&#195;&#182;&#195;&#159;ten Teil dieses grafischen Werkes schuf er in einer recht kurzen Zeitspanne, als er seine volle k&#195;&#188;nstlerische Reife schon erreicht hatte. Von 1886 bis 1888 bediente er sich fast ausschlie&#195;&#159;lich der neuen Ausdrucksform und fertigte in einem rasanten Tempo eine Kaltnadel- oder &#195;&#132;tzradierung nach der anderen. Als er 1904 zu radieren aufh&#195;&#182;rte, hatte er 130 Stiche geschaffen.</line>
	</entry>
	<entry>
		<line lang="English">Like the rest of his work, his engravings are a succession of landscapes, portraits, masquerades, street scenes and fantastic visions.</line>
		<line lang="French">Ses gravures encha&#195;&#174;nent, comme le reste de son oeuvre, paysages, portraits, sc&#195;&#168;nes de masques, spectacles des rues, visions fantasmagoriques.</line>
		<line lang="German">In seinen Grafiken wie in seinem sonstigen Werk verbinden sich Landschaften, Portr&#195;&#164;ts, Maskenaufz&#195;&#188;ge und Stra&#195;&#159;enszenen zu einem Reigen phantastischer Visionen.</line>
	</entry>
	<entry>
		<line lang="English">Ensor's work, in which in the words of Jean Cassou, "the Angel of the Bizarre holds perpetual sway" looks forward not only to Expressionism but also to Surrealism. His world, insidiously peopled by skulls, skeletons and the disturbing creatures of nightmare, echoes the fantastic imagery of his great predecessors Goya, Bruegel and Bosch.</line>
		<line lang="French">Pr&#195;&#169;curseur de l'Expressionnisme, Ensor l'est aussi du Surr&#195;&#169;alisme par une oeuvre o&#195;&#185;, comme l'&#195;&#169;crit Jean Cassou, "<i>r&#195;&#168;gne &#195;&#160; jamais l'ange du bizarre</i>". Cr&#195;&#162;nes, squelettes et cr&#195;&#169;atures issues d'inqui&#195;&#169;tants cauchemars, envahissent sournoisement l'univers de James Ensor et introduisent le fantastique dans son art, qui rejoint ainsi l'imagerie des grands pr&#195;&#169;d&#195;&#169;cesseurs du graveur: Goya, Bruegel, Bosch.</line>
		<line lang="German">Durch sein Oeuvre, in welchem "f&#195;&#188;r immer der Engel des Bizarren herrscht", wie Jean Cassou schreibt, ist Ensor nicht nur ein Vorl&#195;&#164;ufer des Expressionismus, sondern auch des Surrealismus. Totenschadel, Gerippe und Wesen, die gleichsam einem beklemmenden Alptraum zu entsteigen scheinen und sich hinterh&#195;&#164;ltig in James Ensors Welt einschleichen, verleihen seiner Kunst jenen Zug ins Phantastische, der ihn in eine Reihe stellt mit seinen gro&#195;&#159;en Vorg&#195;&#164;ngern: Goya, Bruegel, Bosch.</line>
	</entry>
	<entry>
		<line lang="English">All the works exhibited in Strasbourg come from the same collection, that of Mira Jacob, who from the 1950s onward ran a gallery in Paris devoted to drawing as an art form, <i>Le Bateau Lavoir</i>, in the rue de Seine. In addition to the black and white engravings, the collection is further enhanced by 25 extremely rare prints with colouring added by the artist.</line>
		<line lang="French">Toutes les oeuvres expos&#195;&#169;es &#195;&#160; Strasbourg proviennent d'une seule et m&#195;&#170;me collection, celle de Mira Jacob qui, &#195;&#160; partir des ann&#195;&#169;es cinquante, anima une galerie, rue de Seine, <i>Le Bateau Lavoir</i>, galerie consacr&#195;&#169;e &#195;&#160; l'art du dessin. A c&#195;&#180;t&#195;&#169; des gravures noir et blanc, 25 &#195;&#169;preuves d'une grande raret&#195;&#169;, rehauss&#195;&#169;es de couleurs par l'artiste, compl&#195;&#168;tent cette collection.</line>
		<line lang="German">S&#195;&#164;mtliche in Stra&#195;&#159;burg gezeigten Bl&#195;&#164;tter kommen aus einer einzigen Quelle, aus der Sammlung von Mira Jacob, die sich seit den f&#195;&#188;nfziger Jahren in ihrer Galerie <i>Le Bateau</i> Laioir in der Rue de Seine f&#195;&#188;r die Kunst des Zeichnens einsetzte. Die aus Schwarzwei&#195;&#159;-Grafiken bestehende Sammlung wird durch 25 &#195;&#164;uberst seltene, vom K&#195;&#188;nstler farbig &#195;&#188;berarbeitete Abz&#195;&#188;ge vervolist&#195;&#164;ndigt.</line>
	</entry>
</multilingual_entry><multilingual_entry>
	<titles>
		<title lang="English">Gift wrap</title>
		<title lang="French">Papier cadeau</title>
		<title lang="German">Geschenkpapier</title>
	</titles>
	<title lang="English">Gift wrap</title>
	<entry>
		<line lang="English">Gift wrap</line>
		<line lang="French">Papier cadeau</line>
		<line lang="German">Geschenkpapier</line>
	</entry>
	<entry>
		<line lang="English">Bleached chlorine-free</line>
		<line lang="French">Blanchi sans chlore</line>
		<line lang="German">Chlorfrei gebleicht</line>
	</entry>
</multilingual_entry><multilingual_entry>
	<titles>
		<title lang="English">On the tags of a pair of Levi's</title>
		<title lang="French">Sur une paire de pantalon Levi's</title>
		<title lang="German" />
	</titles>
	<entry>
		<line lang="English">Bar tacked at points of strain</line>
		<line lang="French">Point d'arr&#195;&#170;t pour renforcer les coutures</line>
		<line lang="German" />
	</entry>
	<entry>
		<line lang="English">America's original jeans - since 1850</line>
		<line lang="French">America's original jeans - depuis 1850</line>
		<line lang="German" />
	</entry>
	<entry>
		<line lang="English">Preshrunk jeans</line>
		<line lang="French">Jean pr&#195;&#169;lav&#195;&#169;</line>
		<line lang="German" />
	</entry>
	<entry>
		<line lang="English">Levi's 501 jeans, America's original jeans since 1850, now come washed and shrunk for your convenience.</line>
		<line lang="French">Ce jean Levi's 501 en denim super r&#195;&#169;sistant a &#195;&#169;t&#195;&#169; pr&#195;&#169;lav&#195;&#169; et r&#195;&#169;tr&#195;&#169;ci pour un meilleur confort.</line>
		<line lang="German" />
	</entry>
	<entry>
		<line lang="English">Please note that the size shown on the garment is the size after washing.</line>
		<line lang="French">La taille indiqu&#195;&#169;e sur le jean tient compte de ce r&#195;&#169;tr&#195;&#169;cissement, choisissez votre taille habituelle.</line>
		<line lang="German" />
	</entry>
	<entry>
		<line lang="English">To ensure a proper fit, please try on before buying and expect further shrinkage of approximately 3% in length.</line>
		<line lang="French">(L&#195;&#169;ger retrait de 3% en longeur au premier lavage).</line>
		<line lang="German" />
	</entry>
	<entry>
		<line lang="English">Original Levi's 501 button-fly jeans.</line>
		<line lang="French">L'authentique jean Levi's 501 braguette boutons.</line>
		<line lang="German" />
	</entry>
	<entry>
		<line lang="English">The coloured Tab and stitched pocket designs are registered trademarks to help you identify garments made only by Levi Strauss &amp; Co.</line>
		<line lang="French">La griffe Levi's en tissu et le dessin des surpiqures des poches sont des marques d&#195;&#169;pos&#195;&#169;es qui vous permettent de reconna&#195;&#174;tre ce jean de la marque Levi's.</line>
		<line lang="German" />
	</entry>
	<entry>
		<line lang="English">Given the rich dye characteristic of this product, colour may transfer when new onto other garments or upholstery.</line>
		<line lang="French">La forte densit&#195;&#169; de la teinture de ce produit peut provoquer quand il est neuf un ph&#195;&#169;nom&#195;&#168;ne de d&#195;&#169;gorgement sur d'autres v&#195;&#170;tements et articles en tissue ou cuir.</line>
		<line lang="German">Aufgrund der tiefen F&#195;&#164;rbung k&#195;&#182;nnen Kleidungsst&#195;&#188;cke oder Polster, die mit der Jeans in Verbindung kommen, anfangs etwas Farbe annehmen. Dieser Effekt legt sich nach den ersten W&#195;&#164;schen.</line>
	</entry>
</multilingual_entry><multilingual_entry>
	<titles>
		<title lang="English">Liege</title>
		<title lang="French">Li&#195;&#168;ge</title>
		<title lang="German">L&#195;&#188;ttich</title>
	</titles>
	<entry>
		<line lang="English"><b>LIEGE</b>, is a one-thousand-year-old city at the heart of Europe.</line>
		<line lang="French"><b>LI&#195;&#136;GE</b>, cit&#195;&#169; mill&#195;&#169;naire au c&#194;&#156;ur de l'Europe.</line>
		<line lang="German"><b>L&#195;&#156;TTICH</b>, tausendj&#195;&#164;hrige Stadt im Herzen Europas.</line>
	</entry>
	<entry>
		<line lang="English">As the ancient capital of an independent principality for eight centuries, Liege, the &#194;&#171;Glowing City&#194;&#187;, possesses an exceptional cultural and architectural patrimony, which is mainly highlighted in its museums.</line>
		<line lang="French">Ancienne capitale d'une principaut&#195;&#169; ind&#195;&#169;pendante pendant plus de huit si&#195;&#168;cles, la &#194;&#171;Cit&#195;&#169; Ardente&#194;&#187; renferme un patrimoine culturel et architectural de premi&#195;&#168;re importance, largement mis en valeur dans ses mus&#195;&#169;es.</line>
		<line lang="German">Die &#194;&#171;feurige Stadt&#194;&#187; war acht Jahrhunderte lang eine ehemalige Hauptstadt eines unabh&#195;&#164;ngigen F&#195;&#188;rstbistums. Sie besitzt einen au&#195;&#159;ergew&#195;&#182;hnlichen Kultur- und Architekturschatz, den wir in seiner ganzen F&#195;&#188;lle in den Museen besichtigen k&#195;&#182;nnen.</line>
	</entry>
	<entry>
		<line lang="English">Near Maastricht and Aachen, Liege, with its reputed university, the exhibition halls, the markets, the Congress Hall, the shopping areas, its river port (ranked third in Europe) is a dynamic economic and commercial centre.</line>
		<line lang="French">Tout pr&#195;&#168;s de Maastricht et d'Aix-la-Chapelle, Li&#195;&#168;ge, avec son universit&#195;&#169; renomm&#195;&#169;e, ses halles de foires, ses march&#195;&#169;s, son Palais des congr&#195;&#168;s, ses rues commer&#195;&#167;antes, son port fluvial (le troisi&#195;&#168;me d'Europe) est un centre &#195;&#169;conomique et commercial dynamique.</line>
		<line lang="German">Ganz nahe bei Maastricht und Aachen liegt L&#195;&#188;ttich. Es verf&#195;&#188;gt &#195;&#188;ber eine ber&#195;&#188;hmte Universit&#195;&#164;t, Messehallen und M&#195;&#164;rkte, &#195;&#188;ber einen Kongresszentrum, einen Binnenhafen (drittgr&#195;&#182;&#195;&#159;er Europas) und Gesch&#195;&#164;ftsstra&#195;&#159;en. Es ist ein dynamisches Wirtschafts- und Handelszentrum.</line>
	</entry>
	<entry>
		<line lang="English">The theatres, the Opera, the Philharmonic Orchestra, as well as the caf&#195;&#169;s and reputed restaurants enhance the warm character of a welcoming city.</line>
		<line lang="French">Ses th&#195;&#169;atres, son Op&#195;&#169;ra, son Orchestre philharmonique, mais aussi ses caf&#195;&#169;s et ses restaurants r&#195;&#169;put&#195;&#169;s en font une ville &#195;&#160; l'accueil chaleureux.</line>
		<line lang="German">Die Theater, die Oper, das Philharmonische Orchester, aber auch die Caf&#195;&#169;s und die ber&#195;&#188;hmten Restaurants machen aus L&#195;&#188;ttich die Stadt mit dem herzlichen Empfang.</line>
	</entry>
</multilingual_entry><multilingual_entry>
	<titles>
		<title lang="English">Shorthand notebook</title>
		<title lang="French">Bloc-st&#195;&#169;no</title>
		<title lang="German">Stenoblock</title>
	</titles>
	<entry>
		<line lang="English">Shorthand notebook</line>
		<line lang="French">Bloc-st&#195;&#169;no</line>
		<line lang="German">Stenoblock</line>
	</entry>
	<entry>
		<line lang="English">Feint ruled</line>
		<line lang="French">R&#195;&#169;gl&#195;&#169;</line>
		<line lang="German">Liniert</line>
	</entry>
	<entry>
		<line lang="English" />
		<line lang="French" />
		<line lang="German" />
	</entry>
	<entry>
		<line lang="English" />
		<line lang="French" />
		<line lang="German" />
	</entry>
	<entry>
		<line lang="English" />
		<line lang="French" />
		<line lang="German" />
	</entry>
</multilingual_entry><multilingual_entry>
	<titles>
		<title lang="English" />
		<title lang="French">Cr&#195;&#168;me &#195;&#160; R&#195;&#169;curer</title>
		<title lang="German">Scheuercreme</title>
	</titles>
	<entry>
		<line lang="English" />
		<line lang="French">Cr&#195;&#168;me &#195;&#160; R&#195;&#169;curer</line>
		<line lang="German">Scheuercreme</line>
	</entry>
	<entry>
		<line lang="English" />
		<line lang="French">Nettoie en douceur et en profondeur, sans rayer.</line>
		<line lang="German">Reinigt sanft und kraftvoll, ohne Kratzer im ganzen Haus.</line>
	</entry>
	<entry>
		<line lang="English" />
		<line lang="French">TOUTES LES SURFACES PLASTIFI&#195;&#137;ES, STRATIFI&#195;&#137;ES, EMAIL ET INOX.</line>
		<line lang="German">PLASTIK, EMAILLE, INOX.</line>
	</entry>
	<entry>
		<line lang="English" />
		<line lang="French">Verser le produit sur la surface &#195;&#160; nettoyer ou sur un chiffon humide.</line>
		<line lang="German">Unverd&#195;&#188;nnt auf Tuch oder Fl&#195;&#164;che geben.</line>
	</entry>
	<entry>
		<line lang="English" />
		<line lang="French">Nettoyer, frotter, rincer.</line>
		<line lang="German">Reinigen, wischen, nachsp&#195;&#188;len.</line>
	</entry>
	<entry>
		<line lang="English" />
		<line lang="French">Refermer le bouchon apr&#195;&#168;s usage.</line>
		<line lang="German">Verschlu&#195;&#159; nach Gebrauch schlie&#195;&#159;en.</line>
	</entry>
	<entry>
		<line lang="English" />
		<line lang="French">Biod&#195;&#169;gradabilit&#195;&#169; sup&#195;&#169;rieure &#195;&#160; 90%.</line>
		<line lang="German">&#195;&#156;ber 90% abbaubar.</line>
	</entry>
</multilingual_entry><multilingual_entry url="adidas_hat">
	<titles>
		<title lang="English">Adidas Hat</title>
		<title lang="French" />
		<title lang="German" />
	</titles>
	<entry>
		<line lang="English">This product has been designed to give you performance, comfort and style.</line>
		<line lang="French">Ce produit a &#195;&#169;t&#195;&#169; con&#195;&#167;u pour vous apporter performance, confort et style.</line>
		<line lang="German">Dieses Produkt wurde entwickelt, um Dir Leistungsverm&#195;&#182;gen, Tragekomfort und Stil zu verleihen.</line>
	</entry>
	<entry>
		<line lang="English">For over 50 years world-class athletes have been relying on adidas to meet their training and competition needs.</line>
		<line lang="French">Depuis plus de 50 ans, les sportifs de niveau international font confiance &#195;&#160; adidas pour satisfaire &#195;&#160; leurs besoins dans le domaine de l'entra&#195;&#174;nement et de la comp&#195;&#169;tition.</line>
		<line lang="German">Seit mehr als 50 Jahren verlassen sich Weltklassesportler auf adidas, um ihre Bed&#195;&#188;rfnisse bei Training und Wettkampf zu erf&#195;&#188;llen.</line>
	</entry>
</multilingual_entry><multilingual_entry>
	<titles>
		<title lang="English">Shoe sole</title>
		<title lang="French">Semelle de chaussure</title>
		<title lang="German">Laufsohle</title>
	</titles>
	<entry>
		<line lang="English">Futter (synthetic)</line>
		<line lang="French">Doublure (synth&#195;&#169;tique)</line>
		<line lang="German">Futter (synthetisch)</line>
	</entry>
	<entry>
		<line lang="English">Inlay sole (synthetic)</line>
		<line lang="French">Semelle int&#195;&#169;rieure (synth&#195;&#169;tique)</line>
		<line lang="German">Innensohle (synthetisch)</line>
	</entry>
	<entry>
		<line lang="English">Outsole (rubber)</line>
		<line lang="French">Semelle (caoutchouc)</line>
		<line lang="German">Laufsohle (Kunststoff)</line>
	</entry>
</multilingual_entry><multilingual_entry>
	<titles>
		<title lang="English">On a box of Grangala biscuits</title>
		<title lang="French" />
		<title lang="German" />
	</titles>
	<entry>
		<line lang="English">Meringue biscuits with hazelnut and chocolate filling</line>
		<line lang="French">Biscuits meringu&#195;&#169;s fourr&#195;&#169;s aux noisettes et au chocolat</line>
		<line lang="German">Sandwich-Schaumgeb&#195;&#164;ck mit haselnuss- und schokoladehaltiger F&#195;&#188;llung</line>
	</entry>
	<entry>
		<line lang="English">Ingredients:</line>
		<line lang="French">Ingr&#195;&#169;dients:</line>
		<line lang="German">Zutaten:</line>
	</entry>
	<entry>
		<line lang="English">Sugar,</line>
		<line lang="French">Sucre,</line>
		<line lang="German">Zucker,</line>
	</entry>
	<entry>
		<line lang="English">hydrogenated vegetable fat and vegetable oil,</line>
		<line lang="French">graisse v&#195;&#169;g&#195;&#169;tale hydrog&#195;&#169;n&#195;&#169;e et huile v&#195;&#169;g&#195;&#169;tale,</line>
		<line lang="German">Pflanzenfett geh&#195;&#164;rtet und Pflanzen&#195;&#182;l,</line>
	</entry>
	<entry>
		<line lang="English">wheatflour,</line>
		<line lang="French">farine de froment,</line>
		<line lang="German">Weizenmehl,</line>
	</entry>
	<entry>
		<line lang="English">skimmed milk powder,</line>
		<line lang="French">lait &#195;&#169;cr&#195;&#169;m&#195;&#169; en poudre,</line>
		<line lang="German">Magermilchpulver,</line>
	</entry>
	<entry>
		<line lang="English">almonds,</line>
		<line lang="French">amandes,</line>
		<line lang="German">Mandeln,</line>
	</entry>
	<entry>
		<line lang="English">cocoa mass 3.8%,</line>
		<line lang="French">p&#195;&#162;te de cacao 3,8%,</line>
		<line lang="German">Kakaomasse 3,8%,</line>
	</entry>
	<entry>
		<line lang="English">egg white powder,</line>
		<line lang="French">blanc d'oeuf en poudre,</line>
		<line lang="German">Eiwei&#195;&#159;pulver,</line>
	</entry>
	<entry>
		<line lang="English">hazelnuts 2.5%,</line>
		<line lang="French">noisettes 2,5%,</line>
		<line lang="German">Haseln&#195;&#188;sse 2,5%,</line>
	</entry>
	<entry>
		<line lang="English">wheat starch,</line>
		<line lang="French">amidon de bl&#195;&#169;,</line>
		<line lang="German">Weizenst&#195;&#164;rke,</line>
	</entry>
	<entry>
		<line lang="English">cocoa butter 0.4%,</line>
		<line lang="French">beurre de cacao 0,4%,</line>
		<line lang="German">Kakaobutter 0,4%,</line>
	</entry>
	<entry>
		<line lang="English">yeast,</line>
		<line lang="French">levure naturelle,</line>
		<line lang="German">Hefe,</line>
	</entry>
	<entry>
		<line lang="English">condensed milk,</line>
		<line lang="French">lait concentr&#195;&#169;,</line>
		<line lang="German">Kondensmilch,</line>
	</entry>
	<entry>
		<line lang="English">butter,</line>
		<line lang="French">beurre,</line>
		<line lang="German">Butter,</line>
	</entry>
	<entry>
		<line lang="English">invert sugar,</line>
		<line lang="French">sucre inverti,</line>
		<line lang="German">Invertzuckersirup,</line>
	</entry>
	<entry>
		<line lang="English">salt,</line>
		<line lang="French">sel,</line>
		<line lang="German">Kochsalz,</line>
	</entry>
	<entry>
		<line lang="English">malt extract,</line>
		<line lang="French">extrait de malt,</line>
		<line lang="German">Malzextrakt,</line>
	</entry>
	<entry>
		<line lang="English">emulsifier (E 322),</line>
		<line lang="French">&#195;&#169;mulsifiant (E 322),</line>
		<line lang="German">Emulgator (E 322),</line>
	</entry>
	<entry>
		<line lang="English">egg powder,</line>
		<line lang="French">oeufs en poudre,</line>
		<line lang="German">Eipulver,</line>
	</entry>
	<entry>
		<line lang="English">glucose syrup,</line>
		<line lang="French">sirop de glucose,</line>
		<line lang="German">Glukosesirup,</line>
	</entry>
	<entry>
		<line lang="English">raising agents (E 503, E 500),</line>
		<line lang="French">poudres &#195;&#160; lever (E 503, E 500),</line>
		<line lang="German">Backtriebmittel (E 503, E 500),</line>
	</entry>
	<entry>
		<line lang="English">milk powder,</line>
		<line lang="French">prot&#195;&#169;ines de lait en poudre,</line>
		<line lang="German">Milcheiwei&#195;&#159;pulver,</line>
	</entry>
	<entry>
		<line lang="English">flavourings,</line>
		<line lang="French">ar&#195;&#180;mes,</line>
		<line lang="German">Aromen,</line>
	</entry>
	<entry>
		<line lang="English">acidity regulator (E 330).</line>
		<line lang="French">acidifiant (E 330).</line>
		<line lang="German">S&#195;&#164;uerungsmittel (E 330).</line>
	</entry>
	<entry>
		<line lang="English">Best before: see side panel</line>
		<line lang="French">A consommer de pr&#195;&#169;f&#195;&#169;rence avant le: voir sur le c&#195;&#180;t&#195;&#169;</line>
		<line lang="German">Mindestens haltbar bis: Siehe Seitenlasche</line>
	</entry>
	<entry>
		<line lang="English">Product of Switzerland</line>
		<line lang="French">Fabriqu&#195;&#169; en Suisse</line>
		<line lang="German">Hergestellt in der Schweiz</line>
	</entry>
	<entry>
		<line lang="English">Specially made for...</line>
		<line lang="French">Distribu&#195;&#169; par...</line>
		<line lang="German">Hergestellt f&#195;&#188;r...</line>
	</entry>
	<entry>
		<line lang="English">100g contains...</line>
		<line lang="French">100g contiennent...</line>
		<line lang="German">100g entahlten...</line>
	</entry>
	<entry>
		<line lang="English">Energy</line>
		<line lang="French">Valeur &#195;&#169;nerg&#195;&#169;tique</line>
		<line lang="German">Energiewert</line>
	</entry>
	<entry>
		<line lang="English">Proteines</line>
		<line lang="French">Prot&#195;&#169;ines</line>
		<line lang="German">Eiwei&#195;&#159;</line>
	</entry>
	<entry>
		<line lang="English">Fat</line>
		<line lang="French">Lipides</line>
		<line lang="German">Fett</line>
	</entry>
	<entry>
		<line lang="English">Net weight</line>
		<line lang="French">Poids net</line>
		<line lang="German">Nettogewicht</line>
	</entry>
</multilingual_entry><multilingual_entry>
	<titles>
		<title lang="English">Walldorf</title>
		<title lang="French">Walldorf</title>
		<title lang="German">Walldorf</title>
	</titles>
	<entry>
		<line lang="English">WALLDORF, Rhine-Neckar-District, the town among the woodlands, was first mentioned in 770, civic rights since 1901.</line>
		<line lang="French">WALLDORF, Rhein-Neckar-Kreis, la ville blottie dans les for&#195;&#170;ts, admise au rang de ville en 1901, dont des documents annoncent l'existence d&#195;&#168;s l'an 770.</line>
		<line lang="German">WALLDORF, Rhein - Neckar - Kreis, die Stadt zwischen den W&#195;&#164;ldern, wurde 770 erstmals urkundlich erw&#195;&#164;hnt und 1901 zur Stadt erhoben.</line>
	</entry>
	<entry>
		<line lang="English">Walldorf calls itself "Astorstadt" after Johann Jakob Astor, born here in 1763 who then emigrated to the U.S.A. and became one of the richest men in the world through fur trade and real estate business.</line>
		<line lang="French">On l'appelle commun&#195;&#169;ment la "ville d'Astor" du nom d'un de ses fils, Johann-Jakob-Astor, qui y naquit en 1763. Il &#195;&#169;migra ensuite en Am&#195;&#169;rique o&#195;&#185; il fit le commerce des fourrures et des biens immobiliers et devint un des hommes les plus riches du monde.</line>
		<line lang="German">Walldorf nennt sich "Astorstadt" nach dem 1763 hier geborenen und nach Amerika ausgewanderten Johann Jakob Astor, der durch Peizhandel und Grundst&#195;&#188;cksgesch&#195;&#164;fte zu einem der reichsten M&#195;&#164;nner der Welt wurde.</line>
	</entry>
	<entry>
		<line lang="English">Walldorf is a twin town of Astoria, Oregon, U.S.A. founded by J.J. Astor.</line>
		<line lang="French">J.J. Astor fonda dans l'Etat d'Oregon, aux Etats-Unis, la ville d'Astoria. Walldorf et Astoria entretiennent d'excellentes relations et sont jumel&#195;&#169;es.</line>
		<line lang="German">Mit der von J.J. Astor gegr&#195;&#188;ndeten Stadt Astoria im Staate Oregon, USA, verbindet Walldorf eine St&#195;&#164;dtepartnerschaft.</line>
	</entry>
	<entry>
		<line lang="English">Walldorf is surrounded by large pinewoods and mixed woodlands offering numerous oppotunities for rest and recreation.</line>
		<line lang="French">Walldorf est entour&#195;&#169; de grandes for&#195;&#170;ts de pins et d'arbres d'essences diverses, qui offrent de nombreuses possibilit&#195;&#169;s de repos et de d&#195;&#169;tente.</line>
		<line lang="German">Walldorf ist von gro&#195;&#159;en Kiefern- und Mischw&#195;&#164;ldern umgeben, welche zahlreiche M&#195;&#182;glichkeiten zur Rast und zur Erholung bieten.</line>
	</entry>
	<entry>
		<line lang="English">Owing to its favourable situation near the motorways Karlsruhe - Frankfurt and Mannheim - Heilbronn (motorway intersection Walldorf), Walldorf is a favourite residential and industrial town.</line>
		<line lang="French">Gr&#195;&#162;ce &#195;&#160; sa situation g&#195;&#169;ographique id&#195;&#169;ale non loin des autoroutes Karlsruhe - Frankfurt et Mannheim - Heilbronn (&#195;&#169;changeur de Walldorf), la ville est une localit&#195;&#169; o&#195;&#185; il fait bon habiter et dont les habitants vivent de l'industrie.</line>
		<line lang="German">Die Stadt ist dank ihrer g&#195;&#188;nstigen Verkehrslage an den Autobahnen Karlsruhe - Frankfurt und Mannheim - Heilbronn (Autobahnkreuz Walldorf) bevorzugte Wohn- und Industriegemeinde.</line>
	</entry>
	<entry>
		<line lang="English">It lies 116m above sea-level, has 14,000 inhabitants and provides almost every kind of facility for leisure.</line>
		<line lang="French">Elle est situ&#195;&#169;e &#195;&#160; 116m d'altitude, a 14.000 habitants et poss&#195;&#168;de pour ainsi dire toutes les installations de loisirs modernes.</line>
		<line lang="German">Sie liegt 116m &#195;&#188;.d.M, hat 14000 Einwohner und besitzt nahezu s&#195;&#164;mtliche Freizeiteinrichtungen.</line>
	</entry>
</multilingual_entry><multilingual_entry url="archaeology">
	<titles>
		<title lang="English">Archaeology</title>
		<title lang="French">Arch&#195;&#169;ologie</title>
		<title lang="German">Arch&#195;&#164;ologie</title>
	</titles>
	<title lang="English">Archaeology</title>
	<entry>
		<line lang="English">Archaeology</line>
		<line lang="French">Arch&#195;&#169;ologie</line>
		<line lang="German">Arch&#195;&#164;ologie</line>
	</entry>
	<entry>
		<line lang="English">The administrator</line>
		<line lang="French">L'administrateur</line>
		<line lang="German">der Verwalter, - / die Verwalterin, -nen</line>
	</entry>
	<entry>
		<line lang="English">Master in Archaeology and Art History</line>
		<line lang="French">Licenci&#195;&#169;(e) en Arch&#195;&#169;ologie et histoire de l'Art</line>
		<line lang="German">Magister in Arch&#195;&#164;ologie und Kunstgeschichte</line>
	</entry>
	<entry>
		<line lang="English">The president</line>
		<line lang="French">Le pr&#195;&#169;sident</line>
		<line lang="German">der Vorsitzender, -</line>
	</entry>
</multilingual_entry></merged_multilang>