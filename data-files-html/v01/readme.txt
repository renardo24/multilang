readme.txt - Multilingual Documents
===================================
Olivier Renard - 13/06/2000

Description:
------------
Stylesheets to highlight the different languages in my multilingual HTML
documents.

version_004 (21/11/01)
----------------------
Document dealing with 4 languages (quadrilingual): English, French,
German and Japanese.

version_001, 002, 003 (13/06/2000, 06/07/2002, 28/06/2001)
----------------------------------------------------------
Documents dealing with 3 languages (trilingual): English, French and
German.