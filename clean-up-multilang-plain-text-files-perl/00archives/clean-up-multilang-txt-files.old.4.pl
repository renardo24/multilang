#!/usr/bin/perl

use File::Spec;
use strict;

my $file;
my $fullPath;
my $fullPathNewFile;
my $mainDir;
my $newFile;
my $line;
my @fileList;

# $#ARGV represents the number of args passed to script; initially it is -1
die "$0 requires an argument: a directory." if $#ARGV != 0;
$mainDir = $ARGV[0];
die "$mainDir is not a directory" if !( -d ${mainDir} && -r ${mainDir});
opendir(MAINDIR, ${mainDir}) || die "Can't open directory $mainDir: $!\n";
@fileList = readdir ( MAINDIR ); # list all files
closedir(MAINDIR);

foreach $file ( @{fileList} ) {
    # print "FILE: ${file}\n";
    if (${file} ne "." && ${file} ne "..") {
         $fullPath = File::Spec->catfile(${mainDir}, ${file});
         print "${fullPath}\n";
         if ( -f ${fullPath} ) {
             open ( FILE, ${fullPath} ) || die "Can't open file '$fullPath': $!\n";
             while ( ${line} = <FILE> ) {
                 $line =~ s/^\s+//; # remove all leading whitespace (spaces, tabs)
				 $line =~ s/\s+$//; # remove all trailing whitespace (spaces, tabs)
                 $line =~ s/^\s*$//gi;
                 $line =~ s/^De$/de/i;
                 $line =~ s/^En$/en/i;
                 $line =~ s/^Fr$/fr/i;
                 $line =~ s/^Title$/title/i;
                 $line =~ s/^Line$/line/i;
                 $line =~ s/\n*^line$/\nline/m; # word "line" preceded by multiple \n replaced by single \n
                 $newFile .= $line;
             }
             close ( FILE );
             $fullPathNewFile = ${fullPath} . "_newFile";
             open ( OUTFILE, ">${fullPathNewFile}" );
             print OUTFILE ${newFile};
             close ( OUTFILE );
             $newFile = "";
         }
    }
}
