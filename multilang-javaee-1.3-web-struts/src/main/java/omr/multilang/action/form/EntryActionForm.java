package omr.multilang.action.form;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * AddEntryActionForm.java
 * 
 * @author Olivier Renard
 * @version 1.0 - 14/08/2003
 */
public class EntryActionForm extends ActionForm {
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1547891358815L;

	private String englishText = null;

	private String frenchText = null;

	private String germanText = null;

	private String japaneseText = null;

	/**
	 * Constructor for AddEntryActionForm.
	 */
	public EntryActionForm() {
		super();
		init();
	}

	/**
	 * Initialisation method called by both the constructor and the
	 * <code>reset</code> method.
	 */
	protected void init() {
		/*
		 * Locale locale = Locale.getDefault(); MessageResources messages =
		 * MessageResources .getMessageResources("ApplicationResources");
		 */

		englishText = "";
		frenchText = "";
		germanText = "";
		japaneseText = "";
	}

	public void reset(ActionMapping actionMapping, HttpServletRequest request) {
		init();
	}

	public ActionErrors validate(ActionMapping actionMapping,
			HttpServletRequest request) {
		ActionErrors errors = new ActionErrors();

		if (((englishText == null) || (englishText.trim().length() == 0))
				&& ((frenchText == null) || (frenchText.trim().length() == 0))
				&& ((germanText == null) || (germanText.trim().length() == 0))
				&& ((japaneseText == null) || (japaneseText.trim().length() == 0))) {
			errors.add("text", new ActionError("error.no.text"));
		}

		return errors;
	}

	/**
	 * Returns the englishText.
	 * 
	 * @return String
	 */
	public String getEnglishText() {
		return englishText;
	}

	/**
	 * Returns the frenchText.
	 * 
	 * @return String
	 */
	public String getFrenchText() {
		return frenchText;
	}

	/**
	 * Returns the germanText.
	 * 
	 * @return String
	 */
	public String getGermanText() {
		return germanText;
	}

	/**
	 * Returns the japaneseText.
	 * 
	 * @return String
	 */
	public String getJapaneseText() {
		return japaneseText;
	}

	/**
	 * Sets the englishText.
	 * 
	 * @param englishText
	 *            The englishText to set
	 */
	public void setEnglishText(String englishText) {
		this.englishText = englishText;
	}

	/**
	 * Sets the frenchText.
	 * 
	 * @param frenchText
	 *            The frenchText to set
	 */
	public void setFrenchText(String frenchText) {
		this.frenchText = frenchText;
	}

	/**
	 * Sets the germanText.
	 * 
	 * @param germanText
	 *            The germanText to set
	 */
	public void setGermanText(String germanText) {
		this.germanText = germanText;
	}

	/**
	 * Sets the japaneseText.
	 * 
	 * @param japaneseText
	 *            The japaneseText to set
	 */
	public void setJapaneseText(String japaneseText) {
		this.japaneseText = japaneseText;
	}
}