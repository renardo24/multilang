#!/usr/bin/env python3
# -*- coding: <encoding name> -*-

import os
import pathlib
import pprint
import tempfile
import xml.etree.ElementTree as ET

OUTPUT_FILE = 'merged_with_etree.xml'


def build_output_file_path():
    """ Build the output file path. """
    output_file_path = pathlib.Path(tempfile.gettempdir(), OUTPUT_FILE)
    # print(output_file_path)
    return output_file_path


def process_xml_files_with_etree(xml_files):
    """ Process XML files using etree. """

    top_element = ET.Element("merged_multilang")
    ET.dump(top_element)
    for xml_file in xml_files:
        with open(xml_file, mode='r', encoding='ISO-8859-1') as fd:
            print(xml_file)
            #lines = fd.readlines()
            # print(lines)
            tree = ET.parse(fd)
            root = tree.getroot()
            # ET.dump(root)
            top_element.append(root)
    ET.dump(top_element)
    output_file_path = build_output_file_path()
    ET.ElementTree(top_element).write(output_file_path, method='xml')


def process_files():
    xml_files_path_envvar = os.getenv("XML_DATA_FILES_PATH")
    if xml_files_path_envvar is not None:
        print(f"Processing files in {xml_files_path_envvar}")
        xml_files_path = pathlib.Path(xml_files_path_envvar)
        xml_files = list(xml_files_path.glob("*.xml"))
        pprint.pprint(xml_files, indent="2")
        process_xml_files_with_etree(xml_files)
    else:
        print(f"XML_DATA_FILES_PATH environment variable not defined")


if __name__ == '__main__':
    process_files()
