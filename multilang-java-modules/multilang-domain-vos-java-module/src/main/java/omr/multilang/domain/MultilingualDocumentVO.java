package omr.multilang.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

/**
 * Class representing a multilingual document or a multilingual text.
 *
 * @author Olivier Renard
 * @version 1.0 - 25/07/2003
 */
public class MultilingualDocumentVO implements Serializable {

    private static final long serialVersionUID = 3849605645830439182L;
    private UUID id;
    private List<MultilingualEntryVO> entries = new ArrayList<MultilingualEntryVO>();
    private String title = "";

    public MultilingualDocumentVO() {
        super();
        id = UUID.randomUUID();
    }

    public void addEntry(final MultilingualEntryVO newEntry) {
        entries.add(newEntry);
    }

    /**
     * Overrides the equals() method. Checks for equality by comparing each object's
     * title, <b>NOT</b> taking into account case sensitivity.
     *
     * @param o the object to check for equality
     * @return boolean true if 2 objects are equal; false otherwise
     */
    @Override
    public boolean equals(final Object o) {
        // Step 1: Perform an == test
        if (this == o) {
            return true;
        }

        // Step 2: Instance of check
        if (!(o instanceof MultilingualDocumentVO)) {
            return false;
        }

        // Step 3: Cast argument
        MultilingualDocumentVO mdvo = (MultilingualDocumentVO) o;

        // Step 4: For each important field, check to see if they are equal
        // For primitives use ==
        // For objects use equals() but be sure to also
        // handle the null case first
        // ----------------------------------------
        // Compare the ids.
        // ----------------------------------------
        if (id != mdvo.id) {
            return false;
        }

        // ----------------------------------------
        // Compare the titles.
        // ----------------------------------------
        if (title != null) {
            if (title.equalsIgnoreCase(mdvo.title)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Returns the hascode for this object.
     *
     * @return int the hascode for this object.
     */
    @Override
    public int hashCode() {
        StringBuilder buf = new StringBuilder();
        buf.append(id);
        buf.append(entries.size());
        buf.append(title);

        return buf.toString()
                  .hashCode();
    }

    public MultilingualEntryVO getEntryById(final String entryId) {
        MultilingualEntryVO entry = null;

        if (entries != null && !entries.isEmpty()
                && entryId != null
                && entryId.trim()
                          .length() > 0) {
            entry = this.getEntryById(UUID.fromString(entryId));
        }

        return entry;
    }

    public MultilingualEntryVO getEntryById(final UUID entryId) {
        MultilingualEntryVO entry = null;

        if (entries != null && !entries.isEmpty()) {
            int size = entries.size();
            for (int i = 0; i < size; i++) {
                MultilingualEntryVO vo = entries.get(i);
                if (entryId.equals(vo.getId())) {
                    entry = vo;
                    break;
                }
            }
        }

        return entry;
    }

    public List<MultilingualEntryVO> getEntries() {
        return entries;
    }

    public void setEntries(final List<MultilingualEntryVO> entries) {
        if (entries != null && !entries.isEmpty()) {
            this.entries = entries;
        }
    }

    public UUID getId() {
        return id;
    }

    public void setId(final UUID id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        if (title != null) {
            String newTitle = title.trim();
            if (newTitle.length() > 0) {
                this.title = newTitle;
            }
        }
    }

    /**
     * Removes an entry with a specific id. Checks whether the list is not null and
     * not empty and whether the index specified is within the list's range before
     * returning the MultilingualEntryVO object that has been removed.
     *
     * @param entryId the id of the MultilingualEntryVO object to remove
     * @return the MultilingualEntryVO object just removed
     */
    public MultilingualEntryVO removeEntryById(final UUID entryId) {
        MultilingualEntryVO entry = null;

        for (Iterator<MultilingualEntryVO> iterator = entries.iterator(); iterator.hasNext(); ) {
            entry = iterator.next();
            if (entryId.equals(entry.getId())) {
                // Remove the current element from the iterator and the list.
                iterator.remove();
                break;
            }
        }

        return entry;
    }

    /**
     * Sets a specific entry's details given a specific id.
     *
     * @param entryId the specified id
     * @param entryVO the MultilingualEntryVO to set for the given
     */
    public void setEntryById(final UUID entryId, final MultilingualEntryVO entryVO) {
        MultilingualEntryVO entry = null;

        if (entries != null && !entries.isEmpty() && entryId != null && entryVO != null) {
            entry = getEntryById(entryId);
            entry.setEnglishText(entryVO.getEnglishText());
            entry.setFrenchText(entryVO.getFrenchText());
            entry.setGermanText(entryVO.getGermanText());
        }
    }

    @Override
    public String toString() {
        String LS = System.getProperty("line.separator");
        StringBuilder buf = new StringBuilder();
        buf.append("--------------------------------------------------");
        buf.append(LS);
        buf.append("Text id:\t");
        buf.append(id);
        buf.append(LS);
        buf.append("Title:\t");
        buf.append(title);
        buf.append(LS);
        if (entries != null) {
            int size = entries.size();
            for (int i = 0; i < size; i++) {
                buf.append(entries.get(i));
            }
        }

        return buf.toString();
    }
}