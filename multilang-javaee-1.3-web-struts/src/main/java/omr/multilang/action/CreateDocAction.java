package omr.multilang.action;

import java.io.File;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import omr.multilang.MultilangConstants;
import omr.multilang.action.form.CreateDocActionForm;
import omr.multilang.dao.DataAccessException;
import omr.multilang.dao.MultilingualDocumentDAOIF;
import omr.multilang.dao.MultilingualDocumentDAOImpl;
import omr.multilang.vo.MultilingualDocumentVO;

public class CreateDocAction extends org.apache.struts.action.Action {
	public CreateDocAction() {
		super();
	}

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		CreateDocActionForm createDocForm = (CreateDocActionForm) form;

		if (this.isCancelled(request)) {
			return (mapping.findForward("home"));
		}

		String action = request.getParameter("action");
		if (action == null) {
			action = "createDoc";
		}
		servlet.log("CreateDocAction: Processing " + action + " action");

		Locale locale = getLocale(request);
		MessageResources mr = getResources(request);

		String docTitle = createDocForm.getDocTitle();
		String docPath = createDocForm.getDocPath();

		File doc = new File(docPath + ".xml");
		String docName = doc.getName();

		if (doc.exists()) {
			servlet.log("The document " + doc + " already exists!");
			createDocForm.resetDocPathAndDocTitle(servlet.getServletContext()
					.getAttribute("original-path").toString());
			request.setAttribute("error.message", mr.getMessage(locale,
					"error.document.already.exists", docName));
			return (mapping.findForward("failure"));
		} else {
			MultilingualDocumentVO docVO = new MultilingualDocumentVO();
			docVO.setTitle(docTitle);

			MultilingualDocumentDAOIF docDAO = new MultilingualDocumentDAOImpl();

			try {
				docDAO.save(docVO, doc.toString());
				HttpSession session = request.getSession();
				session.setAttribute("doc", docVO);
				session.setAttribute("docPath", doc.toString());
				return (mapping.findForward(MultilangConstants.FORWARD_SUCCESS));
			} catch (DataAccessException dae) {
				servlet.log(dae.getMessage());
				request.setAttribute("error.message", mr.getMessage(locale,
						"error.document.save", docName));
				return (mapping.findForward(MultilangConstants.FORWARD_FAILURE));
			}
		}
	}
}
