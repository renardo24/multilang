README


% mode line for jEdit; line starting with a percent
% sign is a comment for txt2tags
% :mode=text:indentSize=2:folding=indent:
% :maxLineLen=72:noTabs=true:tabSize=2:wrap=hard:


Created
=======
2007-06-21 (originally created 2003-08-26).


Description
===========
Version 04 is very similar to version 03. The difference is that every
<entry> tag has a "id" attribute with a unique value.

Used service at http://www.hitsw.com/xml_utilites/ to generate DTD.
