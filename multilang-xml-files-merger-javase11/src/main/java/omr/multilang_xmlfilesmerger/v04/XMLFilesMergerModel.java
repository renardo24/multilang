package omr.multilang_xmlfilesmerger.v04;

import java.io.*;
import java.util.*;

/**
 * Insert the type's description here.
 * <p>
 * Creation date: (05/02/03 11:42:48)
 *
 * @author: Olivier Renard
 */
public abstract class XMLFilesMergerModel implements XMLFilesMergerModelInterface, XMLFilesMergerConstants {
    /**
     * Properties object containing the main parameters to enable the merging of all
     * XML files into one.
     */
    private Properties props = null;

    /**
     * Error message to return if there is a problem
     */
    private String errorMessage = null;

    /**
     * XML information
     */
    private String xmlVersion = null;
    private String encoding = null;
    private boolean standalone = true;
    private boolean validateInput = false;
    private String[] xmlApis = null;

    /**
     * Input and Output information
     */
    private File inputDirectory = null;
    private File outputDirectory = null;
    private String outputFileName = null;

    /**
     * Doctype and DTD information
     */
    private boolean includeDoctype = false;
    private boolean doctypeSystem = false;
    private boolean doctypePublic = false;
    private String publicId = null;
    private File systemId = null;

    /**
     * Root and start elements information. For the output, we need to know the name
     * of the root element of the document. For input, we need to know the name of
     * the element whose contents (and children) will be included in the large XML
     * file.
     */
    private String rootOutputElement = null;
    private String startInputElement = null;

    /**
     * XmlFilesMergerModel constructor comment.
     */
    public XMLFilesMergerModel() {
        super();
    }

    /**
     * Checks where the data (parameters) to be used for generating the output XML
     * file is valid.
     * <p>
     * This is the default implementation and this can obviously be overriden by
     * subclasses.
     * <p>
     * Creation date: (06/02/03 10:43:19)
     *
     * @return java.util.List
     */
    @Override
    public List<String> checkParameters() {
        // List of error messages
        ArrayList<String> errorMessages = new ArrayList<>(10);

        File inputDirectory = getInputDirectory();
        File outputDirectory = getOutputDirectory();
        String outputFileName = getOutputFileName();

        // Check that all input information is valid
        if (inputDirectory != null && inputDirectory.exists()) {
            // Check that the output directory information is valid
            if (outputDirectory != null && outputDirectory.exists()) {
                // Check that the output file name information is valid
                if (outputFileName != null && outputFileName.trim()
                                                            .length() != 0) {
                    // Carry out some further checks
                }
            }
        }

        // Trim to size
        errorMessages.trimToSize();

        return errorMessages;
    }

    /**
     * Generate output file.
     * <p>
     * Method to be implemented by all implementing classes.
     * <p>
     * Creation date: (06/02/03 09:29:12)
     *
     * @throws IOException The exception description.
     */
    @Override
    public abstract void generateOutputFile() throws IOException;

    /**
     * Insert the method's description here. Creation date: (06/02/03 10:13:53)
     *
     * @return java.lang.String
     */
    public String getEncoding() {
        return encoding;
    }

    /**
     * Insert the method's description here. Creation date: (06/02/03 10:13:53)
     *
     * @param newEncoding java.lang.String
     */
    public void setEncoding(final String newEncoding) {
        encoding = newEncoding;
    }

    /**
     * Insert the method's description here. Creation date: (06/02/03 10:13:53)
     *
     * @return java.lang.String
     */
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * Insert the method's description here. Creation date: (06/02/03 10:13:53)
     *
     * @param newErrorMessage java.lang.String
     */
    public void setErrorMessage(final String newErrorMessage) {
        errorMessage = newErrorMessage;
    }

    /**
     * Insert the method's description here. Creation date: (06/02/03 10:13:53)
     *
     * @return java.io.File
     */
    public File getInputDirectory() {
        return inputDirectory;
    }

    /**
     * Insert the method's description here. Creation date: (06/02/03 10:13:53)
     *
     * @param newInputDirectory java.io.File
     */
    public void setInputDirectory(final File newInputDirectory) {
        inputDirectory = newInputDirectory;
    }

    /**
     * Returns a List containing File objects which represent the XML input files
     * needed for processing.
     * <p>
     * This is the default implementation. Subclasses are free to override this is
     * they so wish.
     * <p>
     * Creation date: (06/02/03 18:54:18)
     *
     * @param inputDirectory java.io.File
     * @return java.util.List
     */
    @Override
    public List<File> getInputFiles(final File inputDirectory) {
        ArrayList<File> inputFilesList = null;
        File[] files = inputDirectory.listFiles();

        for (int i = 0; i < files.length; i++) {
            if (files[i].isFile()) {
                // Check that the file has an extension of .xml
                if (files[i].getName()
                            .indexOf(XML_FILE_EXTENSION) != -1) {
                    if (inputFilesList == null) {
                        inputFilesList = new ArrayList<>();
                    }

                    System.out.println(files[i]);
                    inputFilesList.add(files[i]);
                }
            }
        }

        // Trim the list to size and sort its contents so that we process
        // the files in alphabetical order
        if (inputFilesList != null && !inputFilesList.isEmpty()) {
            inputFilesList.trimToSize();
            Collections.sort(inputFilesList);
        }

        return inputFilesList;
    }

    /**
     * Insert the method's description here. Creation date: (06/02/03 10:13:53)
     *
     * @return java.io.File
     */
    public File getOutputDirectory() {
        return outputDirectory;
    }

    /**
     * Insert the method's description here. Creation date: (06/02/03 10:13:53)
     *
     * @param newOutputDirectory java.io.File
     */
    public void setOutputDirectory(final File newOutputDirectory) {
        outputDirectory = newOutputDirectory;
    }

    /**
     * Insert the method's description here. Creation date: (06/02/03 10:13:53)
     *
     * @return java.lang.String
     */
    public String getOutputFileName() {
        return outputFileName;
    }

    /**
     * Insert the method's description here. Creation date: (06/02/03 10:13:53)
     *
     * @param newOutputFileName java.lang.String
     */
    public void setOutputFileName(final String newOutputFileName) {
        outputFileName = newOutputFileName;
    }

    /**
     * Insert the method's description here. Creation date: (06/02/03 10:13:53)
     *
     * @return java.util.Properties
     */
    public Properties getProps() {
        return props;
    }

    /**
     * Insert the method's description here. Creation date: (06/02/03 10:13:53)
     *
     * @param newProps java.util.Properties
     */
    public void setProps(final Properties newProps) {
        props = newProps;
    }

    /**
     * Insert the method's description here. Creation date: (06/02/03 10:13:53)
     *
     * @return java.lang.String
     */
    public String getPublicId() {
        return publicId;
    }

    /**
     * Insert the method's description here. Creation date: (06/02/03 10:13:53)
     *
     * @param newPublicId java.lang.String
     */
    public void setPublicId(final String newPublicId) {
        publicId = newPublicId;
    }

    /**
     * Insert the method's description here. Creation date: (06/02/03 10:13:53)
     *
     * @return java.lang.String
     */
    public String getRootOutputElement() {
        return rootOutputElement;
    }

    /**
     * Insert the method's description here. Creation date: (06/02/03 10:13:53)
     *
     * @param newRootOutputElement java.lang.String
     */
    public void setRootOutputElement(final String newRootOutputElement) {
        rootOutputElement = newRootOutputElement;
    }

    /**
     * Insert the method's description here. Creation date: (06/02/03 10:13:53)
     *
     * @return java.lang.String
     */
    public String getStartInputElement() {
        return startInputElement;
    }

    /**
     * Insert the method's description here. Creation date: (06/02/03 10:13:53)
     *
     * @param newStartInputElement java.lang.String
     */
    public void setStartInputElement(final String newStartInputElement) {
        startInputElement = newStartInputElement;
    }

    /**
     * Insert the method's description here. Creation date: (06/02/03 10:13:53)
     *
     * @return java.io.File
     */
    public File getSystemId() {
        return systemId;
    }

    /**
     * Insert the method's description here. Creation date: (06/02/03 10:13:53)
     *
     * @param newSystemId java.io.File
     */
    public void setSystemId(final File newSystemId) {
        systemId = newSystemId;
    }

    /**
     * Insert the method's description here. Creation date: (07/02/03 15:09:54)
     *
     * @return java.lang.String[]
     */
    public String[] getXmlApis() {
        return xmlApis;
    }

    /**
     * Insert the method's description here. Creation date: (07/02/03 15:09:54)
     *
     * @param newXmlApis java.lang.String[]
     */
    public void setXmlApis(final String[] newXmlApis) {
        xmlApis = newXmlApis;
    }

    /**
     * Insert the method's description here. Creation date: (06/02/03 10:13:53)
     *
     * @return java.lang.String
     */
    public String getXmlVersion() {
        return xmlVersion;
    }

    /**
     * Insert the method's description here. Creation date: (06/02/03 10:13:53)
     *
     * @param newXmlVersion java.lang.String
     */
    public void setXmlVersion(final String newXmlVersion) {
        xmlVersion = newXmlVersion;
    }

    /**
     * Insert the method's description here. Creation date: (06/02/03 10:13:53)
     *
     * @return boolean
     */
    public boolean isDoctypePublic() {
        return doctypePublic;
    }

    /**
     * Insert the method's description here. Creation date: (06/02/03 10:13:53)
     *
     * @param newDoctypePublic boolean
     */
    public void setDoctypePublic(final boolean newDoctypePublic) {
        doctypePublic = newDoctypePublic;
    }

    /**
     * Insert the method's description here. Creation date: (06/02/03 10:13:53)
     *
     * @return boolean
     */
    public boolean isDoctypeSystem() {
        return doctypeSystem;
    }

    /**
     * Insert the method's description here. Creation date: (06/02/03 10:13:53)
     *
     * @param newDoctypeSystem boolean
     */
    public void setDoctypeSystem(final boolean newDoctypeSystem) {
        doctypeSystem = newDoctypeSystem;
    }

    /**
     * Insert the method's description here. Creation date: (06/02/03 10:13:53)
     *
     * @return boolean
     */
    public boolean isIncludeDoctype() {
        return includeDoctype;
    }

    /**
     * Insert the method's description here. Creation date: (06/02/03 10:13:53)
     *
     * @param newIncludeDoctype boolean
     */
    public void setIncludeDoctype(final boolean newIncludeDoctype) {
        includeDoctype = newIncludeDoctype;
    }

    /**
     * Insert the method's description here. Creation date: (06/02/03 10:13:53)
     *
     * @return boolean
     */
    public boolean isStandalone() {
        return standalone;
    }

    /**
     * Insert the method's description here. Creation date: (06/02/03 10:13:53)
     *
     * @param newStandalone boolean
     */
    public void setStandalone(final boolean newStandalone) {
        standalone = newStandalone;
    }

    /**
     * Insert the method's description here. Creation date: (06/02/03 19:24:35)
     *
     * @return boolean
     */
    public boolean isValidateInput() {
        return validateInput;
    }

    /**
     * Insert the method's description here. Creation date: (06/02/03 19:24:35)
     *
     * @param newValidateInput boolean
     */
    public void setValidateInput(final boolean newValidateInput) {
        validateInput = newValidateInput;
    }

    /**
     * Load data from the properties file.
     * <p>
     * Creation date: (05/02/03 12:07:21)
     */
    public void loadProperties() throws IOException {
        props = new Properties();
        InputStream in = new FileInputStream("./" + XML_PROPERTIES_FILE);
        props.load(in);
        in.close();

        /////////////// NEEDS TIDYING UP AND CHECKING FOR MULLS AND EMPTY STRINGS

        /////////////// START - XML APIs ///////////////
        // XML APIs
        String xmlApisStr = null;
        xmlApisStr = props.getProperty(XML_APIS);
        StringTokenizer tk = new StringTokenizer(xmlApisStr,
                                                 ",",
                                                 false);
        int count = tk.countTokens();
        xmlApis = new String[count];
        for (int i = 0; i < count; i++) {
            xmlApis[i] = tk.nextToken()
                           .trim();
        }
        /////////////// END - XML APIs ///////////////

        /////////////// START - XML INFORMATION ///////////////
        // XML version
        xmlVersion = props.getProperty(XML_VERSION);

        // Encoding
        encoding = props.getProperty(ENCODING);

        // Standalone. If the value is null or empty, it will
        // default to the default of the instance variable.
        standalone = "true".equals(props.getProperty(STANDALONE));
        /////////////// END - XML INFORMATION ///////////////

        /////////////// START - INPUT AND OUTPUT INFORMATION ///////////////
        // Input directory
        inputDirectory = new File(props.getProperty(INPUT_DIRECTORY));

        // Output directory
        outputDirectory = new File(props.getProperty(OUTPUT_DIRECTORY));

        // Output file name
        outputFileName = props.getProperty(OUTPUT_FILE_NAME);

        // Validate input. If the value is null or empty, it will
        // default to the default of the instance variable.
        validateInput = "true".equals(props.getProperty(VALIDATE_INPUT));
        /////////////// END - INPUT AND OUTPUT INFORMATION ///////////////

        /////////////// START - DOCTYPE AND DTD INFORMATION ///////////////
        // Doctype and DTD information

        // Include doctype
        includeDoctype = "true".equals(props.getProperty(INCLUDE_DOCTYPE));

        // Public id
        publicId = props.getProperty(PUBLIC_ID);

        // System id
        systemId = new File(props.getProperty(SYSTEM_ID));

        // Doctype public
        doctypePublic = "true".equals(props.getProperty(DOCTYPE_PUBLIC));

        // Doctype system
        doctypeSystem = "true".equals(props.getProperty(DOCTYPE_SYSTEM));
        /////////////// END - DOCTYPE AND DTD INFORMATION ///////////////

        /////////////// START - ROOT AND START ELEMENTS INFORMATION ///////////////
        // Root and start elements information.
        // For the output, we need to know the name
        // of the root element of the document.
        // For input, we need to know the name of
        // the element whose contents (and children)
        // will be included in the large XML file.
        rootOutputElement = props.getProperty(ROOT_OUTPUT_ELEMENT);
        startInputElement = props.getProperty(START_INPUT_ELEMENT);
        /////////////// END - ROOT AND START ELEMENTS INFORMATION ///////////////
    }

    /**
     * Save the data to the properties file.
     * <p>
     * Creation date: (05/02/03 12:07:53)
     */
    public void saveProperties() throws IOException {
        props.store(new FileOutputStream(XML_PROPERTIES_FILE),
                    "xml_files_merger.properties -- " + new java.util.Date());
    }
}
