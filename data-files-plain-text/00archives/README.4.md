# multilang-data

A repository for storing multilingual content and documents.

This project contains documents in multiple languages, predominently in French,
English and German. This is to assist me in the learning of those 3 languages.

+ html -> multilang documents in (X)HTML format
+ txt  -> multilang documents in text format 
+ odt  -> multilang files in ODT file format


## HTML documents
Created on 2007-05-29 (originally created 2003-07-22).


## Template
Th#e ``src`` directory contains a [template ./src/000_template.txt] for 
plain-text multilang documents (written on 2006-02-21).


## Miscellaneous
The following files used to be stored in Microsoft Word files, but have
been migrated to plain text files. Those files were part of a project
originally created on 2003-07-22:
- archaeology.doc (2001-04-28)
- babyliss-paris.doc (2001-05-20)
- bear-castle.doc (2001-04-28)
- grangala-biscuits.doc (2001-05-08)
- indoor-antenna.doc (2001-04-28)
- levis-jeans.doc  (2001-05-08)
- mr-piano.doc (2001-06-05)
- soft-trainer-seat.doc (2001-06-05)
- winnie-musical-mobile.doc (2001-04-27)


### Files
- 000-lib-oli-lang.html.template (template)
- 000-quadrilingual.html.template (template) (2002-11-25)
- css/000-template-lib-oli-lang.css (template)
- css/000-template-quadrilingual.css (template) (2001-09-12)
- bn-chocolates.html (2001-09-21)
- cinema-and-films.html
- colours.html (2001-09-21)
- cote-dor.html (2001-09-21)
- creme-a-recurer.html (2001-09-21)
- education.html (2001-09-21)
- eurostar-no-smoking-please.html (2001-09-21)
- eurostar-ticket-info.html (2001-09-21)
- leonidas.html (2001-06-30)
- liege.html (2001-09-22)
- marriage.html (2001-09-01)
- mr-piano.html
- quadrilingual.html and holyday-inn-japanese.gif were both created on
  2000-12-27. This document deals with 4 languages: English, French,
  German and Japanese.
- shorthand-notebook.html
- sole.html (2001-09-21)
- thomas-sit-and-ride.html (2001-10-12)
- trilingual-v01.html (2000-06-13)
- trilingual-v02.html (2000-07-06)
- trilingual-v03.html (2000-11-28)
- trilingual-v04.html (2001-06-28)
- walldorf.html (2001-09-21)
- winnie-musical-mobile.html
- wrapping-paper.html (2001-09-21)

