package omr.multilang_xmlfilesmerger.v03;

/**
 * Model class.
 *
 * @author Olivier Renard
 */

import java.util.ResourceBundle;

public class XMlFilesMergerModel implements XmlFilesMergerConstants {
    private String inputDirectory = null;

    private String outputDirectory = null;

    private String outputFileName = null;

    private String xmlDeclaration = null;

    private String doctypeDeclaration = null;

    private String inputStartElement = null;

    private String outputRootElement = null;

    public XMlFilesMergerModel() {
        super();
    }

    public void loadData() {
        ResourceBundle bundle = ResourceBundle.getBundle(PROPS_FILE_NAME);
        inputDirectory = bundle.getString(INPUT_DIR);
        outputDirectory = bundle.getString(OUTPUT_DIR);
        outputFileName = bundle.getString(OUTPUT_FILE_NAME);
        xmlDeclaration = bundle.getString(XML_DECL);
        doctypeDeclaration = bundle.getString(DOCTYPE_DECL);
        inputStartElement = bundle.getString(INPUT_START_ELEMENT);
        outputRootElement = bundle.getString(OUTPUT_ROOT_ELEMENT);
    }

    public String getInputDirectory() {
        return inputDirectory;
    }

    public void setInputDirectory(final String param) {
        this.inputDirectory = param;
    }

    public String getOutputDirectory() {
        return outputDirectory;
    }

    public void setOutputDirectory(final String param) {
        this.outputDirectory = param;
    }

    public String getOutputFileName() {
        return outputFileName;
    }

    public void setOutputFileName(final String param) {
        this.outputFileName = param;
    }

    public String getXmlDeclaration() {
        return xmlDeclaration;
    }

    public void setXmlDeclaration(final String param) {
        this.xmlDeclaration = param;
    }

    public String getDoctypeDeclaration() {
        return doctypeDeclaration;
    }

    public void setDoctypeDeclaration(final String param) {
        this.doctypeDeclaration = param;
    }

    public String getInputStartElement() {
        return inputStartElement;
    }

    public void setInputStartElement(final String param) {
        this.inputStartElement = param;
    }

    public String getOutputRootElement() {
        return outputRootElement;
    }

    public void setOutputRootElement(final String param) {
        this.outputRootElement = param;
    }
}