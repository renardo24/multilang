/*
 * InvalidMultilangDocumentException.java
 *
 * Created on 16 February 2006, 10:03
 */

package omr.multilangdom;

/**
 * An exception thrown when a document is considered invalid.
 *
 * @author Olivier Renard
 */
public class InvalidMultilangDocumentException extends RuntimeException {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -2777251322749817472L;

    /**
     * Constructs a new invalid multilang document exception with <code>null</code>
     * as its detail message. The cause is not initialized, and may subsequently be
     * initialized by a call to {@link #initCause}.
     */
    public InvalidMultilangDocumentException() {
        super();
    }

    /**
     * Constructs a new invalid multilang document exception with the specified
     * detail message. The cause is not initialized, and may subsequently be
     * initialized by a call to {@link #initCause}.
     *
     * @param message the detail message. The detail message is saved for later
     *                 retrieval by the {@link #getMessage()} method.
     */
    public InvalidMultilangDocumentException(final String message) {
        super(message);
    }

    /**
     * Constructs a new invalid multilang document exception with the specified
     * detail message and cause. Note that the detail message associated with
     * <code>cause</code> is <i>not</i> automatically incorporated in this runtime
     * exception's detail message.
     *
     * @param message the detail message (which is saved for later retrieval by the
     *                 {@link #getMessage()} method).
     * @param cause   the cause (which is saved for later retrieval by the
     *                 {@link #getCause()} method). (A <tt>null</tt> value is permitted,
     *                 and indicates that the cause is nonexistent or unknown.)
     */
    public InvalidMultilangDocumentException(final String message, final Throwable cause) {
        super(message,
                cause);
    }

    /**
     * Constructs a new invalid multilang document exception with the specified
     * cause and a detail message of
     * <tt>(cause==null ? null : cause.toString())</tt> (which typically contains
     * the class and detail message of <tt>cause</tt>). This constructor is useful
     * for runtime exceptions that are little more than wrappers for other
     * throwables.
     *
     * @param cause the cause (which is saved for later retrieval by the
     *               {@link #getCause()} method). (A <tt>null</tt> value is permitted,
     *               and indicates that the cause is nonexistent or unknown.)
     */
    public InvalidMultilangDocumentException(final Throwable cause) {
        super(cause);
    }
}
