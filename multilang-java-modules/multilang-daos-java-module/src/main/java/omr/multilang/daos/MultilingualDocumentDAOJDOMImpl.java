package omr.multilang.daos;

import omr.multilang.domain.MultilingualDocumentVO;
import omr.multilang.domain.MultilingualEntryVO;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

/**
 * Implementation of the <code>MultilingualDocumentDAO</code> interface for
 * the Data Access Object to be used for MultilingualDocumentVO objects.
 *
 * @author Olivier Renard
 * @version 1.0 - 06/08/2003
 */
public class MultilingualDocumentDAOJDOMImpl implements MultilingualDocumentDAO {
    /**
     * Reads data from an XML file and creates a MultilingualDocumentVO object from
     * the information contained in the XML file.
     *
     * @param path the path to the XML file.
     * @return a MultilingualDocumentVO with the data contained in the XML file.
     * @throws DataAccessException if something goes wrong during the load.
     */
    @Override
    public MultilingualDocumentVO load(final String path) throws DataAccessException {
        MultilingualDocumentVO docVO = null;
        UUID id;

        if (path != null) {
            SAXBuilder builder = new SAXBuilder();
            try {
                docVO = new MultilingualDocumentVO();
                Document doc = builder.build(new File(path));
                Element root = doc.getRootElement();

                // Set the id
                id = UUID.fromString(root.getAttributeValue("id"));
                docVO.setId(id);

                // Set the title
                List<Element> titles = root.getChildren("title");
                docVO.setTitle(titles.get(0)
                                     .getText());

                // Set the entries
                List<Element> entries = root.getChildren("entry");
                int nbrEntries = entries.size();
                for (int i = 0; i < nbrEntries; i++) {
                    Element entry = entries.get(i);

                    MultilingualEntryVO entryVO = new MultilingualEntryVO();

                    id = UUID.fromString(entry.getAttributeValue("id"));
                    entryVO.setId(id);

                    List<Element> lines = entry.getChildren("line");
                    entryVO.setEnglishText(lines.get(0)
                                                .getText());
                    entryVO.setFrenchText(lines.get(1)
                                               .getText());
                    entryVO.setGermanText(lines.get(2)
                                               .getText());
                    docVO.addEntry(entryVO);
                }
            } catch (JDOMException jdome) {
                throw new DataAccessException(jdome.getMessage());
            } catch (IOException ioe) {
                throw new DataAccessException(ioe.getMessage());
            }
        } else {
            throw new DataAccessException("The path cannot be null.");
        }

        return docVO;
    }

    /**
     * Save a multilingual document value object to a particular location defined by
     * the path argument.
     *
     * @param text the multilingual document value object to save.
     * @param path the location where the multilingual document value object is to be
     *             saved.
     * @return true if the save was successful.
     * @throws DataAccessException if something goes wrong during the save.
     */
    @Override
    public boolean save(final MultilingualDocumentVO text, final String path) throws DataAccessException {
        if (text == null || path == null
                || path.trim()
                       .length() == 0) {
            throw new DataAccessException("Invalid path or multilingual document value object.");
        }

        Element root = new Element("text");
        Document doc = new Document(root);
        root.setAttribute("id", "" + text.getId());

        Element title = new Element("title");
        title.setAttribute("lang", "en", Namespace.XML_NAMESPACE);
        title.setText(text.getTitle());
        root.addContent(title);

        List<MultilingualEntryVO> entries = text.getEntries();
        entries.stream()
               .forEach(e -> {
                   Element entry = new Element("entry");
                   entry.setAttribute("id", "" + e.getId());

                   String enText = e.getEnglishText();
                   Element enLine = new Element("line");
                   enLine.setText(enText);
                   enLine.setAttribute("lang", "en", Namespace.XML_NAMESPACE);
                   entry.addContent(enLine);

                   String frText = e.getFrenchText();
                   Element frLine = new Element("line");
                   frLine.setText(frText);
                   frLine.setAttribute("lang", "fr", Namespace.XML_NAMESPACE);
                   entry.addContent(frLine);

                   String deText = e.getGermanText();
                   Element deLine = new Element("line");
                   deLine.setText(deText);
                   deLine.setAttribute("lang", "de", Namespace.XML_NAMESPACE);
                   entry.addContent(deLine);

                   root.addContent(entry);
               });

        try {
            FileOutputStream out = new FileOutputStream(path);
            XMLOutputter outputter = new XMLOutputter();
            outputter.setFormat(Format.getPrettyFormat());
            outputter.output(doc, out);
            outputter.output(doc, System.out);
            out.flush();
        } catch (IOException ioe) {
            throw new DataAccessException(ioe.getMessage());
        }

        return true;
    }
}