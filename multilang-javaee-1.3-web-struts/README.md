# multilang-java-web

## Versions

- v02: javaee-1.3-web-struts-1.1 - restructured
- v01: javaee-1.3-web-struts-1.1 

Created 2006-06-29 (originally created 2003-07-24)

Enable the creation and handling of multilingual texts and data via a web front end.

