package omr.multilang_xmlfilesmerger.v01;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * <p>
 * This class will take every single .xml file in the current directory and
 * merge them into one single large .xml file. All the data required by this
 * small application is stored in the XmlFilesMerger.properties file. <b>ENSURE
 * THAT THE ENCODING OF THE OUTPUT XML DOCUMENT IS "ISO-8859-1".</b>
 * </p>
 * <p>
 * Created 27/11/2001
 */
public class XmlFilesMerger {

    public static final String PROPFILENAME = "XmlFilesMerger_v01";
    // Properties file keys
    public static final String INFILEEXTENSION = "inFileExtension";
    public static final String OUTFILENAME = "outFileName";
    public static final String ROOTNODESTART = "rootNodeStart";
    public static final String ROOTNODEEND = "rootNodeEnd";
    public static final String SEARCHDIR = "searchDir";
    public static final String XMLPI = "xmlPI";
    public static final String XMLPISTART = "xmlPIStart";
    private static ResourceBundle xfmProp = null;
    private ArrayList<File> inputFiles = null;
    private StringBuffer buf = null;

    public XmlFilesMerger() {
        super();
        getProperties();
    }

    public static void main(final String args[]) {
        XmlFilesMerger merger = new XmlFilesMerger();
        merger.processFiles();
    }

    private static void getProperties() {
        try {
            xfmProp = ResourceBundle.getBundle(PROPFILENAME);
        } catch (MissingResourceException mre) {
            System.out.println("Cannot find properties file: " + PROPFILENAME + ". Terminating.");
            terminate();
        }
    }

    private static String getProperty(final String key) {
        String result = null;
        try {
            result = xfmProp.getString(key);
        } catch (Exception e) {
            System.out.println("Failed to read property " + key);
        }
        return result;
    }

    private static void terminate() {
        System.exit(1);
    }

    private void processFiles() {
        getInputFiles();
        readFiles();
        createOutputFile();
    }

    private void getInputFiles() {
        File theDir = new File(getProperty(SEARCHDIR));
        File[] files = theDir.listFiles();
        for (int i = 0; i < files.length; i++) {
            if (files[i].isFile()) {
                if (files[i].getName()
                            .indexOf(getProperty(INFILEEXTENSION)) != -1) {
                    if (!files[i].getName()
                                 .equalsIgnoreCase(getProperty(OUTFILENAME))) {
                        if (inputFiles == null) {
                            inputFiles = new ArrayList<File>();
                        }
                        // System.out.println( files[i] ) ;
                        inputFiles.add(files[i]);
                    }
                }
            }
        }
        Collections.sort(inputFiles);
    }

    private void readFiles() {
        if (inputFiles != null) {
            buf = new StringBuffer();
            buf.append(getProperty(XMLPI) + System.getProperty("line.separator"));
            buf.append(getProperty(ROOTNODESTART) + System.getProperty("line.separator"));
            File theFile = null;
            BufferedReader bf = null;
            try {
                for (int i = 0; i < inputFiles.size(); i++) {
                    String line = null;
                    theFile = inputFiles.get(i);
                    // System.out.println( theFile ) ;
                    bf = new BufferedReader(new FileReader(theFile));
                    buf.append("  ");
                    buf.append("<!-- " + theFile.getName() + " -->" + System.getProperty("line.separator"));
                    while ((line = bf.readLine()) != null) {
                        // System.out.println( getProperty( XMLPISTART ) ) ;
                        if (line.startsWith(getProperty(XMLPISTART))) {
                            continue;
                        } else {
                            buf.append("  ");
                            buf.append(line);
                            buf.append(System.getProperty("line.separator"));
                        }
                    }
                    bf.close();
                    // System.out.println( buf ) ;
                }
            } catch (IOException ioe1) {
                System.out.println("Problem encountered reading file " + theFile
                                           + ": "
                                           + ioe1.getMessage()
                                           + ". Terminating.");
                try {
                    bf.close();
                } catch (IOException ioe2) {
                    System.out.println("Could not close input stream.");
                }
                terminate();
            }
            buf.append(getProperty(ROOTNODEEND));
        } else {
            System.out.println("There are no files to process. Terminating.");
            terminate();
        }
    }

    private void createOutputFile() {
        // System.out.println( getProperty( OUTFILENAME ) ) ;
        try {
            FileWriter fw = new FileWriter(getProperty(OUTFILENAME));
            fw.write(buf.toString());
            fw.close();
        } catch (IOException ioe) {
            System.out.println("Could not write to file " + getProperty(OUTFILENAME) + ": " + ioe.getMessage());
        }
    }
}