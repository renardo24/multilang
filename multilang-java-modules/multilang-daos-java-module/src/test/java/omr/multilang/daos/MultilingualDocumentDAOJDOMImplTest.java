package omr.multilang.daos;

import omr.multilang.domain.MultilingualDocumentVO;
import omr.multilang.domain.MultilingualEntryVO;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.assertj.core.api.Assertions.assertThat;

public class MultilingualDocumentDAOJDOMImplTest {

    public static final String OUTPUT_TEST_FILE_NAME_HELLO = "hello.xml";
    public static final String OUTPUT_TEST_FILE_NAME_HAPPY_MEAL = "happy-meal-toy-plastic-bag.xml";

    @Test
    public void testSaveAndLoadHello(@TempDir Path tempDir) throws IOException, DataAccessException {
        MultilingualDocumentVO saveDVO = new MultilingualDocumentVO();
        saveDVO.setTitle("Hello and goodbye");

        MultilingualEntryVO evo11 = new MultilingualEntryVO();
        evo11.setEnglishText("Hello");
        evo11.setFrenchText("Bonjour");
        evo11.setGermanText("Guten Tag");
        saveDVO.addEntry(evo11);

        MultilingualEntryVO evo12 = new MultilingualEntryVO();
        evo12.setEnglishText("Goodbye");
        evo12.setFrenchText("Au revoir");
        evo12.setGermanText("Auf wiedersehen");
        saveDVO.addEntry(evo12);

        System.out.println(saveDVO);

        Path helloXml = Files.createFile(tempDir.resolve(OUTPUT_TEST_FILE_NAME_HELLO));

        MultilingualDocumentDAO daoImpl = new MultilingualDocumentDAOJDOMImpl();
        daoImpl.save(saveDVO, helloXml.toAbsolutePath()
                                      .toString());

        daoImpl = new MultilingualDocumentDAOJDOMImpl();
        MultilingualDocumentVO readDVO1 = daoImpl.load(helloXml.toAbsolutePath()
                                                               .toString());
        assertThat(readDVO1).isNotNull();
        assertThat(readDVO1.getTitle()).isEqualTo("Hello and goodbye");
        assertThat(readDVO1.getEntries()).hasSize(2);
    }

    @Test
    public void testLoadHello() throws DataAccessException {
        MultilingualDocumentDAO daoImpl = new MultilingualDocumentDAOJDOMImpl();
        MultilingualDocumentVO readDVO1 = daoImpl.load("src/test/resources/hello.xml");
        assertThat(readDVO1).isNotNull();
        assertThat(readDVO1.getTitle()).isEqualTo("Hello and goodbye");
        assertThat(readDVO1.getEntries()).hasSize(2);
    }

    @Test
    public void testSaveAndLoadHappyMeal(@TempDir Path tempDir) throws IOException, DataAccessException {
        MultilingualDocumentVO saveDVO = new MultilingualDocumentVO();
        saveDVO.setTitle("On a Happy Meal toy's platic bag");

        MultilingualEntryVO evo1 = new MultilingualEntryVO();
        evo1.setEnglishText("Risk of choking - small parts.");
        evo1.setFrenchText("Risque d'asphyxie - petits éléments.");
        evo1.setGermanText("Erstickungsgefahr - enthält kleine Teile.");
        saveDVO.addEntry(evo1);

        MultilingualEntryVO evo2 = new MultilingualEntryVO();
        evo2.setEnglishText("Please retain this information for future reference.");
        evo2.setFrenchText("Merci de conserver cette notice d'information.");
        evo2.setGermanText("Bitte diese Information für späteren Gebrauch aufbewahren.");
        saveDVO.addEntry(evo2);

        System.out.println(saveDVO);

        Path happyMealXml = Files.createFile(tempDir.resolve(OUTPUT_TEST_FILE_NAME_HAPPY_MEAL));

        MultilingualDocumentDAO daoImpl = new MultilingualDocumentDAOJDOMImpl();
        daoImpl.save(saveDVO, happyMealXml.toAbsolutePath()
                                          .toString());

        daoImpl = new MultilingualDocumentDAOJDOMImpl();
        MultilingualDocumentVO readDVO1 = daoImpl.load(happyMealXml.toAbsolutePath()
                                                                   .toString());
        assertThat(readDVO1).isNotNull();
        assertThat(readDVO1.getTitle()).isEqualTo("On a Happy Meal toy's platic bag");
        assertThat(readDVO1.getEntries()).hasSize(2);
    }

    @Test
    public void testLoadHappyMeal() throws DataAccessException {
        MultilingualDocumentDAO daoImpl = new MultilingualDocumentDAOJDOMImpl();
        MultilingualDocumentVO readDVO1 = daoImpl.load("src/test/resources/happy-meal-toy-platic-bag.xml");
        assertThat(readDVO1).isNotNull();
        assertThat(readDVO1.getTitle()).isEqualTo("On a Happy Meal toy's platic bag");
        assertThat(readDVO1.getEntries()).hasSize(2);
    }
}
