# multilang DOM Java SE

Define and clarify an object model for multilingual documents using Java SE 6.0.

## History

* 2019-06-17: Moved to Java 11 and JUnit 5
* Originally created 2007-04-02

