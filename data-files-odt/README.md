# Multilang data in ODT format

2017-11-25

Multilingual documents stored in ODT (OpenDocument Text) files.

## Miscellaneous

The following files used to be stored in Microsoft Word files, but have
been migrated to ODT files. Those files were part of a project originally
created on 2003-07-22:
- archaeology.doc (2001-04-28)
- babyliss-paris.doc (2001-05-20)
- bear-castle.doc (2001-04-28)
- grangala-biscuits.doc (2001-05-08)
- indoor-antenna.doc (2001-04-28)
- levis-jeans.doc  (2001-05-08)
- mr-piano.doc (2001-06-05)
- soft-trainer-seat.doc (2001-06-05)
- winnie-musical-mobile.doc (2001-04-27)

