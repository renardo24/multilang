package omr.multilang.action;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import omr.multilang.MultilangConstants;
import omr.multilang.dao.DataAccessException;
import omr.multilang.dao.MultilingualDocumentDAOIF;
import omr.multilang.dao.MultilingualDocumentDAOImpl;
import omr.multilang.vo.MultilingualDocumentVO;

public class DeleteEntryAction extends org.apache.struts.action.Action {
	public static final String FILE_SEPARATOR = System
			.getProperty("file.separator");

	public DeleteEntryAction() {
		super();
	}

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		if (this.isCancelled(request)) {
			return (mapping.findForward("home"));
		}

		String action = request.getParameter("action");
		if (action == null) {
			action = "deleteEntry";
		}
		servlet.log("DeleteEntryAction: Processing " + action + " action");

		Locale locale = getLocale(request);
		MessageResources mr = getResources(request);

		HttpSession session = request.getSession();
		Object oDoc = session.getAttribute("doc");
		Object oPath = session.getAttribute("docPath");

		if (oDoc == null) {
			// HANDLE ERROR - NO DOCUMENT IN THE SESSION
			servlet.log("DeleteEntryAction: The document does not exists!");
			request.setAttribute("error.message", mr.getMessage(locale,
					"error.no.document.available"));
			return (mapping.findForward("failure"));
		} else if (oPath == null) {
			// HANDLE ERROR - NO PATH IN THE SESSION
			servlet
					.log("DeleteEntryAction: The document path does not exists!");
			request.setAttribute("error.message", mr.getMessage(locale,
					"error.no.document.path.available"));
			return (mapping.findForward("failure"));
		} else {
			String entryId = request.getParameter("entryId");
			if (entryId == null) {
				// HANDLE ERROR - NO ENTRY ID IN THE REQUEST
				servlet
						.log("DeleteEntryAction: There is no entry id in the request!");
				request.setAttribute("error.message", mr.getMessage(locale,
						"error.no.entry.id.in.request"));
				return (mapping.findForward("failure"));
			} else {
				MultilingualDocumentVO docVO = (MultilingualDocumentVO) oDoc;
				String docPath = oPath.toString();
				String docName = docPath.substring(docPath
						.lastIndexOf(FILE_SEPARATOR) + 1);

				docVO.removeEntryById(entryId);

				MultilingualDocumentDAOIF docDAO = new MultilingualDocumentDAOImpl();

				try {
					docDAO.save(docVO, docPath);
					session.setAttribute("doc", docVO);
					request.setAttribute("success.message", mr.getMessage(
							locale, "success.delete.entry", "" + entryId,
							docName));

					return (mapping.findForward(MultilangConstants.FORWARD_SUCCESS));
				} catch (DataAccessException dae) {
					servlet.log(dae.getMessage());
					request.setAttribute("error.message", mr.getMessage(locale,
							"error.document.save", docName));
					return (mapping.findForward(MultilangConstants.FORWARD_FAILURE));
				}
			}
		}
	}
}
