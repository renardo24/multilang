<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="text" encoding="iso-8859-1" indent="yes"/>
    <xsl:template match="text">
        <xsl:apply-templates />
    </xsl:template>
    <xsl:template match="title">
title
<xsl:text>  </xsl:text><xsl:value-of select="@xml:lang" />
<xsl:text>&#xA;</xsl:text>
<xsl:text>    </xsl:text><xsl:value-of select="."/>
    </xsl:template>

<xsl:template match="entry">
line<xsl:apply-templates select="line"/> 
</xsl:template>

<xsl:template match="line">
<xsl:text>&#xA;  </xsl:text>
<xsl:value-of select="@xml:lang" />
<xsl:text>&#xA;    </xsl:text><xsl:value-of select="."/>
</xsl:template>
</xsl:stylesheet>
