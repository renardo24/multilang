#!/bin/bash
# 2001-11-28
# Updated 2007-03-27; 2015-06-01
cd ..
mkdir ./tmp/out
java org.apache.xalan.xslt.Process -in ./xml/combined-multilang.xml -xsl ./xslt/multilang-xslt-to-html.xslt -out ./tmp/out/combined.html -EDUMP dump.txt

