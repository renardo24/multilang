title
en
Lee Cooper Trousers

line
en
Flexible process is an exclusive, radical innovation from Lee Cooper.
fr
Flexible process est une innovation radicale et exclusive de Lee Cooper.

line
en
A new fabric with lycra allowing maximum flexibility and movement for body comfort.
fr
Une nouvelle mati�re avec lycra permettant une flexibilit� maximum et un confort de mouvement.

line
en
CMS (Color Management System): A major new innovation which fixes the colour of the indigo and so minimises indigo bleeding.
fr
CMS (Color Management System) est une innovation qui fixe la couleur de l'indigo et vous offre de v�ritables b�n�fices.

line
en
Preserves your wash finish so it stays exactly the same as when you bought it.
fr
Pr�serve la couleur apr�s lavage, comme � son origine.

line
en
Ensures that the fabrix is soft to touch.
fr
Procure un toucher doux � la mati�re.

