/*
 * MultilangEntry.java
 *
 * Created on 16 February 2006, 10:56
 */

package omr.multilangdom;

import java.io.Serializable;

/**
 * A multilang entry can contain up to 4 multilang lines, one for each of the
 * languages supported.
 *
 * @author Olivier Renard
 */
public class MultilangEntry implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 4088119654893579202L;

    /**
     * The entry id.
     */
    private long entryId = -1;

    /**
     * The English multilang line.
     */
    private MultilangLine englishLine = null;

    /**
     * The French multilang line.
     */
    private MultilangLine frenchLine = null;

    /**
     * The German multilang line.
     */
    private MultilangLine germanLine = null;

    /**
     * Creates a new instance of <code>MultilangEntry</code> with an entry id.
     *
     * @param pEntryId the entry id.
     */
    public MultilangEntry(final long pEntryId) {
        super();
        entryId = pEntryId;
    }

    /**
     * Creates a new instance of <code>MultilangEntry</code> with the passed in
     * multilang lines.
     *
     * @param pEntryId    The entry id.
     * @param englishLine The Engligh multilang line.
     * @param frenchLine  The French multilang line.
     * @param germanLine  The German multilang line.
     */
    public MultilangEntry(final long pEntryId, final MultilangLine englishLine, final MultilangLine frenchLine,
                          final MultilangLine germanLine) {
        this(pEntryId);
        this.setEnglishLine(englishLine);
        this.setFrenchLine(frenchLine);
        this.setGermanLine(germanLine);
    }

    /**
     * Returns the hashcode of this instance.
     *
     * @return the hashcode of this instance.
     */
    @Override
    public int hashCode() {
        int hash = 1;
        int primeNumber = 17;

        hash = hash * primeNumber + ((int) entryId);
        hash = hash * primeNumber + ((englishLine != null) ? englishLine.hashCode() : 0);
        hash = hash * primeNumber + ((frenchLine != null) ? frenchLine.hashCode() : 0);
        hash = hash * primeNumber + ((germanLine != null) ? germanLine.hashCode() : 0);

        return hash;
    }

    /**
     * Returns true if pObject is equal to this instance; false otherwise.
     *
     * @param pObject the object to make the comparison against.
     * @return true if pObject is equal to this instance; false otherwise.
     */
    @Override
    public boolean equals(final Object pObject) {
        if (pObject == null) {
            return false;
        }

        // Not strictly necessary, but often a good optimization
        if (this == pObject) {
            return true;
        }

        if (!(this.getClass()
                .equals(pObject.getClass()))) {
            return false;
        }

        MultilangEntry me = (MultilangEntry) pObject;

        boolean result = false;
        boolean entryIdEqual = entryId == me.getEntryId();
        boolean englishLineEqual = (englishLine == null) ? (me.getEnglishLine() == null)
                : (englishLine.equals(me.getEnglishLine()));
        boolean frenchLineEqual = (frenchLine == null) ? (me.getFrenchLine() == null)
                : (frenchLine.equals(me.getFrenchLine()));
        boolean germanLineEqual = (germanLine == null) ? (me.getGermanLine() == null)
                : (germanLine.equals(me.getGermanLine()));

        result = entryIdEqual && englishLineEqual && frenchLineEqual && germanLineEqual;

        return result;
    }

    /**
     * Returns a String representation of this instance.
     *
     * @return a String representation of this instance.
     */
    @Override
    public String toString() {
        String sep = System.getProperty("line.separator");
        StringBuilder buf = new StringBuilder();
        buf.append("entry")
                .append(sep);
        if (englishLine != null) {
            buf.append("\t")
                    .append("en")
                    .append(sep);
            buf.append(englishLine);
        }
        if (frenchLine != null) {
            buf.append("\t")
                    .append("fr")
                    .append(sep);
            buf.append(frenchLine);
        }
        if (germanLine != null) {
            buf.append("\t")
                    .append("de")
                    .append(sep);
            buf.append(germanLine);
        }
        return buf.toString();
    }

    /**
     * Returns the entry id.
     *
     * @return the entry id.
     */
    public long getEntryId() {
        return entryId;
    }

    /**
     * Sets the entry id.
     *
     * @param pEntryId the entry id.
     */
    public void setEntryId(final long pEntryId) {
        entryId = pEntryId;
    }

    /**
     * Returns the English line.
     *
     * @return the English line.
     */
    public MultilangLine getEnglishLine() {
        return englishLine;
    }

    /**
     * Sets the English line.
     *
     * @param englishLine the English line.
     */
    public void setEnglishLine(final MultilangLine englishLine) {
        this.englishLine = englishLine;
    }

    /**
     * Returns the French line.
     *
     * @return the French line.
     */
    public MultilangLine getFrenchLine() {
        return frenchLine;
    }

    /**
     * Sets the French line.
     *
     * @param frenchLine the French line.
     */
    public void setFrenchLine(final MultilangLine frenchLine) {
        this.frenchLine = frenchLine;
    }

    /**
     * Returns the German line.
     *
     * @return the German line.
     */
    public MultilangLine getGermanLine() {
        return germanLine;
    }

    /**
     * Sets the German line.
     *
     * @param germanLine the German line.
     */
    public void setGermanLine(final MultilangLine germanLine) {
        this.germanLine = germanLine;
    }
}
