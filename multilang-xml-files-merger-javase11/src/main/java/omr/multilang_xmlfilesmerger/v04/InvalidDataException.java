package omr.multilang_xmlfilesmerger.v04;

/**
 * Invalid data exception.
 *
 * @author Renardo
 */
public class InvalidDataException extends Exception {

    /**
     * The serial version uid
     */
    private static final long serialVersionUID = -5735348693884732198L;

    public InvalidDataException() {
        super();
    }

    public InvalidDataException(final String message) {
        super(message);
    }

    public InvalidDataException(final Throwable cause) {
        super(cause);
    }

    public InvalidDataException(final String message, final Throwable cause) {
        super(message,
              cause);
    }
}
