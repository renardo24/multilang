package omr.multilang.dao;

/**
 * Data access exception to insulate business objects from the knowledge of the
 * unerlying exception.
 * 
 * @author Olivier Renard
 * @version 1.0 - 24/07/2003
 * @see java.lang.Exception
 */
public class DataAccessException extends Exception {
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 12357345934592345L;

	/**
	 * Default constructor for DataAccessException.
	 */
	public DataAccessException() {
		super();
	}

	/**
	 * Constructor with error message.
	 * 
	 * @param message
	 *            the error message
	 */
	public DataAccessException(String message) {
		super(message);
	}

	/*
	 * Constructor with the original error @param t the original error
	 * 
	 * @see java.lang.Throwable
	 */
	/*
	 * public DataAccessException( Throwable t ) { super( t ) ; }
	 */
}