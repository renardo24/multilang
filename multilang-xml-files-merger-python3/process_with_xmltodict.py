#!/usr/bin/env python3
# -*- coding: <encoding name> -*-

import os
import pathlib
import pprint
import tempfile
from collections import OrderedDict

import xmltodict

OUTPUT_FILE = 'merged_with_xmltodict.xml'


def build_output_file_path():
    """ Build the output file path. """
    output_file_path = pathlib.Path(tempfile.gettempdir(), OUTPUT_FILE)
    # print(output_file_path)
    return output_file_path


def process_xml_files_with_xmltodict(xml_files):
    """ Process XML files using xmltodict. """

    merged_dict = OrderedDict()
    ###merged_dict['merged_multilang'] = dict()
    merged_dict['merged_multilang'] = dict()
    merged_dict['merged_multilang']['multilingual_entries'] = list()
    pprint.pprint(merged_dict, indent=2)

    for xml_file in xml_files:
        with open(xml_file, mode='r', encoding='ISO-8859-1') as fd:
            print(xml_file)
            stem = xml_file.stem
            print(stem)
            #lines = fd.readlines()
            # print(lines)
            doc = xmltodict.parse(fd.read())
            doc['multilingual_entry']['@name'] = stem
            # print(type(doc))
            # pprint.pprint(doc)
            ####merged_dict['merged_multilang'][stem] = doc
            merged_dict['merged_multilang']['multilingual_entries'].append(doc)
            print(len(merged_dict['merged_multilang']))
    output_file_path = build_output_file_path()
    #pprint.pprint(merged_dict, indent=2)
    out = xmltodict.unparse(merged_dict)
    with open(output_file_path, 'w') as of:
        of.write(out)


def process_files():
    xml_files_path_envvar = os.getenv("XML_DATA_FILES_PATH")
    if xml_files_path_envvar is not None:
        print(f"Processing files in {xml_files_path_envvar}")
        xml_files_path = pathlib.Path(xml_files_path_envvar)
        xml_files = list(xml_files_path.glob("*.xml"))
        pprint.pprint(xml_files, indent="2")
        process_xml_files_with_xmltodict(xml_files)
    else:
        print(f"XML_DATA_FILES_PATH environment variable not defined")


if __name__ == '__main__':
    process_files()
