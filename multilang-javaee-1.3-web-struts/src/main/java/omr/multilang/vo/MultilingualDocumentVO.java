package omr.multilang.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Class representing a multilingual document or a multilingual text.
 * 
 * @author Olivier Renard
 * @version 1.0 - 25/07/2003
 */
public class MultilingualDocumentVO implements Serializable {
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 146546545646L;

	private long id = 0;

	private List entries = new ArrayList();

	private String title = "";

	/**
	 * Constructor for MultilingualDocumentVO.
	 */
	public MultilingualDocumentVO() {
		super();
		id = System.currentTimeMillis();
	}

	/**
	 * Add a new entry to the list. The entry must be of type
	 * MultilingualEntryVO in order to be added to the list.
	 * 
	 * @param newEntry
	 *            the new entry (ie MultilingualEntryVO object to add).
	 */
	public void addEntry(MultilingualEntryVO newEntry) {
		entries.add(newEntry);
	}

	/**
	 * Overrides the equals() method. Checks for equality by comparing each
	 * object's title, <b>NOT</b> taking into account case sensitivity.
	 * 
	 * @param o
	 *            the object to check for equality
	 * @return boolean true if 2 objects are equal; false otherwise
	 */
	public boolean equals(Object o) {
		// Step 1: Perform an == test
		if (this == o) {
			return true;
		}

		// Step 2: Instance of check
		if (!(o instanceof MultilingualDocumentVO)) {
			return false;
		}

		// Step 3: Cast argument
		MultilingualDocumentVO mdvo = (MultilingualDocumentVO) o;

		// Step 4: For each important field, check to see if they are equal
		// For primitives use ==
		// For objects use equals() but be sure to also
		// handle the null case first
		// ----------------------------------------
		// Compare the ids.
		// ----------------------------------------
		if (this.id != mdvo.id) {
			return false;
		}

		// ----------------------------------------
		// Compare the titles.
		// ----------------------------------------
		if (this.title != null) {
			if (this.title.equalsIgnoreCase(mdvo.title)) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Returns a specific entry given a specific id.
	 * 
	 * @param entryId
	 *            the specified id
	 * @return the MultilingualEntryVO with the specified id.
	 */
	public MultilingualEntryVO getEntryById(String entryId) {
		MultilingualEntryVO entry = null;

		if (entries != null && !entries.isEmpty() && entryId != null
				&& entryId.trim().length() > 0) {
			long id = Long.parseLong(entryId);

			int size = entries.size();
			for (int i = 0; i < size; i++) {
				MultilingualEntryVO vo = (MultilingualEntryVO) entries.get(i);
				if (vo.getId() == id) {
					entry = vo;
					break;
				}
			}
		}

		return entry;
	}

	/**
	 * Returns the entries.
	 * 
	 * @return List
	 */
	public List getEntries() {
		return entries;
	}

	/**
	 * Returns the id.
	 * 
	 * @return long
	 */
	public long getId() {
		return id;
	}

	/**
	 * Returns the title.
	 * 
	 * @return String
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Returns the hascode for this object.
	 * 
	 * @return int the hascode for this object.
	 */
	public int hashCode() {
		StringBuffer buf = new StringBuffer();
		buf.append(id);
		buf.append(entries.size());
		buf.append(title);

		return buf.toString().hashCode();
	}

	/**
	 * Removes an entry with a specific id. Checks whether the list is not null
	 * and not empty and whether the index specified is within the list's range
	 * before returning the MultilingualEntryVO object that has been removed.
	 * 
	 * @param entryId
	 *            the id of the MultilingualEntryVO object to remove
	 * @return the MultilingualEntryVO object just removed
	 */
	public MultilingualEntryVO removeEntryById(String entryId) {
		MultilingualEntryVO entry = null;

		if (entries != null && !entries.isEmpty() && entryId != null
				&& entryId.trim().length() > 0) {
			long id = Long.parseLong(entryId);

			int size = entries.size();
			for (int i = 0; i < size; i++) {
				entry = (MultilingualEntryVO) entries.get(i);
				if (entry.getId() == id) {
					entries.remove(entry);
					break;
				}
			}
		}

		return entry;
	}

	/**
	 * Sets a specific entry's details given a specific id.
	 * 
	 * @param entryId
	 *            the specified id
	 * @param entryVO
	 *            the MultilingualEntryVO to set for the given
	 */
	public void setEntryById(String entryId, MultilingualEntryVO entryVO) {
		MultilingualEntryVO entry = null;

		if (entries != null && !entries.isEmpty() && entryId != null
				&& entryId.trim().length() > 0 && entryVO != null) {
			entry = getEntryById(entryId);
			entry.setEnglishText(entryVO.getEnglishText());
			entry.setFrenchText(entryVO.getFrenchText());
			entry.setGermanText(entryVO.getGermanText());
			entry.setJapaneseText(entryVO.getJapaneseText());
		}
	}

	/**
	 * Sets the entries.
	 * 
	 * @param entries
	 *            The entries to set
	 */
	public void setEntries(List entries) {
		if (entries != null && !entries.isEmpty()) {
			this.entries = entries;
		}
	}

	/**
	 * Sets the id.
	 * 
	 * @param id
	 *            The id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * Sets the title.
	 * 
	 * @param title
	 *            The title to set
	 */
	public void setTitle(String title) {
		if (title != null) {
			title = title.trim();
			if (title.length() > 0) {
				this.title = title;
			}
		}
	}

	/**
	 * Return the string representation of this object
	 * 
	 * @return the string representation of this object
	 */
	public String toString() {
		String LS = System.getProperty("line.separator");
		StringBuffer buf = new StringBuffer();
		buf.append("--------------------------------------------------");
		buf.append(LS);
		buf.append("Text id:\t");
		buf.append(id);
		buf.append(LS);
		buf.append("Title:\t");
		buf.append(title);
		buf.append(LS);
		if (entries != null) {
			int size = entries.size();
			for (int i = 0; i < size; i++) {
				buf.append(entries.get(i));
			}
		}

		return buf.toString();
	}
}